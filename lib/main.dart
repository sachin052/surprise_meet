import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_apns/apns.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:rxdart/rxdart.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/bloc.dart';
import 'package:surprise_meet/pushnotficationhandler.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';

import 'core/routes/router.gr.dart';
import 'injection_container.dart' as di;
import 'injection_container.dart';

String router = MyRouter.loginPage;
//HomeBloc _homeBloc;
//ChatBloc _chatBloc;
final notification = BehaviorSubject<dynamic>();
final connectionIdMain = BehaviorSubject<String>.seeded("");
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
OverlayEntry overlayEntry;
SharedPrefHelper sharedPrefHelper ;
var user;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await di.init();
  sharedPrefHelper= di.injector<SharedPrefHelper>();
   user=await di.getUser();

  if (user) {
    router = MyRouter.home;
  }
//  await FlutterStatusbarcolor.setStatusBarColor(AppColors.redTextColor);
//  if (useWhiteForeground(Colors.white)) {
//    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
//  } else {
//
//  }
//  FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
//  SystemChrome.setSystemUIOverlayStyle(
//      SystemUiOverlayStyle(statusBarBrightness: Brightness.light) // Or Brightness.dark
//  );
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((c) {
    runApp(MyApp());
  });
}

initBloc() {
//  _homeBloc=injector<HomeBloc>();
//  _chatBloc=injector<ChatBloc>();
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  print("myBackgroundMessageHandler is" + message.toString());
 if(message.containsKey("data")){
   navigateToScreen(message);
   var result = Platform.isAndroid ? message["data"] : message;
   if (result["connection_id"] != null &&
       result["connection_id"] == connectionIdMain.value) {
   } else {
     notification.add(result);
   }
   sendLocalPushNotification(message);
 }
}

Future onDidReceiveLocalNotification(
    int id, String title, String body, String payload) {
  print("did recieve" + payload);
  navigateToScreen(jsonDecode(payload));
}

handlePushNotification() {
  final connector = createPushConnector();
  connector.configure(
    onLaunch: onLaunch,
    onResume: onResume,
    onMessage: onMessage,
    onBackgroundMessage: myBackgroundMessageHandler,
  );
  connector.token.addListener(() async {
    print("device id started");

    if (sharedPrefHelper.getDeviceId() == null) {
      await injector<PushNotificationHandler>().saveDeviceInfo();
    }
    print("device id" + sharedPrefHelper.getDeviceId());
    await sharedPrefHelper.saveDeviceInfo(sharedPrefHelper.getDeviceId(), connector.token.value);
    print("saved token is token " + sharedPrefHelper.getFcmToken());
  });
  connector.requestNotificationPermissions();
}

Future<dynamic> onLaunch(Map<String, dynamic> data) {
  print('onLaunch: $data');
  navigateToScreen(data);
  return Future.value();
}

Future<dynamic> onResume(Map<String, dynamic> data) async {
  print('onResume: $data');

  var result = Platform.isAndroid ? data["data"] : data;
  if (result["connection_id"] != null &&
      result["connection_id"] == connectionIdMain.value) {
  } else {
    notification.add(result);
  }
  navigateToScreen(data);
  return Future.value();
}

Future<dynamic> onMessage(Map<String, dynamic> data) async {
  print('onMessage: $data');
  var result = Platform.isAndroid ? data["data"] : data;
  sendLocalPushNotification(data);
  notification.add(result);
 // _homeBloc.changeNotificationCount(await _homeBloc.currentNotificationCount.first+1);
  return Future.value();
}

void sendLocalPushNotification(Map<String, dynamic> message) async {
  var title = Platform.isIOS ? message["title"] : message["data"]["title"];
  var body = Platform.isIOS ? message["message"] : message["data"]["body"];
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'surprise_meet_notification_channel',
      'surprise_meet_notification_channel',
      'surprise_meet_notification_channel',
      // importance: body=="You've deleted one of your match."?Importance.Low:Importance.Max,
      // priority: body=="You've deleted one of your match."?Priority.Low:Priority.Max,
      importance: Importance.Max,
      priority: Priority.Max,
      playSound: body!="You've deleted one of your match.",
      ticker: 'ticker');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
      body=="You've deleted one of your match."?0:1, title, body, platformChannelSpecifics,
      payload: jsonEncode(message));
//  await flutterLocalNotificationsPlugin.show(
//      0,
//      message["notification"]["title"],
//      message["notification"]["body"],
//      platformChannelSpecifics,
//      payload: jsonEncode(
//          NotificationModel.fromPayload(message["data"]).toJson()));
}

void navigateToScreen(Map<String, dynamic> message) {
  try{
    var result = Platform.isAndroid ? message["data"] : message;
    if (sharedPrefHelper.getUserData() == null) {
      MyRouter.navigatorKey.currentState.popUntil((screen) {
        if (screen.settings.name != MyRouter.loginPage) {

          MyRouter.navigatorKey.currentState.pushNamed(MyRouter.loginPage);
        }
        return true;
      });
    } else if (result["connection_id"] != null) {
      MyRouter.navigatorKey.currentState.popUntil((screen) {
        print("screen"+screen.settings.name);
        if (screen.settings.name != MyRouter.chatScreen) {
          MyRouter.navigatorKey.currentState.pushNamed(MyRouter.chatScreen,
              arguments: SelectedChatScreenArguments(
                  connectionId: result["connection_id"].toString(),
                  otherPersonName: result["sender"].toString()));

        }
        return true;
      });
    } else {
      MyRouter.navigatorKey.currentState.pushNamedAndRemoveUntil(
          MyRouter.home, (r) => false,
          arguments: HomeArguments(fromPush: true));
    }
  }catch(e){

  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // TODO: implement initState
    initBloc();
    super.initState();
    handlePushNotification();
    initLocalNotification();
  }

  initLocalNotification() async {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        requestSoundPermission: false,
        requestBadgePermission: false,
        requestAlertPermission: false,
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payload) async {
//      print("payoload is" + payload);
      var result = jsonDecode(payload);
      navigateToScreen(result);
    });
  }

  @override
  Widget build(BuildContext context) {
//    SizeConfig().init(context);
    return BlocProvider<HomeBloc>(
      create: (_)=>injector<HomeBloc>(),
      child: MaterialApp(
        title: 'Surprise Meet',
        theme: AppTheme.getAppTheme(context),
        debugShowCheckedModeBanner: false,
        initialRoute: router,
//      locale: DevicePreview.of(context).locale, // <--- Add the locale
//      builder: DevicePreview.appBuilder,
        onGenerateRoute: MyRouter.onGenerateRoute,
        navigatorKey: MyRouter.navigatorKey,
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
//    _homeBloc.dispose();
//    _chatBloc.dispose();
    super.dispose();
  }
}

extension StringExtensions on String {
  bool get isValidEmail {
    final emailRegExp = RegExp(r"^[a-zA-Z0-9.-_]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return emailRegExp.hasMatch(this);
  }
}

extension StringExtension on String {
  bool get isValidPass {
    return this.length > 7;
  }
}

extension StringExtensionNumber on String {
  bool get isValidPhone {
    return this.length >= 10;
  }
}

extension DioExtension on DioError {
  String get handleError {
    String errorDescription = "";
    try {
      switch (this.type) {
        case DioErrorType.CANCEL:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.DEFAULT:
          errorDescription = "Request failed due to internet connection";
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorDescription = "Receive timeout";
          break;
        case DioErrorType.RESPONSE:
          errorDescription = "${response.data["error"]["error"][0]}";
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorDescription = "Send timeout";
          break;
      }
    } catch (e) {
      errorDescription = "Something went wrong";
    }
//      } else {
//        errorDescription = "Unexpected error occured";
//      }
    return errorDescription;
  }
}
