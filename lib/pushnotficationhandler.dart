import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';

class PushNotificationHandler {
  final SharedPrefHelper sharedPrefHelper;
  final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  var _firebaseMessaging = FirebaseMessaging();
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  PushNotificationHandler(
      this.sharedPrefHelper, this.flutterLocalNotificationsPlugin);

  init() async {
//    await initLocalNotification();

//    String token = await _firebaseMessaging.getToken();
//    print("token is " + token);

    await saveDeviceInfo();
//    handlePushNotification();
  }
  saveDeviceInfo()async{
    String deviceId;
    // Saving android device id
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfoPlugin.androidInfo;
      deviceId = androidDeviceInfo.androidId.toString();
    }
    //Saving ios device id
    else if (Platform.isIOS) {
      var data = await deviceInfoPlugin.iosInfo;
      deviceId = data.identifierForVendor;
    }
    _saveDeviceInfo(deviceId, sharedPrefHelper.getFcmToken()??"");
  }
  _saveDeviceInfo(String deviceId, String fcmToken) {
    sharedPrefHelper.saveDeviceInfo(deviceId, fcmToken);
  }

  initLocalNotification() async {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {}
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) {}

//  void sendLocalPushNotification(Map<String, dynamic> message) async{
//  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//  'your channel id', 'your channel name', 'your channel description',
//  importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
//  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//  var platformChannelSpecifics = NotificationDetails(
//  androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//  await flutterLocalNotificationsPlugin.show(
//  0, message["notification"]["title"], message["notification"]["body"], platformChannelSpecifics,
//  payload: 'item x');
//  }
}

//Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
//  print("onBg");
//  setState(() {
//    text="On BG";
//  });
//  Router.navigatorKey.currentState.pushNamed(Router.home,arguments: HomeArguments(fromPush: true));
//  if (message.containsKey('data')) {
//    // Handle data message
//    Router.navigatorKey.currentState.pushNamed(Router.home,arguments: HomeArguments(fromPush: true));
//    final dynamic data = message['data'];
//  }
//
//  if (message.containsKey('notification')) {
//    // Handle notification message
//    final dynamic notification = message['notification'];
//    Router.navigatorKey.currentState.pushNamed(Router.home,arguments: HomeArguments(fromPush: true));
//
//  }
//
//  // Or do other work.
//}
