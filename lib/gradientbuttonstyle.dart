import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

final gradientCardStyle = (pressed,{double height}) => ParentStyle()
  ..elevation(pressed ? 0 : 50, color: Colors.grey)
  ..scale(pressed ? 0.95 : 1.0)
//  ..height(height!=null?height:SizeConfig.blockSizeHeight * 7)
  ..margin(all:SizeConfig.blockSizeHeight*2.5)
  ..alignment.center(true)
  ..padding(all: SizeConfig.blockSizeWidth*3)
  ..width(SizeConfig.screenWidth)
  ..borderRadius(all: 15)
  ..boxShadow(
    color: Color(0xFFE40000).withOpacity(.5),
    offset: Offset(3.0, 3.0),
    blur: 7,
  )..linearGradient(
      colors: [AppColors.redTextColor, AppColors.colorPrimary],stops: [0.4,0.8])
  ..ripple(true)
  ..animate(150, Curves.easeOut);