import 'package:flutter/cupertino.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/getchatresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/selectchatresponse.dart';

class ChatEntity {
  final String connectionId;
  final String name;
   DateTime messageTime;
  String message;
  String anonymousUserId;

  ChatEntity(
      {@required this.connectionId,
      @required this.name,
        this.anonymousUserId,
      @required this.messageTime,
      @required this.message});

  factory ChatEntity.fromApiResponse(ChatItem item) => ChatEntity(
      connectionId: item.id.toString(),
      name: item.anyUser,
      anonymousUserId: item.anonymousUserId.toString(),
      messageTime: item.messageTime,
      message: item.message);

  factory ChatEntity.fromSelectedChatResponse(ChatMessage item) => ChatEntity(
      connectionId: item.id.toString(),
      name: item.sender,
      messageTime: item.createdAt,
      message: item.message);
}
