import 'package:flutter/material.dart';
import 'package:super_enum/super_enum.dart';

class PotentialMeetEntity extends Equatable{
  final int meetId;
  final String meetName;
  final String facebookId;
  final String instgramUsername;
  final String emilId;
  final String phone;

  PotentialMeetEntity( {
    @required this.meetId,
    @required this.meetName,
    this.facebookId,
    this.instgramUsername,
    this.emilId,
    this.phone
  });

  @override
  // TODO: implement props
  List<int> get props => [meetId];


}
class ListOfPotentialMeets extends Equatable{
  final List<PotentialMeetEntity> potentialMeetEntityList;
  final int notificationCount;
  ListOfPotentialMeets(
  { @required this.potentialMeetEntityList,
    @required this.notificationCount});

  @override
  // TODO: implement props
  List<int> get props => [notificationCount];
}