import 'package:flutter/cupertino.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/selectchatresponse.dart';

class SelectedChatEntity {
  final String senderId;
  final String message;
  final DateTime messageTime;

  SelectedChatEntity(
      {@required this.senderId, @required this.message, @required this.messageTime});

  factory SelectedChatEntity.fromApiResponse(ChatMessage item) =>
      SelectedChatEntity(
          senderId: item.senderId.toString(),
          message: item.message,
          messageTime: item.createdAt);
}
