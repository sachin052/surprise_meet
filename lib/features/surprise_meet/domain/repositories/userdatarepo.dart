

import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';

abstract class UserDataRepo{
  Future<Either<Failure,ListOfPotentialMeets>> getUserPotentialMeets();
  Future<Either<Failure,ProfileResponse>> getUserProfile();
  Future<Either<Failure,PotentialMeetEntity>> addPotentialMeet(Map <String,dynamic> body);
  Future<Either<Failure,PotentialMeetEntity>> deletePotentialMeet(String meetID);
  Future<Either<Failure,PotentialMeetEntity>> updatePotentialMeet(Map <String,dynamic> body);
  Future<Either<Failure,ProfileResponse>> updateUserProfile(Map <String,dynamic> body);
  Future<Either<Failure,dynamic>> updatePassword(Map <String,dynamic> body);
}