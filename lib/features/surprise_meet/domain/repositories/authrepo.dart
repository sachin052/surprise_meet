import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/addsocialaccountresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/resetpasswordresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/signupresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/loginEntity.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/socialrequesttype.dart';

abstract class AuthRepo {
  Future<Either<Failure, SignUpResponse>> signUp(Map<String, String> body);

  Future<Either<Failure, LoginEntity>> signIn(Map<String, String> body);

  Future<Either<Failure, ResetPasswordResponse>> resetPassword(String email);

  Future<Either<Failure, LoginEntity>> loginUsingFaceBook(Map<String,String> map);

  Future<Either<Failure, LoginEntity>> loginUsingInsta(String authToken);
  // to check if we have fb username of current user in our database
  // if yes hit the login api using fb token
  // if not get the fb username from TextField and send it to server with login api
  Future<Either<Failure, LoginEntity>> checkFacebookUsername(String authToken);

  Future<Either<Failure, AddSocialAccountResponse>> addFacebookWithProfile(
      SocialRequestType requestType);

  Future<Either<Failure, AddSocialAccountResponse>> addInstaWithProfile(
      SocialRequestType authToken);

  Future<Either<Failure, dynamic>> logOut();


}
