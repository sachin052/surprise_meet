import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/report_user_response.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/selectchatresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/chatentity.dart';

abstract class ChatRepo {
  Future<Either<Failure, List<ChatEntity>>> getChatList();

  Future<Either<Failure, SelectedChatResponse>> getSelectedChatList(String connectionId);

  Future<Either<Failure,dynamic>> deleteRecentChat(String connectionId);
  Future<Either<Failure,ReportUserResponse>> reportUser(String userId);
}
