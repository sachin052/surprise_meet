import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';

//class GetPotentialMeetsUseCase extends UseCase<ListOfPotentialMeets>, dynamic> {
//  final UserDataRepo _userDataRepo;
//
//  GetPotentialMeetsUseCase(this._userDataRepo);
//
////  @override
////  Future<Either<Failure, ListOfPotentialMeets>> call(params) {
////    return _userDataRepo.getUserPotentialMeets();
////  }
//}

class GetPotentialMeetsUseCase extends UseCase<ListOfPotentialMeets,dynamic>{
    final UserDataRepo _userDataRepo;

  GetPotentialMeetsUseCase(this._userDataRepo);
  @override
  Future<Either<Failure, ListOfPotentialMeets>> call(params) {
    return _userDataRepo.getUserPotentialMeets();
  }

}
