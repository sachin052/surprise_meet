import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';

class DeletePotentialMeetsUseCase extends UseCase<PotentialMeetEntity,String>{
  final UserDataRepo _userDataRepo;

  DeletePotentialMeetsUseCase(this._userDataRepo);
  @override
  Future<Either<Failure, PotentialMeetEntity>> call(String params) {
    
    return _userDataRepo.deletePotentialMeet(params);
  }
  
}