import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';

class GetProfileUseCase extends UseCase<ProfileResponse,String>{
  final UserDataRepo _userDataRepo;
  GetProfileUseCase(this._userDataRepo);
  @override
  Future<Either<Failure, ProfileResponse>> call(String params) {
    return _userDataRepo.getUserProfile();
  }

}