import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';

class AddPotentialMeetsUseCase extends UseCase<PotentialMeetEntity,Map<String,dynamic>>{
  final UserDataRepo _userDataRepo;

  AddPotentialMeetsUseCase(this._userDataRepo);
  @override
  Future<Either<Failure, PotentialMeetEntity>> call(params) {
    // TODO: implement call
    return _userDataRepo.addPotentialMeet(params);
  }
}