import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';


class UpdatePotentialMeetUseCase extends UseCase<PotentialMeetEntity,Map<String,dynamic>>{
  final UserDataRepo _userDataRepo;

  UpdatePotentialMeetUseCase(this._userDataRepo);
  @override
  Future<Either<Failure, PotentialMeetEntity>> call(Map<String, dynamic> params) {
    return _userDataRepo.updatePotentialMeet(params);
  }

}