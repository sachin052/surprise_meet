import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/chatentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/chatrepo.dart';

class GetChatUseCase extends UseCase<List<ChatEntity>,String>{

  final ChatRepo chatRepo;
  GetChatUseCase(this.chatRepo);
  @override
  Future<Either<Failure, List<ChatEntity>>> call(String params) {
    return chatRepo.getChatList();
  }

}