import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/chatrepo.dart';

class DeleteRecentMatchUseCase extends UseCase<dynamic,String>{
  final ChatRepo _chatRepo;

  DeleteRecentMatchUseCase(this._chatRepo);

  @override
  Future<Either<Failure, dynamic>> call(String params) {
    return _chatRepo.deleteRecentChat(params);

  }
}