import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/report_user_response.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/chatrepo.dart';

class ReportUserUseCase extends UseCase<ReportUserResponse,String>{
  final ChatRepo _chatRepo;

  ReportUserUseCase(this._chatRepo);

  @override
  Future<Either<Failure, ReportUserResponse>> call(String params) {
    return _chatRepo.reportUser(params);
  }
}