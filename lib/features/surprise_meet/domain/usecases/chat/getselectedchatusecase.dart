import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/selectchatresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/chatrepo.dart';

class GetSelectedChatUseCase extends UseCase<SelectedChatResponse, String> {
  final ChatRepo _chatRepo;

  GetSelectedChatUseCase(this._chatRepo);

  @override
  Future<Either<Failure, SelectedChatResponse>> call(String params) {
    return _chatRepo.getSelectedChatList(params);
  }
}
