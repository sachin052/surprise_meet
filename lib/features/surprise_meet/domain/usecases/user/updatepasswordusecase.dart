import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';

class UpdatePasswordUseCase extends UseCase<dynamic,Map<String,String>>{
  final UserDataRepo _userDataRepo;

  UpdatePasswordUseCase(this._userDataRepo);
  @override
  Future<Either<Failure, dynamic>> call(Map<String, String> params) {
    return _userDataRepo.updatePassword(params);
  }

}