import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/loginEntity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/authrepo.dart';

class LoginUsingFBUseCase extends UseCase<LoginEntity,Map<String,String>>{
  final AuthRepo authRepo;

  LoginUsingFBUseCase(this.authRepo);
  @override
  Future<Either<Failure, LoginEntity>> call(Map<String,String> params) {
    return authRepo.loginUsingFaceBook(params);
  }
}