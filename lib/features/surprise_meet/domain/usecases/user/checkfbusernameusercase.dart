import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/loginEntity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/authrepo.dart';

class CheckFBUserNameUseCase extends UseCase<LoginEntity, String> {
  final AuthRepo _authRepo;
  CheckFBUserNameUseCase(this._authRepo);
  @override
  Future<Either<Failure, LoginEntity>> call(String params) {
    return _authRepo.checkFacebookUsername(params);
  }
}
