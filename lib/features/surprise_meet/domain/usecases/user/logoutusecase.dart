import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/authrepo.dart';

class LogOutUseCase extends UseCase<String,String>{
  final AuthRepo _authRepo;
  LogOutUseCase(this._authRepo);
  @override
  Future<Either<Failure, String>> call(String params) {
    return _authRepo.logOut();
  }

}