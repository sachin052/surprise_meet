import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/resetpasswordresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/authrepo.dart';

class ResetPasswordUseCase extends UseCase<ResetPasswordResponse,String>{
  final AuthRepo authRepo;

  ResetPasswordUseCase(this.authRepo);
  @override
  Future<Either<Failure, ResetPasswordResponse>> call(String params) {
    return authRepo.resetPassword(params);
  }
}