import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';

class UpdateUserProfileUseCase extends UseCase<ProfileResponse, Map<String,dynamic>>{
  final UserDataRepo _userDataRepo;

  UpdateUserProfileUseCase(this._userDataRepo);
  @override
  Future<Either<Failure, ProfileResponse>> call(Map<String, dynamic> params) {
    return _userDataRepo.updateUserProfile(params);
  }

}