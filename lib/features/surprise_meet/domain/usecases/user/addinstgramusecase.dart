
import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/addsocialaccountresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/authrepo.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/socialrequesttype.dart';

class AddUpdateInstaUseCase extends UseCase<AddSocialAccountResponse,SocialRequestType>{
  final AuthRepo authRepo;
  AddUpdateInstaUseCase(this.authRepo);
  @override
  Future<Either<Failure, AddSocialAccountResponse>> call(SocialRequestType params) {
    return authRepo.addInstaWithProfile(params);
  }
}