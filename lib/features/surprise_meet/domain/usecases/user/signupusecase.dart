import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/usecase/usecase.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/signupresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/authrepo.dart';

class SignUpUseCase implements UseCase<SignUpResponse, Map<String, String>> {
  final AuthRepo authRepo;

  SignUpUseCase(this.authRepo);

  @override
  Future<Either<Failure, SignUpResponse>> call(Map<String, String> params) {
    return authRepo.signUp(params);
  }
}
