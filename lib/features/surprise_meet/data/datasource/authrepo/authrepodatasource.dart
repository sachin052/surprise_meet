import 'dart:io';

import 'package:dio/dio.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/addsocialaccountresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/loginResponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/resetpasswordresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/signupresponse.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/socialrequesttype.dart';

abstract class AuthRepoDataSource {
  Future<SignUpResponse> signUp(Map<String, String> body);

  Future<LoginResponse> signIn(Map<String, String> body);

  Future<ResetPasswordResponse> forgotPassword(String email);

  Future<LoginResponse> loginUsingFaceBook(Map<String,String> map);
  
  Future<LoginResponse> checkFacebookUsername(String authToken);

  Future<LoginResponse> loginUsingInsta(String authToken);

  Future<AddSocialAccountResponse> addFacebookWithProfile(
      SocialRequestType requestType);

  Future<AddSocialAccountResponse> addInstaWithProfile(
      SocialRequestType authToken);

  Future<dynamic> logOut();
}

class AuthRepoDataSourceImpl extends AuthRepoDataSource {
  final ApiHelper client;
  final SharedPrefHelper _sharedPrefHelper;

  AuthRepoDataSourceImpl(this.client, this._sharedPrefHelper);

  @override
  Future<SignUpResponse> signUp(Map<String, String> body) async =>
      await _signUp("signup", body);

  @override
  Future<LoginResponse> signIn(Map<String, String> body) async =>
      await _signIn("login", body);

  @override
  Future<ResetPasswordResponse> forgotPassword(String email) async =>
      await _forgotPassword("forgotpassword", email);

  @override
  Future<LoginResponse> loginUsingFaceBook(Map<String,String> map) async =>
      await _loginUsingFaceBook("fb-login", map);

  @override
  Future<LoginResponse> loginUsingInsta(String authToken) async =>
      await _loginUsingInsta("insta-login", authToken);

  @override
  Future<AddSocialAccountResponse> addFacebookWithProfile(
          SocialRequestType requestType) async =>
      await _addFacebookWithProfile("fb-signup", requestType);

  @override
  Future<AddSocialAccountResponse> addInstaWithProfile(
          SocialRequestType requestType) async =>
      await _addInstaWithProfile("insta-signup", requestType);

  @override
  Future<dynamic> logOut() async=> await _logOut("logout");

  @override
  Future<LoginResponse> checkFacebookUsername(String authToken) async => _checkFacebookUsername("fb-existence-login", authToken);

  // private methods for network calls
  Future<SignUpResponse> _signUp(String url, Map<String, String> body) async {
    var formData = FormData.fromMap(body);
    final response = await client.dio.post(url, data: formData);
    if (response.statusCode == 201) {
      return SignUpResponse.fromJson(response.data);
    } else {
      throw ServerFailure("Error");
    }
  }

  Future<LoginResponse> _signIn(String url, Map<String, String> body) async {
    body.addAll({
      "device_id": _sharedPrefHelper.getDeviceId(),
      "device_token": _sharedPrefHelper.getFcmToken(),
      "device_type": Platform.isIOS ? "0" : "1"
    });
    var formData = FormData.fromMap(body);
    final response = await client.dio.post(url, data: formData);
    if (response.statusCode == 200) {
      return LoginResponse.fromJson(response.data);
    } else {
      throw ServerFailure("Error");
    }
  }

  Future<ResetPasswordResponse> _forgotPassword(
      String url, String email) async {
    var formData = FormData.fromMap({"email": email});
    final response = await client.dio.post(url, data: formData);
    if (response.statusCode == 200) {
      return ResetPasswordResponse.fromJson(response.data);
    } else {
      throw ServerFailure("Error");
    }
  }
  Future<LoginResponse> _checkFacebookUsername(
      String url, String authToken) async {
    var formData = FormData.fromMap({
      "fb_access_token": authToken,
      "device_id": _sharedPrefHelper.getDeviceId(),
      "device_token": _sharedPrefHelper.getFcmToken(),
      "device_type": Platform.isIOS ? "0" : "1"
    });
    final response = await client.dio.post(url, data: formData);
    if (response.statusCode == 200) {
      return LoginResponse.fromJson(response.data);
    }
    else if(response.statusCode==206){
      throw NoFacebookUserName("No User found");
    }
    else {
      throw ServerFailure("Something went wrong");
    }
  }
  Future<LoginResponse> _loginUsingFaceBook(
      String url, Map<String,String> map) async {
    var formData = FormData.fromMap({
//      "fb_access_token": authToken,
      "device_id": _sharedPrefHelper.getDeviceId(),
      "device_token": _sharedPrefHelper.getFcmToken(),
      "device_type": Platform.isIOS ? "0" : "1"
    }..addAll(map));
    print("login using facebook"+map.toString());
    final response = await client.dio.post(url, data: formData);
    if (response.statusCode == 200) {
      return LoginResponse.fromJson(response.data);
    }
    else if(response.statusCode==206){
      throw NoFacebookUserName("No User found");
    }
    else {
      throw ServerFailure("Something went wrong");
    }
  }

  Future<LoginResponse> _loginUsingInsta(String url, String authToken) async {
    var formData = FormData.fromMap({
      "insta_access_token": authToken,
      "device_id": _sharedPrefHelper.getDeviceId(),
      "device_token": _sharedPrefHelper.getFcmToken(),
      "device_type": Platform.isIOS ? "0" : "1"
    });
    final response = await client.dio.post(url, data: formData);
    if (response.statusCode == 200) {
      return LoginResponse.fromJson(response.data);
    } else {
      throw ServerFailure("Error");
    }
  }

  Future<AddSocialAccountResponse> _addFacebookWithProfile(
      String url, SocialRequestType requestType) async {
    return requestType.when(addSocialAccount: (data) async {
      var formData = FormData.fromMap(data.body);
      final response = await client.dio.post(url, data: formData);
      if (response.statusCode == 201) {
        return AddSocialAccountResponse.fromJson(response.data);
      } else {
        throw ServerFailure("Error");
      }
    }, updateSocialAccount: (data) async {
      var formData = FormData.fromMap(data.body);
      final response = await client.dio.put(url, data: formData);
      if (response.statusCode == 200) {
        return AddSocialAccountResponse.fromJson(response.data);
      } else {
        throw ServerFailure("Error");
      }
    });
  }

  Future<AddSocialAccountResponse> _addInstaWithProfile(
      String url, SocialRequestType requestType) async {
    return requestType.when(addSocialAccount: (data) async {
      var formData = FormData.fromMap(data.body);
      final response = await client.dio.post(url, data: formData);
      if (response.statusCode == 201||response.statusCode==200) {
        return AddSocialAccountResponse.fromJson(response.data);
      } else {
        throw ServerFailure("Error");
      }
    }, updateSocialAccount: (data) async {
      var formData = FormData.fromMap(data.body);
      final response = await client.dio.put(url, data: formData);
      if (response.statusCode == 200) {
        return AddSocialAccountResponse.fromJson(response.data);
      } else {
        throw ServerFailure("Error");
      }
    });
  }



  Future<dynamic> _logOut(String url)async{
    var formData = FormData.fromMap({"device_id": _sharedPrefHelper.getDeviceId()});
    final response = await client.dio.post(url, data: formData);
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw ServerFailure("Error");
    }
  }

  
}
