import 'package:dio/dio.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/getchatresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/report_user_response.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/selectchatresponse.dart';

abstract class ChatDataSource{
  Future<GetChatResponse> getChatList();
  Future<SelectedChatResponse> getSelectedChatList(String connectionId);
  Future<dynamic> deleteSelectedRecent(String connectionId);
  Future<ReportUserResponse> reportUser(String userId);
}

class ChatDataSourceImpl extends ChatDataSource{
  final ApiHelper _apiHelper;

  ChatDataSourceImpl(this._apiHelper);
  @override
  Future<GetChatResponse> getChatList() async => await _getChatList("surprisematch");
  
  Future<GetChatResponse>_getChatList(String url)async{
    final response = await _apiHelper.dio.get(url);
    if (response.statusCode == 200) {
      var json= GetChatResponse.fromJson(response.data);
      if(json.chatItems!=null)
        return json;
            throw NoDataFound("No data found");
    }  else {
      throw ServerFailure("Error");
    }
  }

  @override
  Future<SelectedChatResponse> getSelectedChatList(String connectionId) async=> await _getSelectedChatResponse("chats", connectionId);

  Future<SelectedChatResponse> _getSelectedChatResponse(String url, String connectionId)async{
    var map=FormData.fromMap({
      "connection_id":connectionId
    });
    final response = await _apiHelper.dio.post(url,data: map);
    if (response.statusCode == 200) {
      return SelectedChatResponse.fromJson(response.data);
    } else if (response.statusCode == 204) {
      throw NoDataFound("No data Found");
    } else {
      throw ServerFailure("Error");
    }
  }

  @override
  Future<dynamic> deleteSelectedRecent(String connectionId) async{
    var map=FormData.fromMap({
      "match_id":connectionId
    });
    var response = await _apiHelper.dio.delete("surprisematch",data: map);
    return response;
  }

  @override
  Future<ReportUserResponse> reportUser(String userId)async {
    var map=FormData.fromMap({
      "reported_user_id":userId
    });
    var response = await _apiHelper.dio.post("report-user",data: map);
    if (response.statusCode == 200) {
      return ReportUserResponse.fromJson(response.data);
    } else {
      throw ServerFailure("Something went wrong");
    }
  }
  
}