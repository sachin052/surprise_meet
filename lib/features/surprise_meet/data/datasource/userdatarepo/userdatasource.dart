import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/potentialmeetresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';

abstract class UserDataSource {
  Future<PotentialMeetResponse> getPotentialMeets();

  Future<ProfileResponse> getUserProfile();

  Future<Meet> addPotentialMeet(Map<String, dynamic> body);

  Future<Meet> updatePotentialMeet(Map<String, dynamic> body);

  Future<ProfileResponse> updateUserProfile(Map<String, dynamic> body);

  Future<Meet> deletePotentialMeet(String meetID);

  Future<dynamic> updatePassword(Map<String,String> body);
}

class UserDataSourceImpl extends UserDataSource {
  final ApiHelper client;

  UserDataSourceImpl(this.client);

  @override
  Future<PotentialMeetResponse> getPotentialMeets() async =>
      await _getPotentialMeets("potentialmeet");

  @override
  Future<Meet> addPotentialMeet(Map<String, dynamic> body) async =>
      await _addPotentialMeet("potentialmeet", body);

  Future<PotentialMeetResponse> _getPotentialMeets(String url) async {
    final response = await client.dio.get(url);

      if (response.statusCode == 200) {
        var json= PotentialMeetResponse.fromJson(response.data);
        if(json.meets!=null)
          return json;
        else
          throw NoDataFound("No data Found");
      } else {
        throw ServerFailure("Error");
      }
  }

  Future<Meet> _addPotentialMeet(String url, Map<String, dynamic> body) async {
    var map = FormData.fromMap(body);
    final response = await client.dio.post(url, data: map);
    if (response.statusCode == 201) {
      return Meet.fromJson(response.data['data']);
    } else if (response.statusCode == 204) {
      throw NoDataFound("No data Found");
    } else {
      throw ServerFailure("Error");
    }
  }

  Future<Meet> _deletePotentialMeet(String url, String meetID) async {
    var map = {"potential_meet_id": meetID};
    final response = await client.dio.delete(url, data: map);
    if (response.statusCode == 200) {
      jsonEncode(response.data);
      return Meet.fromJson(response.data["data"]);
    } else {
      throw ServerFailure("Error");
    }
  }

  Future<ProfileResponse> _getUserProfile(String url) async {
    final response = await client.dio.get(url);
    if (response.statusCode == 200) {
      return ProfileResponse.fromJson(response.data);
    } else {
      throw ServerFailure("Error");
    }
  }

  Future<Meet> _updatePotentialMeet(
      String url, Map<String, dynamic> body) async {
    var map = FormData.fromMap(body);
    final response = await client.dio.put(url, data: map);
    if (response.statusCode == 200) {
      return Meet.fromJson(response.data['data']);
    } else {
      throw ServerFailure("Error");
    }
  }

  Future<dynamic> _updatePassword(Map<String,String>body)async{
  var map = FormData.fromMap(body);
  final response = await client.dio.post("updatepassword", data: map);
  if (response.statusCode == 200) {
  return response.data;
  } else {
  throw ServerFailure("Error");
  }
  }

  @override
  Future<Meet> deletePotentialMeet(String meetID) async =>
      _deletePotentialMeet("potentialmeet", meetID);

  @override
  Future<ProfileResponse> getUserProfile() async => _getUserProfile("profile");

  @override
  Future<Meet> updatePotentialMeet(Map<String, dynamic> body) async =>
      await _updatePotentialMeet("potentialmeet", body);

  @override
  Future<ProfileResponse> updateUserProfile(Map<String, dynamic> body) async =>
      await _updateProfile("profile", body);

  Future<ProfileResponse> _updateProfile(
      String url, Map<String, dynamic> body) async {
    var map = FormData.fromMap(body);
    final response = await client.dio.patch(url, data: map);
    if (response.statusCode == 200) {
      return ProfileResponse.fromJson(response.data);
    } else {
      throw ServerFailure("Error");
    }
  }

  @override
  Future updatePassword(Map<String, String> body) async=> _updatePassword(body);
}
