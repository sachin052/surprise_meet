import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/loginResponse.dart';

abstract class SharedPrefHelper {

  UserData getUserData();

  Future saveUserData(UserData user);

  Future clearData();

  Future saveDeviceInfo(String deviceId, String fcmToken);

  String getDeviceId();

  String getFcmToken();
}

class SharedPrefHelperImpl implements SharedPrefHelper {
  final SharedPreferences prefs;

  SharedPrefHelperImpl(this.prefs);

  @override
  Future clearData() async {
    await prefs.clear();
  }

  @override
  UserData getUserData() {
    if (prefs.containsKey("user")) {
      return UserData.fromJson(jsonDecode(prefs.get("user")));
    } else {
      return null;
    }
  }

  @override
  Future saveUserData(UserData user) async {
    await prefs.setString("user", jsonEncode(user));
  }

  @override
  Future saveDeviceInfo(String deviceId, String fcmToken) async {
    await prefs.setString("deviceId", deviceId);
    await prefs.setString("fcmToken", fcmToken);
  }

  @override
  String getDeviceId() {
    return prefs.getString("deviceId");
  }

  @override
  String getFcmToken() {
    return prefs.getString("fcmToken")??"0";
  }
}
