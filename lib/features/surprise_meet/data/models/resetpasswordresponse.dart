// To parse this JSON data, do
//
//     final resetPasswordResponse = resetPasswordResponseFromJson(jsonString);

import 'dart:convert';

ResetPasswordResponse resetPasswordResponseFromJson(String str) => ResetPasswordResponse.fromJson(json.decode(str));

String resetPasswordResponseToJson(ResetPasswordResponse data) => json.encode(data.toJson());

class ResetPasswordResponse {
  bool valid;
  String message;
  int code;
  ResetDetails resetDetails;

  ResetPasswordResponse({
    this.valid,
    this.message,
    this.code,
    this.resetDetails,
  });

  factory ResetPasswordResponse.fromJson(Map<String, dynamic> json) => ResetPasswordResponse(
    valid: json["valid"],
    message: json["message"],
    code: json["code"],
    resetDetails: ResetDetails.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "message": message,
    "code": code,
    "details": resetDetails.toJson(),
  };
}

class ResetDetails {
  String email;
  String url;

  ResetDetails({
    this.email,
    this.url,
  });

  factory ResetDetails.fromJson(Map<String, dynamic> json) => ResetDetails(
    email: json["email"],
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "url": url,
  };
}
