// To parse this JSON data, do
//
//     final profileResponse = profileResponseFromJson(jsonString);

import 'dart:convert';

ProfileResponse profileResponseFromJson(String str) => ProfileResponse.fromJson(json.decode(str));

String profileResponseToJson(ProfileResponse data) => json.encode(data.toJson());

class ProfileResponse {
  bool valid;
  String message;
  int code;
  Userprofile userprofile;

  ProfileResponse({
    this.valid,
    this.message,
    this.code,
    this.userprofile,
  });

  factory ProfileResponse.fromJson(Map<String, dynamic> json) => ProfileResponse(
    valid: json["valid"],
    message: json["message"],
    code: json["code"],
    userprofile: Userprofile.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "message": message,
    "code": code,
    "userprofile": userprofile.toJson(),
  };
}

class Userprofile {
  int id;
  String name;
  String phone;
  String email;
  bool isVerified;
  bool isFacebook;
  bool isInsta;
  String facebookId;
  String instaId;
  bool hasPassword;
  Userprofile({
    this.id,
    this.name,
    this.phone,
    this.email,
    this.isVerified,
    this.isFacebook,
    this.isInsta,
    this.facebookId,
    this.instaId,
    this.hasPassword
  });

  factory Userprofile.fromJson(Map<String, dynamic> json) => Userprofile(
    id: json["id"],
    name: json["name"],
    phone: json["phone"],
    email: json["email"],
    isVerified: json["is_verified"],
    isFacebook: json["is_facebook"],
    isInsta: json["is_insta"],
    facebookId: json["fb_email"],
    instaId: json["insta_user"],
      hasPassword: json["has_password"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "phone": phone,
    "email": email,
    "is_verified": isVerified,
    "is_facebook": isFacebook,
    "is_insta": isInsta,
    "fb_email":facebookId,
    "insta_user":instaId
  };
}
