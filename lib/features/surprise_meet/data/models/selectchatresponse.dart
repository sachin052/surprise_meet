// To parse this JSON data, do
//
//     final selectedChatResponse = selectedChatResponseFromJson(jsonString);

import 'dart:convert';

SelectedChatResponse selectedChatResponseFromJson(String str) => SelectedChatResponse.fromJson(json.decode(str));

String selectedChatResponseToJson(SelectedChatResponse data) => json.encode(data.toJson());

class SelectedChatResponse {
  bool valid;
  String message;
  int code;
  ChatData data;

  SelectedChatResponse({
    this.valid,
    this.message,
    this.code,
    this.data,
  });

  factory SelectedChatResponse.fromJson(Map<String, dynamic> json) => SelectedChatResponse(
    valid: json["valid"],
    message: json["message"],
    code: json["code"],
    data: ChatData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "message": message,
    "code": code,
    "data": data.toJson(),
  };
}

class ChatData {
  String chatRoom;
  List<ChatMessage> messages;
  ChatData({
    this.chatRoom,
    this.messages,
  });

  factory ChatData.fromJson(Map<String, dynamic> json) => ChatData(
    chatRoom: json["chat_room"],
    messages: List<ChatMessage>.from(json["messages"].map((x) => ChatMessage.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "chat_room": chatRoom,
    "messages": List<dynamic>.from(messages.map((x) => x.toJson())),
  };
}

class ChatMessage {
  int id;
  String message;
  String sender;
  int senderId;
  String receiver;
  int receiverId;
  DateTime createdAt;

  ChatMessage({
    this.id,
    this.message,
    this.sender,
    this.senderId,
    this.receiver,
    this.receiverId,
    this.createdAt,
  });

  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
    id: json["id"],
    message: json["message"],
    sender: json["sender"],
    senderId: json["sender_id"],
    receiver: json["receiver"],
    receiverId: json["receiver_id"],
    createdAt: DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "message": message,
    "sender": sender,
    "sender_id": senderId,
    "receiver": receiver,
    "receiver_id": receiverId,
    "created_at": createdAt.toIso8601String(),
  };
}
