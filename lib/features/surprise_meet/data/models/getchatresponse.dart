// To parse this JSON data, do
//
//     final getChatResponse = getChatResponseFromJson(jsonString);

import 'dart:convert';

GetChatResponse getChatResponseFromJson(String str) => GetChatResponse.fromJson(json.decode(str));

String getChatResponseToJson(GetChatResponse data) => json.encode(data.toJson());

class GetChatResponse {
  bool valid;
  String message;
  int code;
  List<ChatItem> chatItems;

  GetChatResponse({
    this.valid,
    this.message,
    this.code,
    this.chatItems,
  });

  factory GetChatResponse.fromJson(Map<String, dynamic> json) => GetChatResponse(
    valid: json["valid"],
    message: json["message"],
    code: json["code"],
    chatItems: json["data"].isNotEmpty?List<ChatItem>.from(json["data"].map((x) => ChatItem.fromJson(x))):null,
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "message": message,
    "code": code,
    "data": List<dynamic>.from(chatItems.map((x) => x.toJson())),
  };
}

class ChatItem {
  int id;
  String chatUser;
  String chatRoom;
  String anyUser;
  bool isActive;
  String message;
  int anonymousUserId;
  DateTime messageTime;

  ChatItem({
    this.id,
    this.chatUser,
    this.chatRoom,
    this.isActive,
    this.message,
    this.messageTime,
    this.anyUser,
    this.anonymousUserId
  });

  factory ChatItem.fromJson(Map<String, dynamic> json) => ChatItem(
    id: json["id"],
    chatUser: json["chat_user"],
    chatRoom: json["chat_room"],
    isActive: json["is_active"],
    message: json["message"],
    anyUser: json["anonymous_user_name"],
    anonymousUserId: json["anonymous_user_id"],
    messageTime: DateTime.parse(json["message_time"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "chat_user": chatUser,
    "chat_room": chatRoom,
    "is_active": isActive,
    "message": message,
    "anonymous_user_id":anonymousUserId,
    "anonymous_user_name":anyUser,
    "message_time": messageTime.toIso8601String(),
  };
}
