// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:super_enum/super_enum.dart';

LoginResponse loginResponseFromJson(String str) =>
    LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse extends Equatable {
  final bool valid;
  final String message;
  final int code;
  final UserData userData;

  LoginResponse({
    this.valid,
    this.message,
    this.code,
    this.userData,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        valid: json["valid"],
        message: json["message"],
        code: json["code"],
        userData: UserData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "valid": valid,
        "message": message,
        "code": code,
        "userData": userData.toJson(),
      };

  @override
  List<dynamic> get props => [code, message];
}

class UserData {
  String name;
  int id;
  String email;
  String authToken;
  int expiresIn;
  bool isInsta;
  bool isFacebook;

  UserData(
      {this.name,
      this.id,
      this.email,
      this.authToken,
      this.expiresIn,
      this.isInsta,
      this.isFacebook,
      });

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
      name: json["name"],
      id: json["id"],
      email: json["email"],
      authToken: json["auth_token"],
      expiresIn: json["expires_in"],
      isFacebook: json["is_facebook"],
      isInsta: json["is_insta"],);

  Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
        "email": email,
        "auth_token": authToken,
        "expires_in": expiresIn,
        "is_facebook": isFacebook,
        "is_insta": isInsta
      };
}
