import 'package:flutter/cupertino.dart';

class NotificationModel {
  final String senderName;
  final String connectionId;

  @override
  String toString() {
    return toJson().toString();
  }

  NotificationModel({@required this.senderName, @required this.connectionId});

  factory NotificationModel.fromPayload(Map<dynamic, dynamic> json) {
    return NotificationModel(
        senderName: json["sender"], connectionId: json["connection_id"]);
  }

  Map<String, dynamic> toJson() => {
        "sender": senderName,
        "connection_id": connectionId,
      };
}
