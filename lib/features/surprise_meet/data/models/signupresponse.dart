class SignUpResponse {
  bool valid;
  int code;
  UserData data;
  String message;
  SignUpResponse({this.valid, this.code, this.data, this.message});

  factory SignUpResponse.fromJson(Map<String, dynamic> json) => SignUpResponse(
        valid: json["valid"],
        code: json["code"],
        message: json["message"],
        data: UserData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "valid": valid,

        "code": code,
        "message": message,
        "data": data.toJson(),
      };
}

class Error {
  List<String> email;
  List<String> phone;
  List<String> password;

}

class UserData {
  String name;
  String email;
  String phone;

  UserData({
    this.name,
    this.email,
    this.phone,
  });

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "phone": phone,
      };
}
