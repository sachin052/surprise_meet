import 'dart:convert';

ReportUserResponse reportUserResponseFromJson(String str) => ReportUserResponse.fromJson(json.decode(str));

String reportUserResponseToJson(ReportUserResponse data) => json.encode(data.toJson());

class ReportUserResponse {
  ReportUserResponse({
    this.valid,
    this.message,
    this.code,
    this.data,
  });

  final bool valid;
  final String message;
  final int code;
  final Data data;

  factory ReportUserResponse.fromJson(Map<String, dynamic> json) => ReportUserResponse(
    valid: json["valid"] == null ? null : json["valid"],
    message: json["message"] == null ? null : json["message"],
    code: json["code"] == null ? null : json["code"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid == null ? null : valid,
    "message": message == null ? null : message,
    "code": code == null ? null : code,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.email,
  });

  final String email;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    email: json["email"] == null ? null : json["email"],
  );

  Map<String, dynamic> toJson() => {
    "email": email == null ? null : email,
  };
}
