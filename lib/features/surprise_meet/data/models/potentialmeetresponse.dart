// To parse this JSON data, do
//
//     final potentialMeetResponse = potentialMeetResponseFromJson(jsonString);

import 'dart:convert';

PotentialMeetResponse potentialMeetResponseFromJson(String str) => PotentialMeetResponse.fromJson(json.decode(str));

String potentialMeetResponseToJson(PotentialMeetResponse data) => json.encode(data.toJson());

class PotentialMeetResponse {
  bool valid;
  String message;
  int code;
  int unreadNotificationCount;
  List<Meet> meets;

  PotentialMeetResponse({
    this.valid,
    this.message,
    this.code,
    this.meets,
    this.unreadNotificationCount
  });

  factory PotentialMeetResponse.fromJson(Map<String, dynamic> json) => PotentialMeetResponse(
    valid: json["valid"],
    message: json["message"],
    code: json["code"],
    unreadNotificationCount: json["unread_notification_count"],
    meets: json["data"].isNotEmpty?List<Meet>.from(json["data"].where((x)=>x!=null).map((x) => Meet.fromJson(x))):null,
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "message": message,
    "code": code,
    "unread_notification_count":unreadNotificationCount,
    "meets": List<dynamic>.from(meets.map((x) => x.toJson())),
  };
}

class Meet {
  int id;
  String facebookUsername;
  String instaHandler;
  String phone;
  String email;
  String name;

  Meet({
    this.id,
    this.facebookUsername,
    this.instaHandler,
    this.phone,
    this.email,
    this.name,
  });

  factory Meet.fromJson(Map<String, dynamic> json) => Meet(
    id: json["id"],
    facebookUsername: json["facebook_username"] == null ? null : json["facebook_username"]??"",
    instaHandler: json["insta_handler"] == null ? null : json["insta_handler"]??"",
    phone: json["phone"],
    email: json["email"]??"",
    name: json["name"]??"",
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "facebook_username": facebookUsername == null ? null : facebookUsername,
    "insta_handler": instaHandler == null ? null : instaHandler,
    "phone": phone,
    "email": email,
    "name":name
  };
}
