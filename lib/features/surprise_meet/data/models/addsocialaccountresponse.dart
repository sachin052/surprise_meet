import 'dart:convert';

import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';

AddSocialAccountResponse addSocialAccountResponseFromJson(String str) => AddSocialAccountResponse.fromJson(json.decode(str));

String addSocialAccountResponseToJson(AddSocialAccountResponse data) => json.encode(data.toJson());

class AddSocialAccountResponse {
  bool valid;
  String message;
  int code;
  Userprofile data;

  AddSocialAccountResponse({
    this.valid,
    this.message,
    this.code,
    this.data,
  });

  factory AddSocialAccountResponse.fromJson(Map<String, dynamic> json) => AddSocialAccountResponse(
    valid: json["valid"],
    message: json["message"],
    code: json["code"],
    data: Userprofile.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "message": message,
    "code": code,
    "data": data,
  };
}
