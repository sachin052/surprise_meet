import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/authrepo/authrepodatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/addsocialaccountresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/resetpasswordresponse.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/signupresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/loginEntity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/authrepo.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/socialrequesttype.dart';
import 'package:surprise_meet/main.dart';

class AuthRepoImpl extends AuthRepo {
  final AuthRepoDataSource authRepoDataSource;
  final SharedPrefHelper preference;

  AuthRepoImpl(
    this.authRepoDataSource,
    this.preference,
  );

  @override
  Future<Either<Failure, LoginEntity>> signIn(Map<String, String> body) async {
    try {
      var response = await authRepoDataSource.signIn(body);
      await preference.saveUserData(response.userData);
      return Right(
          LoginEntity(response.userData.isFacebook, response.userData.isInsta));
//    } on FormatException{
//      return Left(ServerFailure("Error in json"));
//    }
//    on NoSuchMethodError{
//      return Left(ServerFailure("Something went wrong in parsing"));
//    }
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }

  @override
  Future<Either<Failure, SignUpResponse>> signUp(
      Map<String, String> body) async {
    try {
      var response = await authRepoDataSource.signUp(body);
      return Right(response);
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
//    return null;
  }

  @override
  Future<Either<Failure, ResetPasswordResponse>> resetPassword(
      String email) async {
    try {
      var response = await authRepoDataSource.forgotPassword(email);
      return Right(response);
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }

  @override
  Future<Either<Failure, LoginEntity>> loginUsingFaceBook(
      Map<String, String> map) async {
    try {
      var response = await authRepoDataSource.loginUsingFaceBook(map);
      await preference.saveUserData(response.userData);
      return Right(
          LoginEntity(response.userData.isFacebook, response.userData.isInsta));
    } on ServerFailure {
      return Left(ServerFailure("Error in json"));
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }

  @override
  Future<Either<Failure, LoginEntity>> loginUsingInsta(String authToken) async {
    try {
      var response = await authRepoDataSource.loginUsingInsta(authToken);
      await preference.saveUserData(response.userData);
      return Right(
          LoginEntity(response.userData.isFacebook, response.userData.isInsta));
    } on FormatException {
      return Left(ServerFailure("Error in json"));
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }

  @override
  Future<Either<Failure, AddSocialAccountResponse>> addFacebookWithProfile(
      SocialRequestType requestType) async {
    try {
      var response =
          await authRepoDataSource.addFacebookWithProfile(requestType);
      var userData = preference.getUserData();
      userData.isFacebook = true;
      // updating user data locally
      await preference.saveUserData(userData);
      return Right(response);
    } on FormatException {
      return Left(ServerFailure("Error in json"));
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }

  @override
  Future<Either<Failure, AddSocialAccountResponse>> addInstaWithProfile(
      SocialRequestType requestType) async {
    try {
      var response = await authRepoDataSource.addInstaWithProfile(requestType);
      var userData = preference.getUserData();
      userData.isInsta = true;
      // updating user data locally
      await preference.saveUserData(userData);
      return Right(response);
    } on FormatException {
      return Left(ServerFailure("Error in json"));
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }

  @override
  Future<Either<Failure, dynamic>> logOut() async {
    try {
      var response = await authRepoDataSource.logOut();
      // updating user data locally
      await preference.clearData();
      // get fcm and device id for new user if he logged in again using same session
//      await di.init();
      await handlePushNotification();
//      initBloc();
      MyRouter.navigatorKey.currentState.pushNamedAndRemoveUntil(MyRouter.loginPage, (MyRouter)=>false);
//      di.injector.unregister();

      return Right(response);
    } on FormatException {
      return Left(ServerFailure("Error in json"));
    } catch (e) {
//      di.injector.unregister();
      main();
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }

  @override
  Future<Either<Failure, LoginEntity>> checkFacebookUsername(
      String authToken) async {
    try {
      var response = await authRepoDataSource.checkFacebookUsername(authToken);
//      await preference.saveUserData(response.userData);
      return Right(
          LoginEntity(response.userData.isFacebook, response.userData.isInsta));
    } on ServerFailure {
      return Left(ServerFailure("Error in json"));
    } on NoFacebookUserName {
      return Left(NoFacebookUserName(""));
    } catch (e) {
      var exception = e as DioError;
      exception.handleError;
      return Left(ServerFailure(exception.handleError));
//      print(e);
    }
  }
}
