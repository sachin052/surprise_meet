import 'package:dartz/dartz.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/userdatarepo/userdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/userdatarepo.dart';

class
UserDataRepoImpl extends UserDataRepo {
  final UserDataSource _userDataSource;

  UserDataRepoImpl(this._userDataSource);

  @override
  Future<Either<Failure, ListOfPotentialMeets>>
      getUserPotentialMeets() async {
    try {
      var response = await _userDataSource.getPotentialMeets();

      var meetList = List<PotentialMeetEntity>();
      if (response.meets.isNotEmpty) {
        response.meets.forEach((meet) {
          meetList.add(PotentialMeetEntity(
              meetId: meet.id,
              meetName: meet.name,
              emilId: meet.email,
              facebookId: meet.facebookUsername,
              instgramUsername: meet.instaHandler,
              phone: meet.phone!=null?meet?.phone?.toString():""));
        });
      }
      return Right(ListOfPotentialMeets(potentialMeetEntityList: meetList,notificationCount: response.unreadNotificationCount));
    } catch (e) {
      return handleApiErrors<ListOfPotentialMeets>(e);
//      print(e);
    }
  }

  @override
  Future<Either<Failure, PotentialMeetEntity>> addPotentialMeet(
      Map<String, dynamic> body) async {
    try {
      var addedMeet = await _userDataSource.addPotentialMeet(body);
      return Right(
          PotentialMeetEntity(meetName: addedMeet.name, meetId: addedMeet.id));
    } catch (e) {
      return handleApiErrors<PotentialMeetEntity>(e);
//      print(e);
    }
  }

  @override
  Future<Either<Failure, PotentialMeetEntity>> deletePotentialMeet(
      String meetID) async {
    try {
      var addedMeet = await _userDataSource.deletePotentialMeet(meetID);
      return Right(
          PotentialMeetEntity(meetName: addedMeet.name, meetId: addedMeet.id));
    } catch (e) {
      return handleApiErrors<PotentialMeetEntity>(e);
//      print(e);
    }
  }

  @override
  Future<Either<Failure, ProfileResponse>> getUserProfile() async {
    try {
      var response = await _userDataSource.getUserProfile();
      return Right(response);
    } catch (e) {
      return handleApiErrors<ProfileResponse>(e);
    }
  }

  @override
  Future<Either<Failure, PotentialMeetEntity>> updatePotentialMeet(
      Map<String, dynamic> body) async {
    try {
      var addedMeet = await _userDataSource.updatePotentialMeet(body);
      return Right(
          PotentialMeetEntity(meetName: addedMeet.name, meetId: addedMeet.id));
    } catch (e) {
      return handleApiErrors<PotentialMeetEntity>(e);
//      print(e);
    }
  }

  @override
  Future<Either<Failure, ProfileResponse>> updateUserProfile(Map<String, dynamic> body) async{
    try {
      var response = await _userDataSource.updateUserProfile(body);
      return Right(response);
    } catch (e) {
      return handleApiErrors<ProfileResponse>(e);
    }
  }

  @override
  Future<Either<Failure, dynamic>> updatePassword(Map<String, dynamic> body) async{
    try {
      var response = await _userDataSource.updatePassword(body);
      return Right(response);
    } catch (e) {
      return handleApiErrors<dynamic>(e);
//      print(e);
    }
  }
}
