import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/chat/chatrepodatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/report_user_response.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/selectchatresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/chatentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/repositories/chatrepo.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/error/failure.dart';

class ChatRepoImpl extends ChatRepo {
  final ChatDataSource chatRepoDataSource;

  ChatRepoImpl(this.chatRepoDataSource);

  @override
  Future<Either<Failure, List<ChatEntity>>> getChatList() async {
    try {
      var response = await chatRepoDataSource.getChatList();

      var meetList = List<ChatEntity>();
      if (response.chatItems.isNotEmpty) {
        response.chatItems.forEach((item) {
          meetList.add(ChatEntity.fromApiResponse(item));
        });
      }
      return Right(meetList);
    } catch (e) {
      return handleApiErrors<List<ChatEntity>>(e);
//      print(e);
    }
  }

  @override
  Future<Either<Failure, SelectedChatResponse>> getSelectedChatList(
      String connectionId) async {
    try {
      var response = await chatRepoDataSource.getSelectedChatList(connectionId);

      return Right(response);
    } catch (e) {
      return handleApiErrors<SelectedChatResponse>(e);
//      print(e);
    }
  }

  @override
  Future<Either<Failure, dynamic>> deleteRecentChat(String connectionId) async{
    try{
      var response = await chatRepoDataSource.deleteSelectedRecent(connectionId);
      return Right(response);
    }catch(e){
      return handleApiErrors<SelectedChatResponse>(e);
    }
  }

  @override
  Future<Either<Failure, ReportUserResponse>> reportUser(String userId)async {
    try{
      var response = await chatRepoDataSource.reportUser(userId);
      return Right(response);
    }catch(e){
      if(e is DioError){
        if(e.type == DioErrorType.RESPONSE){
          return Left(ServerFailure("${e.response.data["error"]["error"]}"));
        }else{
          return handleApiErrors<ReportUserResponse>(e);
        }
      }
      return handleApiErrors<ReportUserResponse>(e);
    }
  }
}
