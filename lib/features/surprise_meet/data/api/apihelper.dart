import 'package:dio/dio.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';

class ApiHelper {
  final Dio dio;
  final SharedPrefHelper helper;

  Map<String, String> requestHeaders = {
    'Accept': 'application/json',
  };

  ApiHelper(this.dio, this.helper) {
    setUpNetworkClient();
    //TODO: Add header setup
  }

  void setUpNetworkClient() async {
    dio.options.baseUrl = ApiConstants.baseUrl;
    print("base url is " + ApiConstants.baseUrl);
    dio.options.headers = requestHeaders;
    dio.interceptors.add(
        LogInterceptor(responseBody: true, requestBody: true, request: true));
    var userData = helper.getUserData();
    if (userData != null) {
      requestHeaders["Authorization"] = "Bearer ${userData.authToken}";
    }
  }
}

class ApiConstants {
  static String devIp="165.227.208.94";
  static String liveIp="67.205.167.76";
  // Live URl
  static String baseIp="67.205.167.76";
  // Dev URL
 // static String baseIp="165.227.208.94";
//  static String baseIp="0.0.0.0";
//  static String baseIp = "192.168.1.101";
  static String baseUrl = "http://$baseIp:8000/surprisemeet/";
  static String chatUrl = "ws://$baseIp:8000/ws/chat";
  static String privacyPolicy="${baseUrl}privacy_policy/";
  static String termsCondition="${baseUrl}terms_conditions/";
  static String aboutUs="${baseUrl}about_us/";
}
