import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';

part 'profile_event.g.dart';

@superEnum
enum _UpdateProfileEvent {
  @object
  AddFacebook,
  @Data(fields: [DataField<bool>("addProfile")])
  LoginWithFbUsername,
  @Data(fields: [DataField<String>("token")])
  AddInsta,
  @object
  UpdateFacebook,
  @Data(fields: [DataField<String>("token")])
  UpdateInsta,
  @object
  GetUserProfile,
  @object
  LogOutUser,
  @object
  UpdateUserProfile,
  @object
  UpdatePassword
}
