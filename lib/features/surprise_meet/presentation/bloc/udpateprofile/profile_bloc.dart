import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:rxdart/rxdart.dart';
import 'package:super_enum/super_enum.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/getprofileusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/addfacebookusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/addinstgramusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/checkfbusernameusercase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/logoutusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/updatepasswordusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/updateuserprofileusecaes.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/basebloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/socialrequesttype.dart';
import 'package:surprise_meet/main.dart';

import './bloc.dart';

class UpdateProfileBloc extends Bloc<UpdateProfileEvent, UpdateProfileState>
    implements BaseBloc {
// Use Cases
  final AddUpdateFacebookUseCase _addFacebookUseCase;
  final CheckFBUserNameUseCase _checkFBUserNameUseCase;
  final AddUpdateInstaUseCase _addInstaUseCase;
  final GetProfileUseCase _getProfileUseCase;
  final UpdateUserProfileUseCase _updateUserProfileUseCase;
  final LogOutUseCase _logOutUseCase;
  final UpdatePasswordUseCase _updatePasswordUseCase;
  // Controllers
  TextEditingController nameController;
  TextEditingController phoneController;
  TextEditingController emailController;
  TextEditingController facebookController;
  TextEditingController instaController;

  final _fbUserNameController = BehaviorSubject<String>();
  final countryCodeController = BehaviorSubject<Country>.seeded(Country(phoneCode: "1"));
  Function(Country) get changeCountryCode => countryCodeController.sink.add;
  Stream<Country> get countryCode=>countryCodeController.stream;
  String _fbToken;

  // old password
  final _oldPasswordController = BehaviorSubject<String>();
  TextEditingController oldPassWordEditingController=TextEditingController();
  Function(String) get changeOldPassword => _oldPasswordController.sink.add;
  Stream<String> get oldPassword =>
      _oldPasswordController.stream.transform(validatePassword);


  // Password Field
  final _newPasswordController = BehaviorSubject<String>();
  TextEditingController newPassWordEditingController=TextEditingController();
  Function(String) get changeNewPassword => _newPasswordController.sink.add;
  Stream<String> get newPassword =>
      _newPasswordController.stream.transform(validatePassword);


  //Confirm Password
  final _cPasswordController = BehaviorSubject<String>();
  TextEditingController cPassWordEditingController=TextEditingController();
  Function(String) get changeConfirmPassword => _cPasswordController.sink.add;
  // Used combineLatest2Stream for validations
  Stream<String> get confirmPassword =>
      CombineLatestStream.combine2(_newPasswordController, _cPasswordController,
              (password, cpassword) {
            if (password == cpassword) return cpassword;
            _cPasswordController.addError("Password Mismatched");
            return null;
          });
  bool _isValid;
Stream<bool> get isValid=>CombineLatestStream.combine3<String,String,String,bool>(oldPassword, confirmPassword, newPassword, (a,b,c){
  if(a==null||b==null||c==null)return false;
  else if(a.isNotEmpty&&a.isValidPass&&b.isNotEmpty&&b.isValidPass&&c.isNotEmpty&&c.isValidPass&&b==c)return true;
  return false;
}).asBroadcastStream();
  UpdateProfileBloc(
      this._addFacebookUseCase,
      this._addInstaUseCase,
      this._getProfileUseCase,
      this._updateUserProfileUseCase,
      this._logOutUseCase,
      this._checkFBUserNameUseCase, this._updatePasswordUseCase) {
    nameController = TextEditingController();
    phoneController = TextEditingController();
    emailController = TextEditingController();
    facebookController = TextEditingController();
    instaController = TextEditingController();
    isValid.listen((value)=>_isValid=value);
//    countryCodeController.add(Country());
  }

  @override
  UpdateProfileState get initialState =>
      UpdateProfileState.intialProfileState();

  TextEditingController userNameController = TextEditingController();

  Stream<String> get userName =>
      _fbUserNameController.stream.transform(notEmptyValidator);

  Function(String) get changeUserName => _fbUserNameController.sink.add;

  @override
  Stream<UpdateProfileState> mapEventToState(
    UpdateProfileEvent event,
  ) async* {
    if (event is AddFacebook) {
      yield* mapFacebookEventToStream(true);
    } else if (event is LoginWithFbUsername) {
      if (_fbUserNameController.value?.isEmpty == true) {
        _fbUserNameController.sink.addError("Must not be empty");
        return;
      }
      yield* fbLogin({
        "fb_access_token": _fbToken,
        "fb_user_name": _fbUserNameController.value
      }, event.addProfile);
    } else if (event is UpdateFacebook) {
      yield* mapFacebookEventToStream(false);
    } else if (event is AddInsta) {
      yield* mapInstaEventToStream(true, event.token);
    } else if (event is UpdateInsta) {
      yield* mapInstaEventToStream(false, event.token);
    } else if (event is GetUserProfile) {
      yield UpdateProfileState.loadingProfileState();
      yield* mapGetProfileEventToStream();
    } else if (event is UpdateUserProfile) {
      yield UpdateProfileState.loadingProfileState();
        var map = {
          "name": nameController.text,
          "phone": phoneController.text.toString().isNotEmpty
              ? "+"+countryCodeController.value.phoneCode.replaceAll("-","").replaceAll("+","")+"-"+phoneController.text
              : ""
        };
        yield* mapUpdateProfileEventToStream(map);

    } else if (event is LogOutUser) {
      notification.add("reset");
      yield UpdateProfileState.loadingProfileState();
      await _logOutUseCase("");
    }
    else if(event is UpdatePassword){

       yield* updatePasswordMap(_isValid);

    }
  }

  Stream<UpdateProfileState> mapFacebookEventToStream(
      bool addNewProfile) async* {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        var response = await _checkFBUserNameUseCase(result.accessToken.token);
        _fbToken = result.accessToken.token;
        var apiResponse = response.fold(
            (failure) => failure is NoFacebookUserName
                ? UpdateProfileState.getFbUserName()
                : UpdateProfileState.errorProfileState(
                    error: failure.errorMessage), (success){
              facebookLogin.logOut();
          return UpdateProfileState.fbUserNameRegistered();
        });
        if (apiResponse == UpdateProfileState.fbUserNameRegistered()) {
          var map = {
            "fb_access_token": _fbToken,
          };
          yield* fbLogin(map, addNewProfile);
        } else {
          yield apiResponse;
        }

        break;
      case FacebookLoginStatus.cancelledByUser:
        print("user canelled");
//        yield UpdateProfileState.errorProfileState(error: "User Cancelled");
        break;
      case FacebookLoginStatus.error:
        print("error is" + result.errorMessage);
        yield UpdateProfileState.errorProfileState(error: result.errorMessage);
        break;
    }
  }

  Stream<UpdateProfileState> mapInstaEventToStream(
      bool addNewProfile, String token) async* {
    if (token != null) {
      var response = await _addInstaUseCase(addNewProfile
          ? SocialRequestType.addSocialAccount(
              body: {"insta_access_token": token})
          : SocialRequestType.updateSocialAccount(
              body: {"insta_access_token": token}));
      yield response.fold(
          (failure) =>
              UpdateProfileState.errorProfileState(error: failure.errorMessage),
          (success) => UpdateProfileState.instaVerified());
    } else {
      yield UpdateProfileState.errorProfileState(
          error: "Failed to connect instagram");
    }
  }

  Stream<UpdateProfileState> mapGetProfileEventToStream() async* {
    var response = await _getProfileUseCase("");
    yield response.fold(
        (failure) =>
            UpdateProfileState.errorProfileState(error: failure.errorMessage),
        (success) => UpdateProfileState.successProfileState(data: success));
  }

  Stream<UpdateProfileState> mapUpdateProfileEventToStream(
      Map<String, String> body) async* {
    var response = await _updateUserProfileUseCase(body);
    yield response.fold(
        (failure) =>
            UpdateProfileState.errorProfileState(error: failure.errorMessage),
        (success) => UpdateProfileState.successProfileState(data: success));
  }

  Stream<UpdateProfileState> fbLogin(
      Map<String, String> body, bool addNewProfile) async* {
    var response = await _addFacebookUseCase(addNewProfile
        ? SocialRequestType.addSocialAccount(body: body)
        : SocialRequestType.updateSocialAccount(body: body));
    yield response.fold(
        (failure) =>
            UpdateProfileState.errorProfileState(error: failure.errorMessage),
        (success) => UpdateProfileState.faceBookVerified());
  }

  disposeUpdatePassStreams(){

  }

  Stream<UpdateProfileState> updatePasswordMap(bool value)async*{
    if(value==null){
      if(_oldPasswordController.stream.value==null||_oldPasswordController.stream.value.isEmpty){
        yield UpdateProfileState.errorProfileState(error: "Old Password must not be empty");
        yield UpdateProfileState.intialProfileState();
      }
      else if(_newPasswordController.stream.value==null||_newPasswordController.stream.value.isEmpty){
        yield UpdateProfileState.errorProfileState(error: "New Password must not be empty");
        yield UpdateProfileState.intialProfileState();
      }
      else if(_cPasswordController.stream.value==null||_cPasswordController.stream.value.isEmpty){
        yield UpdateProfileState.errorProfileState(error: "Confirm Password must not be empty");
        yield UpdateProfileState.intialProfileState();
      }
    }
    else{
        var map={
          "password":_oldPasswordController.stream.value,
          "new_password":_newPasswordController.stream.value
        };
        yield UpdateProfileState.loadingProfileState();
        var response=await _updatePasswordUseCase(map);
        yield response.fold(
                (failure)=>UpdateProfileState.errorProfileState(error: failure.errorMessage),
            (success)=>UpdateProfileState.successProfileState(data: null));
    }
  }
  addEmptyError(){
    if (oldPassWordEditingController.text.isEmpty) {
      _oldPasswordController.addError("Old password must not be empty");
    }
    if(newPassWordEditingController.text.isEmpty){
      _newPasswordController.addError("New Password must not be empty");
    }
    if(cPassWordEditingController.text.isEmpty){
      _cPasswordController.addError("Confirm Password must not be empty");
    }
  }

  @override
  void dispose() {
    _fbUserNameController.close();
    countryCodeController.close();
    _cPasswordController.close();
    _newPasswordController.close();
    _oldPasswordController.close();
  }
}
