// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_state.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class UpdateProfileState extends Equatable {
  const UpdateProfileState(this._type);

  factory UpdateProfileState.intialProfileState() = IntialProfileState;

  factory UpdateProfileState.fbUserNameRegistered() = FbUserNameRegistered;

  factory UpdateProfileState.getFbUserName() = GetFbUserName;

  factory UpdateProfileState.loadingProfileState() = LoadingProfileState;

  factory UpdateProfileState.errorProfileState({@required String error}) =
      ErrorProfileState;

  factory UpdateProfileState.faceBookVerified() = FaceBookVerified;

  factory UpdateProfileState.instaVerified() = InstaVerified;

  factory UpdateProfileState.successProfileState(
      {@required ProfileResponse data}) = SuccessProfileState;

  final _UpdateProfileState _type;

//ignore: missing_return
  R when<R>(
      {@required
          FutureOr<R> Function(IntialProfileState) intialProfileState,
      @required
          FutureOr<R> Function(FbUserNameRegistered) fbUserNameRegistered,
      @required
          FutureOr<R> Function(GetFbUserName) getFbUserName,
      @required
          FutureOr<R> Function(LoadingProfileState) loadingProfileState,
      @required
          FutureOr<R> Function(ErrorProfileState) errorProfileState,
      @required
          FutureOr<R> Function(FaceBookVerified) faceBookVerified,
      @required
          FutureOr<R> Function(InstaVerified) instaVerified,
      @required
          FutureOr<R> Function(SuccessProfileState) successProfileState}) {
    assert(() {
      if (intialProfileState == null ||
          fbUserNameRegistered == null ||
          getFbUserName == null ||
          loadingProfileState == null ||
          errorProfileState == null ||
          faceBookVerified == null ||
          instaVerified == null ||
          successProfileState == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _UpdateProfileState.IntialProfileState:
        return intialProfileState(this as IntialProfileState);
      case _UpdateProfileState.FbUserNameRegistered:
        return fbUserNameRegistered(this as FbUserNameRegistered);
      case _UpdateProfileState.GetFbUserName:
        return getFbUserName(this as GetFbUserName);
      case _UpdateProfileState.LoadingProfileState:
        return loadingProfileState(this as LoadingProfileState);
      case _UpdateProfileState.ErrorProfileState:
        return errorProfileState(this as ErrorProfileState);
      case _UpdateProfileState.FaceBookVerified:
        return faceBookVerified(this as FaceBookVerified);
      case _UpdateProfileState.InstaVerified:
        return instaVerified(this as InstaVerified);
      case _UpdateProfileState.SuccessProfileState:
        return successProfileState(this as SuccessProfileState);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(IntialProfileState) intialProfileState,
      FutureOr<R> Function(FbUserNameRegistered) fbUserNameRegistered,
      FutureOr<R> Function(GetFbUserName) getFbUserName,
      FutureOr<R> Function(LoadingProfileState) loadingProfileState,
      FutureOr<R> Function(ErrorProfileState) errorProfileState,
      FutureOr<R> Function(FaceBookVerified) faceBookVerified,
      FutureOr<R> Function(InstaVerified) instaVerified,
      FutureOr<R> Function(SuccessProfileState) successProfileState,
      @required FutureOr<R> Function(UpdateProfileState) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _UpdateProfileState.IntialProfileState:
        if (intialProfileState == null) break;
        return intialProfileState(this as IntialProfileState);
      case _UpdateProfileState.FbUserNameRegistered:
        if (fbUserNameRegistered == null) break;
        return fbUserNameRegistered(this as FbUserNameRegistered);
      case _UpdateProfileState.GetFbUserName:
        if (getFbUserName == null) break;
        return getFbUserName(this as GetFbUserName);
      case _UpdateProfileState.LoadingProfileState:
        if (loadingProfileState == null) break;
        return loadingProfileState(this as LoadingProfileState);
      case _UpdateProfileState.ErrorProfileState:
        if (errorProfileState == null) break;
        return errorProfileState(this as ErrorProfileState);
      case _UpdateProfileState.FaceBookVerified:
        if (faceBookVerified == null) break;
        return faceBookVerified(this as FaceBookVerified);
      case _UpdateProfileState.InstaVerified:
        if (instaVerified == null) break;
        return instaVerified(this as InstaVerified);
      case _UpdateProfileState.SuccessProfileState:
        if (successProfileState == null) break;
        return successProfileState(this as SuccessProfileState);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(IntialProfileState) intialProfileState,
      FutureOr<void> Function(FbUserNameRegistered) fbUserNameRegistered,
      FutureOr<void> Function(GetFbUserName) getFbUserName,
      FutureOr<void> Function(LoadingProfileState) loadingProfileState,
      FutureOr<void> Function(ErrorProfileState) errorProfileState,
      FutureOr<void> Function(FaceBookVerified) faceBookVerified,
      FutureOr<void> Function(InstaVerified) instaVerified,
      FutureOr<void> Function(SuccessProfileState) successProfileState}) {
    assert(() {
      if (intialProfileState == null &&
          fbUserNameRegistered == null &&
          getFbUserName == null &&
          loadingProfileState == null &&
          errorProfileState == null &&
          faceBookVerified == null &&
          instaVerified == null &&
          successProfileState == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _UpdateProfileState.IntialProfileState:
        if (intialProfileState == null) break;
        return intialProfileState(this as IntialProfileState);
      case _UpdateProfileState.FbUserNameRegistered:
        if (fbUserNameRegistered == null) break;
        return fbUserNameRegistered(this as FbUserNameRegistered);
      case _UpdateProfileState.GetFbUserName:
        if (getFbUserName == null) break;
        return getFbUserName(this as GetFbUserName);
      case _UpdateProfileState.LoadingProfileState:
        if (loadingProfileState == null) break;
        return loadingProfileState(this as LoadingProfileState);
      case _UpdateProfileState.ErrorProfileState:
        if (errorProfileState == null) break;
        return errorProfileState(this as ErrorProfileState);
      case _UpdateProfileState.FaceBookVerified:
        if (faceBookVerified == null) break;
        return faceBookVerified(this as FaceBookVerified);
      case _UpdateProfileState.InstaVerified:
        if (instaVerified == null) break;
        return instaVerified(this as InstaVerified);
      case _UpdateProfileState.SuccessProfileState:
        if (successProfileState == null) break;
        return successProfileState(this as SuccessProfileState);
    }
  }

  @override
  List get props => const [];
}

@immutable
class IntialProfileState extends UpdateProfileState {
  const IntialProfileState._() : super(_UpdateProfileState.IntialProfileState);

  factory IntialProfileState() {
    _instance ??= IntialProfileState._();
    return _instance;
  }

  static IntialProfileState _instance;
}

@immutable
class FbUserNameRegistered extends UpdateProfileState {
  const FbUserNameRegistered._()
      : super(_UpdateProfileState.FbUserNameRegistered);

  factory FbUserNameRegistered() {
    _instance ??= FbUserNameRegistered._();
    return _instance;
  }

  static FbUserNameRegistered _instance;
}

@immutable
class GetFbUserName extends UpdateProfileState {
  const GetFbUserName._() : super(_UpdateProfileState.GetFbUserName);

  factory GetFbUserName() {
    _instance ??= GetFbUserName._();
    return _instance;
  }

  static GetFbUserName _instance;
}

@immutable
class LoadingProfileState extends UpdateProfileState {
  const LoadingProfileState._()
      : super(_UpdateProfileState.LoadingProfileState);

  factory LoadingProfileState() {
    _instance ??= LoadingProfileState._();
    return _instance;
  }

  static LoadingProfileState _instance;
}

@immutable
class ErrorProfileState extends UpdateProfileState {
  const ErrorProfileState({@required this.error})
      : super(_UpdateProfileState.ErrorProfileState);

  final String error;

  @override
  String toString() => 'ErrorProfileState(error:${this.error})';
  @override
  List get props => [error];
}

@immutable
class FaceBookVerified extends UpdateProfileState {
  const FaceBookVerified._() : super(_UpdateProfileState.FaceBookVerified);

  factory FaceBookVerified() {
    _instance ??= FaceBookVerified._();
    return _instance;
  }

  static FaceBookVerified _instance;
}

@immutable
class InstaVerified extends UpdateProfileState {
  const InstaVerified._() : super(_UpdateProfileState.InstaVerified);

  factory InstaVerified() {
    _instance ??= InstaVerified._();
    return _instance;
  }

  static InstaVerified _instance;
}

@immutable
class SuccessProfileState extends UpdateProfileState {
  const SuccessProfileState({@required this.data})
      : super(_UpdateProfileState.SuccessProfileState);

  final ProfileResponse data;

  @override
  String toString() => 'SuccessProfileState(data:${this.data})';
  @override
  List get props => [data];
}
