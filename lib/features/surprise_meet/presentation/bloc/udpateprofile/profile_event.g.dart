// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_event.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class UpdateProfileEvent extends Equatable {
  const UpdateProfileEvent(this._type);

  factory UpdateProfileEvent.addFacebook() = AddFacebook;

  factory UpdateProfileEvent.loginWithFbUsername({@required bool addProfile}) =
      LoginWithFbUsername;

  factory UpdateProfileEvent.addInsta({@required String token}) = AddInsta;

  factory UpdateProfileEvent.updateFacebook() = UpdateFacebook;

  factory UpdateProfileEvent.updateInsta({@required String token}) =
      UpdateInsta;

  factory UpdateProfileEvent.getUserProfile() = GetUserProfile;

  factory UpdateProfileEvent.logOutUser() = LogOutUser;

  factory UpdateProfileEvent.updateUserProfile() = UpdateUserProfile;

  factory UpdateProfileEvent.updatePassword() = UpdatePassword;

  final _UpdateProfileEvent _type;

//ignore: missing_return
  R when<R>(
      {@required FutureOr<R> Function(AddFacebook) addFacebook,
      @required FutureOr<R> Function(LoginWithFbUsername) loginWithFbUsername,
      @required FutureOr<R> Function(AddInsta) addInsta,
      @required FutureOr<R> Function(UpdateFacebook) updateFacebook,
      @required FutureOr<R> Function(UpdateInsta) updateInsta,
      @required FutureOr<R> Function(GetUserProfile) getUserProfile,
      @required FutureOr<R> Function(LogOutUser) logOutUser,
      @required FutureOr<R> Function(UpdateUserProfile) updateUserProfile,
      @required FutureOr<R> Function(UpdatePassword) updatePassword}) {
    assert(() {
      if (addFacebook == null ||
          loginWithFbUsername == null ||
          addInsta == null ||
          updateFacebook == null ||
          updateInsta == null ||
          getUserProfile == null ||
          logOutUser == null ||
          updateUserProfile == null ||
          updatePassword == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _UpdateProfileEvent.AddFacebook:
        return addFacebook(this as AddFacebook);
      case _UpdateProfileEvent.LoginWithFbUsername:
        return loginWithFbUsername(this as LoginWithFbUsername);
      case _UpdateProfileEvent.AddInsta:
        return addInsta(this as AddInsta);
      case _UpdateProfileEvent.UpdateFacebook:
        return updateFacebook(this as UpdateFacebook);
      case _UpdateProfileEvent.UpdateInsta:
        return updateInsta(this as UpdateInsta);
      case _UpdateProfileEvent.GetUserProfile:
        return getUserProfile(this as GetUserProfile);
      case _UpdateProfileEvent.LogOutUser:
        return logOutUser(this as LogOutUser);
      case _UpdateProfileEvent.UpdateUserProfile:
        return updateUserProfile(this as UpdateUserProfile);
      case _UpdateProfileEvent.UpdatePassword:
        return updatePassword(this as UpdatePassword);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(AddFacebook) addFacebook,
      FutureOr<R> Function(LoginWithFbUsername) loginWithFbUsername,
      FutureOr<R> Function(AddInsta) addInsta,
      FutureOr<R> Function(UpdateFacebook) updateFacebook,
      FutureOr<R> Function(UpdateInsta) updateInsta,
      FutureOr<R> Function(GetUserProfile) getUserProfile,
      FutureOr<R> Function(LogOutUser) logOutUser,
      FutureOr<R> Function(UpdateUserProfile) updateUserProfile,
      FutureOr<R> Function(UpdatePassword) updatePassword,
      @required FutureOr<R> Function(UpdateProfileEvent) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _UpdateProfileEvent.AddFacebook:
        if (addFacebook == null) break;
        return addFacebook(this as AddFacebook);
      case _UpdateProfileEvent.LoginWithFbUsername:
        if (loginWithFbUsername == null) break;
        return loginWithFbUsername(this as LoginWithFbUsername);
      case _UpdateProfileEvent.AddInsta:
        if (addInsta == null) break;
        return addInsta(this as AddInsta);
      case _UpdateProfileEvent.UpdateFacebook:
        if (updateFacebook == null) break;
        return updateFacebook(this as UpdateFacebook);
      case _UpdateProfileEvent.UpdateInsta:
        if (updateInsta == null) break;
        return updateInsta(this as UpdateInsta);
      case _UpdateProfileEvent.GetUserProfile:
        if (getUserProfile == null) break;
        return getUserProfile(this as GetUserProfile);
      case _UpdateProfileEvent.LogOutUser:
        if (logOutUser == null) break;
        return logOutUser(this as LogOutUser);
      case _UpdateProfileEvent.UpdateUserProfile:
        if (updateUserProfile == null) break;
        return updateUserProfile(this as UpdateUserProfile);
      case _UpdateProfileEvent.UpdatePassword:
        if (updatePassword == null) break;
        return updatePassword(this as UpdatePassword);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(AddFacebook) addFacebook,
      FutureOr<void> Function(LoginWithFbUsername) loginWithFbUsername,
      FutureOr<void> Function(AddInsta) addInsta,
      FutureOr<void> Function(UpdateFacebook) updateFacebook,
      FutureOr<void> Function(UpdateInsta) updateInsta,
      FutureOr<void> Function(GetUserProfile) getUserProfile,
      FutureOr<void> Function(LogOutUser) logOutUser,
      FutureOr<void> Function(UpdateUserProfile) updateUserProfile,
      FutureOr<void> Function(UpdatePassword) updatePassword}) {
    assert(() {
      if (addFacebook == null &&
          loginWithFbUsername == null &&
          addInsta == null &&
          updateFacebook == null &&
          updateInsta == null &&
          getUserProfile == null &&
          logOutUser == null &&
          updateUserProfile == null &&
          updatePassword == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _UpdateProfileEvent.AddFacebook:
        if (addFacebook == null) break;
        return addFacebook(this as AddFacebook);
      case _UpdateProfileEvent.LoginWithFbUsername:
        if (loginWithFbUsername == null) break;
        return loginWithFbUsername(this as LoginWithFbUsername);
      case _UpdateProfileEvent.AddInsta:
        if (addInsta == null) break;
        return addInsta(this as AddInsta);
      case _UpdateProfileEvent.UpdateFacebook:
        if (updateFacebook == null) break;
        return updateFacebook(this as UpdateFacebook);
      case _UpdateProfileEvent.UpdateInsta:
        if (updateInsta == null) break;
        return updateInsta(this as UpdateInsta);
      case _UpdateProfileEvent.GetUserProfile:
        if (getUserProfile == null) break;
        return getUserProfile(this as GetUserProfile);
      case _UpdateProfileEvent.LogOutUser:
        if (logOutUser == null) break;
        return logOutUser(this as LogOutUser);
      case _UpdateProfileEvent.UpdateUserProfile:
        if (updateUserProfile == null) break;
        return updateUserProfile(this as UpdateUserProfile);
      case _UpdateProfileEvent.UpdatePassword:
        if (updatePassword == null) break;
        return updatePassword(this as UpdatePassword);
    }
  }

  @override
  List get props => const [];
}

@immutable
class AddFacebook extends UpdateProfileEvent {
  const AddFacebook._() : super(_UpdateProfileEvent.AddFacebook);

  factory AddFacebook() {
    _instance ??= AddFacebook._();
    return _instance;
  }

  static AddFacebook _instance;
}

@immutable
class LoginWithFbUsername extends UpdateProfileEvent {
  const LoginWithFbUsername({@required this.addProfile})
      : super(_UpdateProfileEvent.LoginWithFbUsername);

  final bool addProfile;

  @override
  String toString() => 'LoginWithFbUsername(addProfile:${this.addProfile})';
  @override
  List get props => [addProfile];
}

@immutable
class AddInsta extends UpdateProfileEvent {
  const AddInsta({@required this.token}) : super(_UpdateProfileEvent.AddInsta);

  final String token;

  @override
  String toString() => 'AddInsta(token:${this.token})';
  @override
  List get props => [token];
}

@immutable
class UpdateFacebook extends UpdateProfileEvent {
  const UpdateFacebook._() : super(_UpdateProfileEvent.UpdateFacebook);

  factory UpdateFacebook() {
    _instance ??= UpdateFacebook._();
    return _instance;
  }

  static UpdateFacebook _instance;
}

@immutable
class UpdateInsta extends UpdateProfileEvent {
  const UpdateInsta({@required this.token})
      : super(_UpdateProfileEvent.UpdateInsta);

  final String token;

  @override
  String toString() => 'UpdateInsta(token:${this.token})';
  @override
  List get props => [token];
}

@immutable
class GetUserProfile extends UpdateProfileEvent {
  const GetUserProfile._() : super(_UpdateProfileEvent.GetUserProfile);

  factory GetUserProfile() {
    _instance ??= GetUserProfile._();
    return _instance;
  }

  static GetUserProfile _instance;
}

@immutable
class LogOutUser extends UpdateProfileEvent {
  const LogOutUser._() : super(_UpdateProfileEvent.LogOutUser);

  factory LogOutUser() {
    _instance ??= LogOutUser._();
    return _instance;
  }

  static LogOutUser _instance;
}

@immutable
class UpdateUserProfile extends UpdateProfileEvent {
  const UpdateUserProfile._() : super(_UpdateProfileEvent.UpdateUserProfile);

  factory UpdateUserProfile() {
    _instance ??= UpdateUserProfile._();
    return _instance;
  }

  static UpdateUserProfile _instance;
}

@immutable
class UpdatePassword extends UpdateProfileEvent {
  const UpdatePassword._() : super(_UpdateProfileEvent.UpdatePassword);

  factory UpdatePassword() {
    _instance ??= UpdatePassword._();
    return _instance;
  }

  static UpdatePassword _instance;
}
