import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';

part 'profile_state.g.dart';

@superEnum
enum _UpdateProfileState {
  @object
  IntialProfileState,
  @object
  FbUserNameRegistered,
  @object
  GetFbUserName,
  @object
  LoadingProfileState,
  @Data(fields: [DataField<String>("error")])
  ErrorProfileState,
  @object
  FaceBookVerified,
  @object
  InstaVerified,
  @Data(fields: [DataField<ProfileResponse>("data")])
  SuccessProfileState
}
