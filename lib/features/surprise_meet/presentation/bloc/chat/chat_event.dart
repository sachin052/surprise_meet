import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';

@immutable
abstract class ChatEvent extends Equatable{

}

class GetChat extends ChatEvent{
  @override
  // TODO: implement props
  List<Object> get props => null;

}

class UpdateUserChat extends ChatEvent{
  final String connectionId;
  final String message;
  UpdateUserChat(this.connectionId, this.message);
  @override
  // TODO: implement props
  List<Object> get props => [];

}

class GetSelectedChat extends ChatEvent{
  final String connectionId;
  GetSelectedChat(this.connectionId);

  @override
  // TODO: implement props
  List<String> get props => [connectionId];
}

class DeleteRecentChat extends ChatEvent{
  final String connectionId;
  final int type;
  DeleteRecentChat(this.connectionId,{this.type=0});

  @override
  // TODO: implement props
  List<String> get props => [connectionId];
}

class ReportUser extends ChatEvent{
  final String userId;

  ReportUser(this.userId);

  @override
  List<String> get props => [userId];
}