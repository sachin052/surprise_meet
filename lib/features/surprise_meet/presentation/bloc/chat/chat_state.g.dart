// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_state.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class ChatState extends Equatable {
  const ChatState(this._type);

  factory ChatState.loadingChatState() = LoadingChatState;

  factory ChatState.initlaChatState() = InitlaChatState;

  factory ChatState.errorChatState({@required String error}) = ErrorChatState;

  factory ChatState.noDataChatState() = NoDataChatState;

  factory ChatState.successChatState({@required List<ChatEntity> items}) =
      SuccessChatState;

  factory ChatState.successSelectedChatState(
      {@required List<SelectedChatEntity> items}) = SuccessSelectedChatState;

  factory ChatState.userReportedState(
      {@required String message,
      @required String requestType}) = UserReportedState;

  factory ChatState.errorReportUserState({@required String error}) =
      ErrorReportUserState;

  final _ChatState _type;

//ignore: missing_return
  R when<R>(
      {@required
          FutureOr<R> Function(LoadingChatState) loadingChatState,
      @required
          FutureOr<R> Function(InitlaChatState) initlaChatState,
      @required
          FutureOr<R> Function(ErrorChatState) errorChatState,
      @required
          FutureOr<R> Function(NoDataChatState) noDataChatState,
      @required
          FutureOr<R> Function(SuccessChatState) successChatState,
      @required
          FutureOr<R> Function(SuccessSelectedChatState)
              successSelectedChatState,
      @required
          FutureOr<R> Function(UserReportedState) userReportedState,
      @required
          FutureOr<R> Function(ErrorReportUserState) errorReportUserState}) {
    assert(() {
      if (loadingChatState == null ||
          initlaChatState == null ||
          errorChatState == null ||
          noDataChatState == null ||
          successChatState == null ||
          successSelectedChatState == null ||
          userReportedState == null ||
          errorReportUserState == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _ChatState.LoadingChatState:
        return loadingChatState(this as LoadingChatState);
      case _ChatState.InitlaChatState:
        return initlaChatState(this as InitlaChatState);
      case _ChatState.ErrorChatState:
        return errorChatState(this as ErrorChatState);
      case _ChatState.NoDataChatState:
        return noDataChatState(this as NoDataChatState);
      case _ChatState.SuccessChatState:
        return successChatState(this as SuccessChatState);
      case _ChatState.SuccessSelectedChatState:
        return successSelectedChatState(this as SuccessSelectedChatState);
      case _ChatState.UserReportedState:
        return userReportedState(this as UserReportedState);
      case _ChatState.ErrorReportUserState:
        return errorReportUserState(this as ErrorReportUserState);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(LoadingChatState) loadingChatState,
      FutureOr<R> Function(InitlaChatState) initlaChatState,
      FutureOr<R> Function(ErrorChatState) errorChatState,
      FutureOr<R> Function(NoDataChatState) noDataChatState,
      FutureOr<R> Function(SuccessChatState) successChatState,
      FutureOr<R> Function(SuccessSelectedChatState) successSelectedChatState,
      FutureOr<R> Function(UserReportedState) userReportedState,
      FutureOr<R> Function(ErrorReportUserState) errorReportUserState,
      @required FutureOr<R> Function(ChatState) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _ChatState.LoadingChatState:
        if (loadingChatState == null) break;
        return loadingChatState(this as LoadingChatState);
      case _ChatState.InitlaChatState:
        if (initlaChatState == null) break;
        return initlaChatState(this as InitlaChatState);
      case _ChatState.ErrorChatState:
        if (errorChatState == null) break;
        return errorChatState(this as ErrorChatState);
      case _ChatState.NoDataChatState:
        if (noDataChatState == null) break;
        return noDataChatState(this as NoDataChatState);
      case _ChatState.SuccessChatState:
        if (successChatState == null) break;
        return successChatState(this as SuccessChatState);
      case _ChatState.SuccessSelectedChatState:
        if (successSelectedChatState == null) break;
        return successSelectedChatState(this as SuccessSelectedChatState);
      case _ChatState.UserReportedState:
        if (userReportedState == null) break;
        return userReportedState(this as UserReportedState);
      case _ChatState.ErrorReportUserState:
        if (errorReportUserState == null) break;
        return errorReportUserState(this as ErrorReportUserState);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(LoadingChatState) loadingChatState,
      FutureOr<void> Function(InitlaChatState) initlaChatState,
      FutureOr<void> Function(ErrorChatState) errorChatState,
      FutureOr<void> Function(NoDataChatState) noDataChatState,
      FutureOr<void> Function(SuccessChatState) successChatState,
      FutureOr<void> Function(SuccessSelectedChatState)
          successSelectedChatState,
      FutureOr<void> Function(UserReportedState) userReportedState,
      FutureOr<void> Function(ErrorReportUserState) errorReportUserState}) {
    assert(() {
      if (loadingChatState == null &&
          initlaChatState == null &&
          errorChatState == null &&
          noDataChatState == null &&
          successChatState == null &&
          successSelectedChatState == null &&
          userReportedState == null &&
          errorReportUserState == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _ChatState.LoadingChatState:
        if (loadingChatState == null) break;
        return loadingChatState(this as LoadingChatState);
      case _ChatState.InitlaChatState:
        if (initlaChatState == null) break;
        return initlaChatState(this as InitlaChatState);
      case _ChatState.ErrorChatState:
        if (errorChatState == null) break;
        return errorChatState(this as ErrorChatState);
      case _ChatState.NoDataChatState:
        if (noDataChatState == null) break;
        return noDataChatState(this as NoDataChatState);
      case _ChatState.SuccessChatState:
        if (successChatState == null) break;
        return successChatState(this as SuccessChatState);
      case _ChatState.SuccessSelectedChatState:
        if (successSelectedChatState == null) break;
        return successSelectedChatState(this as SuccessSelectedChatState);
      case _ChatState.UserReportedState:
        if (userReportedState == null) break;
        return userReportedState(this as UserReportedState);
      case _ChatState.ErrorReportUserState:
        if (errorReportUserState == null) break;
        return errorReportUserState(this as ErrorReportUserState);
    }
  }

  @override
  List get props => const [];
}

@immutable
class LoadingChatState extends ChatState {
  const LoadingChatState._() : super(_ChatState.LoadingChatState);

  factory LoadingChatState() {
    _instance ??= LoadingChatState._();
    return _instance;
  }

  static LoadingChatState _instance;
}

@immutable
class InitlaChatState extends ChatState {
  const InitlaChatState._() : super(_ChatState.InitlaChatState);

  factory InitlaChatState() {
    _instance ??= InitlaChatState._();
    return _instance;
  }

  static InitlaChatState _instance;
}

@immutable
class ErrorChatState extends ChatState {
  const ErrorChatState({@required this.error})
      : super(_ChatState.ErrorChatState);

  final String error;

  @override
  String toString() => 'ErrorChatState(error:${this.error})';
  @override
  List get props => [error];
}

@immutable
class NoDataChatState extends ChatState {
  const NoDataChatState._() : super(_ChatState.NoDataChatState);

  factory NoDataChatState() {
    _instance ??= NoDataChatState._();
    return _instance;
  }

  static NoDataChatState _instance;
}

@immutable
class SuccessChatState extends ChatState {
  const SuccessChatState({@required this.items})
      : super(_ChatState.SuccessChatState);

  final List<ChatEntity> items;

  @override
  String toString() => 'SuccessChatState(items:${this.items})';
  @override
  List get props => [items];
}

@immutable
class SuccessSelectedChatState extends ChatState {
  const SuccessSelectedChatState({@required this.items})
      : super(_ChatState.SuccessSelectedChatState);

  final List<SelectedChatEntity> items;

  @override
  String toString() => 'SuccessSelectedChatState(items:${this.items})';
  @override
  List get props => [items];
}

@immutable
class UserReportedState extends ChatState {
  const UserReportedState({@required this.message, @required this.requestType})
      : super(_ChatState.UserReportedState);

  final String message;

  final String requestType;

  @override
  String toString() =>
      'UserReportedState(message:${this.message},requestType:${this.requestType})';
  @override
  List get props => [message, requestType];
}

@immutable
class ErrorReportUserState extends ChatState {
  const ErrorReportUserState({@required this.error})
      : super(_ChatState.ErrorReportUserState);

  final String error;

  @override
  String toString() => 'ErrorReportUserState(error:${this.error})';
  @override
  List get props => [error];
}
