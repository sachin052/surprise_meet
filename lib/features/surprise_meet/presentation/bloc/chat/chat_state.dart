import 'package:super_enum/super_enum.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/chatentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/selectchatentity.dart';

part 'chat_state.g.dart';

@superEnum
enum _ChatState {
  @object
  LoadingChatState,
  @object
  InitlaChatState,
  @Data(fields: [DataField<String>("error")])
  ErrorChatState,
  @object
  NoDataChatState,
  @Data(fields: [DataField<List<ChatEntity>>("items")])
  SuccessChatState,
  @Data(fields: [DataField<List<SelectedChatEntity>>("items")])
  SuccessSelectedChatState,
  @Data(fields: [
    DataField<String>("message"),
    DataField<String>("requestType")
  ])
  UserReportedState,
  @Data(fields: [
    DataField<String>("error"),
  ])
  ErrorReportUserState,
}
