import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/selectchatresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/chatentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/selectchatentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/delete_recent_match.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/getchatusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/getselectedchatusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/report_user.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/basebloc.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;

import '../../../../../main.dart';
import './bloc.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> implements BaseBloc {
  // Use Case
  final GetChatUseCase _getChatUseCase;
  final GetSelectedChatUseCase _getSelectedChatUseCase;
  final DeleteRecentMatchUseCase deleteRecentMatch;
  final SharedPrefHelper _sharedPrefHelper;
  final ReportUserUseCase _reportUserUseCase;

  String _currentUserId;

  String get currentUserId => _currentUserId;

  String _chatRoomName;

  String connectionId="";
  // used to trigger api when user pressed back button
  bool sentAtLeastOneMessage=false;

  IOWebSocketChannel channel;

  GlobalKey<AnimatedListState> animatedListKey;

  ScrollController controller = ScrollController();

  final chatItemsController = BehaviorSubject<List<SelectedChatEntity>>();

  Stream<List<SelectedChatEntity>> get allChats => chatItemsController.stream;

  TextEditingController chatTextController;
  bool _apiHitted=false;
  ChatBloc(this._getChatUseCase, this._getSelectedChatUseCase,
      this._sharedPrefHelper, this.deleteRecentMatch,this._reportUserUseCase) {
    chatTextController = TextEditingController();

    _getUserData();
  }

  _getUserData() {
    var userData = _sharedPrefHelper.getUserData();
    _currentUserId = userData?.id?.toString();
  }

//  final chatController = BehaviorSubject<SelectedChatEntity>();

  addChatItems(SelectedChatEntity item) {
    chatItemsController.value.insert(0, item);
//    chatItemsController.sink.add(chatItemsController.value);
  }

List<ChatEntity> _allChats=[];

  @override
  ChatState get initialState => ChatState.initlaChatState();

  @override
  Stream<ChatState> mapEventToState(
    ChatEvent event,
  ) async* {
    print("event is"+event.toString());
    if (event is GetChat) {
      yield ChatState.loadingChatState();
      yield* mapGetChatEventToStream();
    } else if (event is GetSelectedChat) {
      print("api hitted"+_apiHitted.toString());
     if(!_apiHitted){
       _apiHitted=true;
       yield ChatState.loadingChatState();
       yield* mapGetSelectedChatEventToStream(event.connectionId);
     }
    }
    else if( event is UpdateUserChat){
      yield* updateUserChat(event);
    }
    else if(event is DeleteRecentChat){
      yield ChatState.loadingChatState();
      var response = await deleteRecentMatch(event.connectionId);
      if(event.type!=null&&event.type==1){
        yield* delete(event, response);
      }else{
        add(GetChat());
      }
    }else if(event is ReportUser){
      yield ChatState.loadingChatState();
      yield* reportUserEventToStream(event);
    }
  }

  Stream<ChatState> delete(DeleteRecentChat e,Either response) async* {
   yield response.fold((l) {
     return ChatState.errorReportUserState(error: l.errorMessage);
   }, (r) {
      return ChatState.userReportedState(
          message: '',
          requestType: 'delete'
      );
    });
  }


  Stream<ChatState> reportUserEventToStream(ReportUser e) async* {
    try{
      var response = await _reportUserUseCase(e.userId);
      yield response.fold((failure) {
        if (failure is UnAuthorized) {
          return ChatState.errorChatState(error: failure.errorMessage);
        } else {
          return ChatState.errorReportUserState(error: failure.errorMessage);
        }
      }, (items) {
        return ChatState.userReportedState(
            message: items.message,
            requestType: 'report'
        );
      });
    }catch(e){
     yield ChatState.errorReportUserState(
         error: 'Something went wrong'
     );
    }
  }

  Stream<ChatState> mapGetChatEventToStream() async* {
    var response = await _getChatUseCase("");
    yield response.fold((failure) {
      if (failure is NoDataFound) {
        return ChatState.noDataChatState();
      } else if (failure is UnAuthorized) {
        return ChatState.errorChatState(error: failure.errorMessage);
      } else {
        return ChatState.errorChatState(error: failure.errorMessage);
      }
    }, (items) {
      _allChats=items;
      return ChatState.successChatState(items: items);
    });
  }

  Stream<ChatState> mapGetSelectedChatEventToStream(
      String connectionId) async* {

    var response = await _getSelectedChatUseCase(connectionId);
    yield response.fold((failure)   {
      if (failure is NoDataFound) {
        _apiHitted=false;
        return ChatState.noDataChatState();
      } else if (failure is UnAuthorized) {
        return ChatState.errorChatState(error: failure.errorMessage);
      } else {
        return ChatState.errorChatState(error: failure.errorMessage);
      }

    }, (chatResponse) {
      _apiHitted=false;
      _chatRoomName = chatResponse.data.chatRoom;
              if(!connectChatRoom()){
        return ChatState.errorChatState(error: "Error in websocket connection");
      }
      var meetList = List<SelectedChatEntity>();
      if (chatResponse.data.messages.isNotEmpty) {
        chatResponse.data.messages.forEach((item) {
          meetList.add(SelectedChatEntity.fromApiResponse(item));
        });
      }
      chatItemsController.sink.add(meetList);
//      controller.animateTo(chatItemsController.value.length.toDouble(), duration: Duration(milliseconds: 200), curve: Curves.fastLinearToSlowEaseIn);
      return ChatState.successSelectedChatState(items: meetList);
    });
  }

  Stream<ChatState> updateUserChat(UpdateUserChat event)async*{
    yield ChatState.loadingChatState();
    _allChats.forEach((element) {
      if(element.connectionId==event.connectionId){
        element.message=event.message;
        element.messageTime=DateTime.now();
      }
    });
    yield ChatState.successChatState(items: _allChats);
  }

  @override
  void dispose() {
//    chatController.close();
  closeMessageSink();
    chatItemsController.close();

  }

  closeMessageSink(){
    channel?.sink?.close(status.goingAway);
  }

  bool connectChatRoom()  {
    var userData = _sharedPrefHelper.getUserData();
    var fcmToken =
        Platform.isAndroid?_sharedPrefHelper.getFcmToken().replaceFirst(":", "_surprisem_"):_sharedPrefHelper.getFcmToken();
    var url = "${ApiConstants.chatUrl}/$_chatRoomName/${userData.authToken}/$fcmToken/";
    // var url = "${ApiConstants.chatUrl}/$_chatRoomName/${userData.authToken}/$fcmToken/";
    // var url = "${ApiConstants.chatUrl}/$_chatRoomName/${userData.authToken}/0/";
   try{



       if (channel==null) {
           channel = IOWebSocketChannel.connect(url);
           listenChannelStream();
       }
       else{
         channel.sink.close(status.goingAway);
         channel = IOWebSocketChannel.connect(url);
         listenChannelStream();
       }
       print("channal");
     print(channel.closeCode);
     return true;
   }catch(e){
     print(e);
     return false;
   }

  }

  sendMessage(String message) {
    chatTextController.clear();

    channel?.sink?.add(jsonEncode({"message": message}));
  }

  listenChannelStream() {
    channel.stream.distinct().listen((data) {
      var newMessage = SelectedChatEntity.fromApiResponse(
          ChatMessage.fromJson(jsonDecode(data)));
      animatedListKey?.currentState?.insertItem(
          0,
          duration: Duration(milliseconds: 200));
      addChatItems(newMessage);
      print(newMessage);
      print(animatedListKey.hashCode);
      if(!sentAtLeastOneMessage){
        sentAtLeastOneMessage=true;
      }
//      Timer(Duration(milliseconds: 1500), () => controller.animateTo(0.0, duration: Duration(milliseconds: 250),curve: Curves.ease));
//      print(data.toString());

    },onDone: (){

    });
  }
}
