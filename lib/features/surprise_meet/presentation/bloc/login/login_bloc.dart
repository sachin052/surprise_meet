import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:rxdart/rxdart.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/loginEntity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/checkfbusernameusercase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/instaloginusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/loginUseCase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/loginusingfbusecase.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/basebloc.dart';
import 'package:surprise_meet/main.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import 'bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState>
    implements BaseBloc {
  final _emailController = BehaviorSubject<String>();
  final _fbUserNameController = BehaviorSubject<String>();
  TextEditingController emailTextController = TextEditingController();

  final _passwordController = BehaviorSubject<String>();
  TextEditingController passController = TextEditingController();
  TextEditingController userNameController = TextEditingController();

  // change data
  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changeUserName => _fbUserNameController.sink.add;

  Function(String) get changePassword => _passwordController.sink.add;

  Stream<String> get email => _emailController.stream.transform(validateEmail);
  Stream<String> get userName => _fbUserNameController.stream.transform(notEmptyValidator);
  Stream<String> get password =>
      _passwordController.stream.transform(validatePassword);

  Stream<bool> get validCredentials=>Rx.combineLatest2(email, password, (a, b) => true).asBroadcastStream();
  // Stream<bool> get validCredentials=>Rx.combineLatest([Stream.value(true)],(a)=>true).asBroadcastStream();

  // Use Case
  final LoginUseCase loginUseCase;
  final CheckFBUserNameUseCase _checkFBUserNameUseCase;
  final LoginUsingFBUseCase loginUsingFBUseCase;
  final LoginUsingInsta loginUsingInsta;

  String _fbtoken;
  bool _getFBUsername=false;
  LoginBloc(
    this.loginUseCase,
    this.loginUsingFBUseCase,
    this.loginUsingInsta, this._checkFBUserNameUseCase,
  );

  @override
  LoginState get initialState => LoginState.loginInitial();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is UserLogin) {
      yield* mapResponseToState();
    }
    else if(event is CheckFbUserName){
//       if(_fbtoken==null){
        final facebookLogin = FacebookLogin();
        await facebookLogin.logOut();
        final result = await facebookLogin.logIn(['email']);
        switch (result.status) {
          case FacebookLoginStatus.loggedIn:
            yield LoginState.loginLoading();
            _fbtoken=result.accessToken.token;
            print("fb token "+result.accessToken.token);
            var response = await _checkFBUserNameUseCase(result.accessToken.token);
            await facebookLogin.logOut();
            yield* mapCheckFbUsername(response);
            break;
          case FacebookLoginStatus.cancelledByUser:
            print("user canelled");
            break;
          case FacebookLoginStatus.error:
            print("error is" + result.errorMessage);
            break;
        }
//      }
//      else{
////        add(LoginEvent.fBLogin());
//      }

    }
    else if (event is FBLogin) {
      if(_getFBUsername&&userNameController.text.isEmpty){
        _fbUserNameController.sink.addError("Username must not be empty");
        return;
      }
      var map={
        "fb_access_token":_fbtoken,
      };
      if(_fbUserNameController.value!=null)
        map.addAll({
          "fb_user_name":_fbUserNameController.value
        });
      userNameController.clear();
      yield LoginState.loginLoading();
    var response=await loginUsingFBUseCase(map);
    yield* mapSocialLoginResponse(response);
    } else if (event is InstaLogin) {
      if (event.token != null) {
        var response = await loginUsingInsta(event.token);
        yield* mapSocialLoginResponse(response);
      } else {
        yield LoginState.loginErrorState(error: "Failed to connect instagram");
      }
    }
  }

  @override
  void dispose() {
    emailTextController.dispose();
    _fbUserNameController.close();
    passController.dispose();
    _emailController.close();
    _passwordController.close();
  }

  Stream<LoginState> mapResponseToState() async* {
    if (emailTextController.text.isValidEmail &&
        passController.text.isValidPass) {
      yield LoginState.loginLoading();
      var body = {
        "email": emailTextController.text,
        "password": passController.text,
      };

      var response = await loginUseCase(body);
      yield response.fold(
          (failure) => LoginState.loginErrorState(error: failure.errorMessage),
          (success) => success.isInsta
              ? LoginState.loginSuccessState(data: success)
              : LoginState.completeProfile(data: success));
    } else {
      addEmptyError();
    }
  }
addEmptyError(){
  if (emailTextController.text.isEmpty) {
    _emailController.addError("Enter a valid email");
  }
  if (passController.text.isEmpty) {
    _passwordController.addError("Req min 8 Chars");
  }
}
  Stream<LoginState> mapSocialLoginResponse(
      Either<Failure, LoginEntity> response) async* {

    yield response.fold(
        (failure) => failure is NoFacebookUserName
            ? LoginState.getFaceBookUserName()
            : LoginState.loginErrorState(error: failure.errorMessage),
        (success) => success.isInsta
            ? LoginState.loginSuccessState(data: success)
            : LoginState.completeProfile(data: success));
  }

  Stream<LoginState> mapCheckFbUsername(Either<Failure, LoginEntity> response) async*{
    var result= response.fold(
            (failure) => failure is NoFacebookUserName
            ? LoginState.getFaceBookUserName()
            : LoginState.loginErrorState(error: failure.errorMessage),
            (success) {
          return LoginState.fBUserNameRegistered();
        });
    if(result==LoginState.getFaceBookUserName()){
      _getFBUsername=true;
    }
    yield result;
  }
}
