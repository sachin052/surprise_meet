import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

Future<Token> getToken(String appId, String appSecret) async {
  Stream<String> onCode = await _server();
  onCode.listen((value){
    print("from stram"+value);
  },onError: (error){
    print("from stram"+error);
  },onDone: (){

  });
  String url =
      "https://api.instagram.com/oauth/authorize?client_id=$appId&redirect_uri=https://www.google.com/&response_type=code&scope=user_profile,user_media";
//  final flutterWebviewPlugin = new FlutterWebviewPlugin();

//  flutterWebviewPlugin.launch(url);
  final String code = await onCode.first;
  print("Code ========== Code");
  print("Code -> "+code);
  final http.Response response = await http.post(
      "https://api.instagram.com/oauth/access_token",
      body: {"client_id": appId, "redirect_uri": "https://www.google.com/", "client_secret": appSecret,
        "code": code, "grant_type": "authorization_code",});
//  flutterWebviewPlugin.close();
  return new Token.fromMap(json.decode(response.body));
}

  Future<Stream<String>> _server() async {
  final onCode = new BehaviorSubject<String>();
  HttpServer server = await HttpServer.bind(InternetAddress.loopbackIPv6, Random(8200).nextInt(8800),shared: true);
  server.listen((HttpRequest request) async {
    print("status code is first");
    final String code = request.uri.queryParameters["code"];
    print("status code is "+code);
    request.response
      ..statusCode = 200
      ..headers.set("Content-Type", ContentType.html.mimeType)
      ..write("<html><h1>You can now close this window</h1></html>");
    await request.response.close();
    await server.close(force: true);
    onCode.add(code);
    await onCode.close();
  });
  return onCode.stream;
}

class Token {
  String access;
  String id;
  String username;
  String fullName;
  String profilePicture;

  Token.fromMap(Map json){
    access = json['access_token'];
    id = json['user']['id'];
    username = json['user']['username'];
    fullName = json['user']['full_name'];
    profilePicture = json['user']['profile_picture'];
  }
}