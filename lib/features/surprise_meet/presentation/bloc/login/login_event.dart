import 'package:super_enum/super_enum.dart';

part 'login_event.g.dart';

@superEnum
enum _LoginEvent {
  @object
  UserLogin,
@object
CheckFbUserName,
  @object
  FBLogin,
  @Data(fields: [DataField<String>("token")])
  InstaLogin,
}
