// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_event.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class LoginEvent extends Equatable {
  const LoginEvent(this._type);

  factory LoginEvent.userLogin() = UserLogin;

  factory LoginEvent.checkFbUserName() = CheckFbUserName;

  factory LoginEvent.fBLogin() = FBLogin;

  factory LoginEvent.instaLogin({@required String token}) = InstaLogin;

  final _LoginEvent _type;

//ignore: missing_return
  R when<R>(
      {@required FutureOr<R> Function(UserLogin) userLogin,
      @required FutureOr<R> Function(CheckFbUserName) checkFbUserName,
      @required FutureOr<R> Function(FBLogin) fBLogin,
      @required FutureOr<R> Function(InstaLogin) instaLogin}) {
    assert(() {
      if (userLogin == null ||
          checkFbUserName == null ||
          fBLogin == null ||
          instaLogin == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _LoginEvent.UserLogin:
        return userLogin(this as UserLogin);
      case _LoginEvent.CheckFbUserName:
        return checkFbUserName(this as CheckFbUserName);
      case _LoginEvent.FBLogin:
        return fBLogin(this as FBLogin);
      case _LoginEvent.InstaLogin:
        return instaLogin(this as InstaLogin);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(UserLogin) userLogin,
      FutureOr<R> Function(CheckFbUserName) checkFbUserName,
      FutureOr<R> Function(FBLogin) fBLogin,
      FutureOr<R> Function(InstaLogin) instaLogin,
      @required FutureOr<R> Function(LoginEvent) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _LoginEvent.UserLogin:
        if (userLogin == null) break;
        return userLogin(this as UserLogin);
      case _LoginEvent.CheckFbUserName:
        if (checkFbUserName == null) break;
        return checkFbUserName(this as CheckFbUserName);
      case _LoginEvent.FBLogin:
        if (fBLogin == null) break;
        return fBLogin(this as FBLogin);
      case _LoginEvent.InstaLogin:
        if (instaLogin == null) break;
        return instaLogin(this as InstaLogin);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(UserLogin) userLogin,
      FutureOr<void> Function(CheckFbUserName) checkFbUserName,
      FutureOr<void> Function(FBLogin) fBLogin,
      FutureOr<void> Function(InstaLogin) instaLogin}) {
    assert(() {
      if (userLogin == null &&
          checkFbUserName == null &&
          fBLogin == null &&
          instaLogin == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _LoginEvent.UserLogin:
        if (userLogin == null) break;
        return userLogin(this as UserLogin);
      case _LoginEvent.CheckFbUserName:
        if (checkFbUserName == null) break;
        return checkFbUserName(this as CheckFbUserName);
      case _LoginEvent.FBLogin:
        if (fBLogin == null) break;
        return fBLogin(this as FBLogin);
      case _LoginEvent.InstaLogin:
        if (instaLogin == null) break;
        return instaLogin(this as InstaLogin);
    }
  }

  @override
  List get props => const [];
}

@immutable
class UserLogin extends LoginEvent {
  const UserLogin._() : super(_LoginEvent.UserLogin);

  factory UserLogin() {
    _instance ??= UserLogin._();
    return _instance;
  }

  static UserLogin _instance;
}

@immutable
class CheckFbUserName extends LoginEvent {
  const CheckFbUserName._() : super(_LoginEvent.CheckFbUserName);

  factory CheckFbUserName() {
    _instance ??= CheckFbUserName._();
    return _instance;
  }

  static CheckFbUserName _instance;
}

@immutable
class FBLogin extends LoginEvent {
  const FBLogin._() : super(_LoginEvent.FBLogin);

  factory FBLogin() {
    _instance ??= FBLogin._();
    return _instance;
  }

  static FBLogin _instance;
}

@immutable
class InstaLogin extends LoginEvent {
  const InstaLogin({@required this.token}) : super(_LoginEvent.InstaLogin);

  final String token;

  @override
  String toString() => 'InstaLogin(token:${this.token})';
  @override
  List get props => [token];
}
