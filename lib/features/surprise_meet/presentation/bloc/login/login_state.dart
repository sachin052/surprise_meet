import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/loginEntity.dart';

part 'login_state.g.dart';

//@immutable
//abstract class LoginState {}
//
//class InitialLoginState extends LoginState {}

@superEnum
enum _LoginState {
  @object
  LoginInitial,
  @object
  GetFaceBookUserName,
  @object
  FBUserNameRegistered,
  @object
  LoginLoading,
  @Data(fields: [DataField<String>("error")])
  LoginErrorState,
  @Data(fields: [DataField<LoginEntity>("data")])
  LoginSuccessState,
  @Data(fields: [DataField<LoginEntity>("data")])
  CompleteProfile,
}
