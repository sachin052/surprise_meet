// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_state.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class LoginState extends Equatable {
  const LoginState(this._type);

  factory LoginState.loginInitial() = LoginInitial;

  factory LoginState.getFaceBookUserName() = GetFaceBookUserName;

  factory LoginState.fBUserNameRegistered() = FBUserNameRegistered;

  factory LoginState.loginLoading() = LoginLoading;

  factory LoginState.loginErrorState({@required String error}) =
      LoginErrorState;

  factory LoginState.loginSuccessState({@required LoginEntity data}) =
      LoginSuccessState;

  factory LoginState.completeProfile({@required LoginEntity data}) =
      CompleteProfile;

  final _LoginState _type;

//ignore: missing_return
  R when<R>(
      {@required FutureOr<R> Function(LoginInitial) loginInitial,
      @required FutureOr<R> Function(GetFaceBookUserName) getFaceBookUserName,
      @required FutureOr<R> Function(FBUserNameRegistered) fBUserNameRegistered,
      @required FutureOr<R> Function(LoginLoading) loginLoading,
      @required FutureOr<R> Function(LoginErrorState) loginErrorState,
      @required FutureOr<R> Function(LoginSuccessState) loginSuccessState,
      @required FutureOr<R> Function(CompleteProfile) completeProfile}) {
    assert(() {
      if (loginInitial == null ||
          getFaceBookUserName == null ||
          fBUserNameRegistered == null ||
          loginLoading == null ||
          loginErrorState == null ||
          loginSuccessState == null ||
          completeProfile == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _LoginState.LoginInitial:
        return loginInitial(this as LoginInitial);
      case _LoginState.GetFaceBookUserName:
        return getFaceBookUserName(this as GetFaceBookUserName);
      case _LoginState.FBUserNameRegistered:
        return fBUserNameRegistered(this as FBUserNameRegistered);
      case _LoginState.LoginLoading:
        return loginLoading(this as LoginLoading);
      case _LoginState.LoginErrorState:
        return loginErrorState(this as LoginErrorState);
      case _LoginState.LoginSuccessState:
        return loginSuccessState(this as LoginSuccessState);
      case _LoginState.CompleteProfile:
        return completeProfile(this as CompleteProfile);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(LoginInitial) loginInitial,
      FutureOr<R> Function(GetFaceBookUserName) getFaceBookUserName,
      FutureOr<R> Function(FBUserNameRegistered) fBUserNameRegistered,
      FutureOr<R> Function(LoginLoading) loginLoading,
      FutureOr<R> Function(LoginErrorState) loginErrorState,
      FutureOr<R> Function(LoginSuccessState) loginSuccessState,
      FutureOr<R> Function(CompleteProfile) completeProfile,
      @required FutureOr<R> Function(LoginState) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _LoginState.LoginInitial:
        if (loginInitial == null) break;
        return loginInitial(this as LoginInitial);
      case _LoginState.GetFaceBookUserName:
        if (getFaceBookUserName == null) break;
        return getFaceBookUserName(this as GetFaceBookUserName);
      case _LoginState.FBUserNameRegistered:
        if (fBUserNameRegistered == null) break;
        return fBUserNameRegistered(this as FBUserNameRegistered);
      case _LoginState.LoginLoading:
        if (loginLoading == null) break;
        return loginLoading(this as LoginLoading);
      case _LoginState.LoginErrorState:
        if (loginErrorState == null) break;
        return loginErrorState(this as LoginErrorState);
      case _LoginState.LoginSuccessState:
        if (loginSuccessState == null) break;
        return loginSuccessState(this as LoginSuccessState);
      case _LoginState.CompleteProfile:
        if (completeProfile == null) break;
        return completeProfile(this as CompleteProfile);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(LoginInitial) loginInitial,
      FutureOr<void> Function(GetFaceBookUserName) getFaceBookUserName,
      FutureOr<void> Function(FBUserNameRegistered) fBUserNameRegistered,
      FutureOr<void> Function(LoginLoading) loginLoading,
      FutureOr<void> Function(LoginErrorState) loginErrorState,
      FutureOr<void> Function(LoginSuccessState) loginSuccessState,
      FutureOr<void> Function(CompleteProfile) completeProfile}) {
    assert(() {
      if (loginInitial == null &&
          getFaceBookUserName == null &&
          fBUserNameRegistered == null &&
          loginLoading == null &&
          loginErrorState == null &&
          loginSuccessState == null &&
          completeProfile == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _LoginState.LoginInitial:
        if (loginInitial == null) break;
        return loginInitial(this as LoginInitial);
      case _LoginState.GetFaceBookUserName:
        if (getFaceBookUserName == null) break;
        return getFaceBookUserName(this as GetFaceBookUserName);
      case _LoginState.FBUserNameRegistered:
        if (fBUserNameRegistered == null) break;
        return fBUserNameRegistered(this as FBUserNameRegistered);
      case _LoginState.LoginLoading:
        if (loginLoading == null) break;
        return loginLoading(this as LoginLoading);
      case _LoginState.LoginErrorState:
        if (loginErrorState == null) break;
        return loginErrorState(this as LoginErrorState);
      case _LoginState.LoginSuccessState:
        if (loginSuccessState == null) break;
        return loginSuccessState(this as LoginSuccessState);
      case _LoginState.CompleteProfile:
        if (completeProfile == null) break;
        return completeProfile(this as CompleteProfile);
    }
  }

  @override
  List get props => const [];
}

@immutable
class LoginInitial extends LoginState {
  const LoginInitial._() : super(_LoginState.LoginInitial);

  factory LoginInitial() {
    _instance ??= LoginInitial._();
    return _instance;
  }

  static LoginInitial _instance;
}

@immutable
class GetFaceBookUserName extends LoginState {
  const GetFaceBookUserName._() : super(_LoginState.GetFaceBookUserName);

  factory GetFaceBookUserName() {
    _instance ??= GetFaceBookUserName._();
    return _instance;
  }

  static GetFaceBookUserName _instance;
}

@immutable
class FBUserNameRegistered extends LoginState {
  const FBUserNameRegistered._() : super(_LoginState.FBUserNameRegistered);

  factory FBUserNameRegistered() {
    _instance ??= FBUserNameRegistered._();
    return _instance;
  }

  static FBUserNameRegistered _instance;
}

@immutable
class LoginLoading extends LoginState {
  const LoginLoading._() : super(_LoginState.LoginLoading);

  factory LoginLoading() {
    _instance ??= LoginLoading._();
    return _instance;
  }

  static LoginLoading _instance;
}

@immutable
class LoginErrorState extends LoginState {
  const LoginErrorState({@required this.error})
      : super(_LoginState.LoginErrorState);

  final String error;

  @override
  String toString() => 'LoginErrorState(error:${this.error})';
  @override
  List get props => [error];
}

@immutable
class LoginSuccessState extends LoginState {
  const LoginSuccessState({@required this.data})
      : super(_LoginState.LoginSuccessState);

  final LoginEntity data;

  @override
  String toString() => 'LoginSuccessState(data:${this.data})';
  @override
  List get props => [data];
}

@immutable
class CompleteProfile extends LoginState {
  const CompleteProfile({@required this.data})
      : super(_LoginState.CompleteProfile);

  final LoginEntity data;

  @override
  String toString() => 'CompleteProfile(data:${this.data})';
  @override
  List get props => [data];
}
