import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/basebloc.dart';
import '../../../../../main.dart';
import 'bloc.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> implements BaseBloc{
  final _currentPageController=BehaviorSubject<int>.seeded(1);
  Function (int) get changePage  => _currentPageController.sink.add;
  Stream<int> get currentPageIndex=>_currentPageController.stream;
  // used to change the scaofold background color

  // used for notification count
  final notificationCountController=BehaviorSubject<int>.seeded(0);
  Function (int) get changeNotificationCount  => notificationCountController.sink.add;
  Stream<int> get currentNotificationCount=>notificationCountController.stream;
  
  int notificationCount=0;

  HomeBloc(){
    notification.listen((data){

      // if(data!=null)
      //   changeUserLastMessage(data);
      if(data!="reset")
      updateNotificationCount();
      else changeNotificationCount(0);
    });
  }

  final _hasDataController=BehaviorSubject<bool>.seeded(false);
  Function (bool) get changeDataStatus  => _hasDataController.sink.add;
  Stream<bool> get currentDataStatus=>CombineLatestStream.combine2(_currentPageController, _hasDataController, (cuurentPage,dataStatus){
    if(cuurentPage==1&&dataStatus){
      return true;
    }
    return false;
  });

  @override
  HomeState get initialState => HomeState.potetentialMeetsPage();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if(event is PageTapped){
     yield* mapEventToStream(event);
    }
  }

  @override
  void dispose() {
   _currentPageController.close();
   notificationCountController.close();
   _hasDataController.close();
  }
  updateNotificationCount(){
    print("update notification");
    changeNotificationCount(notificationCountController.value+1);
  }
  Stream<HomeState> mapEventToStream(PageTapped homeState)async*{
    yield homeState.currentPageScreen.when(
        chatScreen: (c)=>  HomeState.chatScreenPage(),
        potentialMeets: (c)=>  HomeState.potetentialMeetsPage(),
        profile: (c)=>  HomeState.profileScreenpage());
  }
}
