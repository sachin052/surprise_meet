import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';
part 'home_event.g.dart';
@immutable
abstract class HomeEvent {}

class PageTapped extends HomeEvent{
  final CurrentPageScreen currentPageScreen;
  PageTapped(this.currentPageScreen);
}

@superEnum
enum _CurrentPageScreen{
  @object
  ChatScreen,
  @object
  PotentialMeets,
  @object
  Profile
}