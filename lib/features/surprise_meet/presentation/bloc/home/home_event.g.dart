// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_event.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class CurrentPageScreen extends Equatable {
  const CurrentPageScreen(this._type);

  factory CurrentPageScreen.chatScreen() = ChatScreen;

  factory CurrentPageScreen.potentialMeets() = PotentialMeets;

  factory CurrentPageScreen.profile() = Profile;

  final _CurrentPageScreen _type;

//ignore: missing_return
  R when<R>(
      {@required FutureOr<R> Function(ChatScreen) chatScreen,
      @required FutureOr<R> Function(PotentialMeets) potentialMeets,
      @required FutureOr<R> Function(Profile) profile}) {
    assert(() {
      if (chatScreen == null || potentialMeets == null || profile == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _CurrentPageScreen.ChatScreen:
        return chatScreen(this as ChatScreen);
      case _CurrentPageScreen.PotentialMeets:
        return potentialMeets(this as PotentialMeets);
      case _CurrentPageScreen.Profile:
        return profile(this as Profile);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(ChatScreen) chatScreen,
      FutureOr<R> Function(PotentialMeets) potentialMeets,
      FutureOr<R> Function(Profile) profile,
      @required FutureOr<R> Function(CurrentPageScreen) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _CurrentPageScreen.ChatScreen:
        if (chatScreen == null) break;
        return chatScreen(this as ChatScreen);
      case _CurrentPageScreen.PotentialMeets:
        if (potentialMeets == null) break;
        return potentialMeets(this as PotentialMeets);
      case _CurrentPageScreen.Profile:
        if (profile == null) break;
        return profile(this as Profile);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(ChatScreen) chatScreen,
      FutureOr<void> Function(PotentialMeets) potentialMeets,
      FutureOr<void> Function(Profile) profile}) {
    assert(() {
      if (chatScreen == null && potentialMeets == null && profile == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _CurrentPageScreen.ChatScreen:
        if (chatScreen == null) break;
        return chatScreen(this as ChatScreen);
      case _CurrentPageScreen.PotentialMeets:
        if (potentialMeets == null) break;
        return potentialMeets(this as PotentialMeets);
      case _CurrentPageScreen.Profile:
        if (profile == null) break;
        return profile(this as Profile);
    }
  }

  @override
  List get props => const [];
}

@immutable
class ChatScreen extends CurrentPageScreen {
  const ChatScreen._() : super(_CurrentPageScreen.ChatScreen);

  factory ChatScreen() {
    _instance ??= ChatScreen._();
    return _instance;
  }

  static ChatScreen _instance;
}

@immutable
class PotentialMeets extends CurrentPageScreen {
  const PotentialMeets._() : super(_CurrentPageScreen.PotentialMeets);

  factory PotentialMeets() {
    _instance ??= PotentialMeets._();
    return _instance;
  }

  static PotentialMeets _instance;
}

@immutable
class Profile extends CurrentPageScreen {
  const Profile._() : super(_CurrentPageScreen.Profile);

  factory Profile() {
    _instance ??= Profile._();
    return _instance;
  }

  static Profile _instance;
}
