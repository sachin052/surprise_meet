// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_state.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class HomeState extends Equatable {
  const HomeState(this._type);

  factory HomeState.homeScreenLoading() = HomeScreenLoading;

  factory HomeState.chatScreenPage() = ChatScreenPage;

  factory HomeState.potetentialMeetsPage() = PotetentialMeetsPage;

  factory HomeState.profileScreenpage() = ProfileScreenpage;

  final _HomeState _type;

//ignore: missing_return
  R when<R>(
      {@required FutureOr<R> Function(HomeScreenLoading) homeScreenLoading,
      @required FutureOr<R> Function(ChatScreenPage) chatScreenPage,
      @required FutureOr<R> Function(PotetentialMeetsPage) potetentialMeetsPage,
      @required FutureOr<R> Function(ProfileScreenpage) profileScreenpage}) {
    assert(() {
      if (homeScreenLoading == null ||
          chatScreenPage == null ||
          potetentialMeetsPage == null ||
          profileScreenpage == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _HomeState.HomeScreenLoading:
        return homeScreenLoading(this as HomeScreenLoading);
      case _HomeState.ChatScreenPage:
        return chatScreenPage(this as ChatScreenPage);
      case _HomeState.PotetentialMeetsPage:
        return potetentialMeetsPage(this as PotetentialMeetsPage);
      case _HomeState.ProfileScreenpage:
        return profileScreenpage(this as ProfileScreenpage);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(HomeScreenLoading) homeScreenLoading,
      FutureOr<R> Function(ChatScreenPage) chatScreenPage,
      FutureOr<R> Function(PotetentialMeetsPage) potetentialMeetsPage,
      FutureOr<R> Function(ProfileScreenpage) profileScreenpage,
      @required FutureOr<R> Function(HomeState) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _HomeState.HomeScreenLoading:
        if (homeScreenLoading == null) break;
        return homeScreenLoading(this as HomeScreenLoading);
      case _HomeState.ChatScreenPage:
        if (chatScreenPage == null) break;
        return chatScreenPage(this as ChatScreenPage);
      case _HomeState.PotetentialMeetsPage:
        if (potetentialMeetsPage == null) break;
        return potetentialMeetsPage(this as PotetentialMeetsPage);
      case _HomeState.ProfileScreenpage:
        if (profileScreenpage == null) break;
        return profileScreenpage(this as ProfileScreenpage);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(HomeScreenLoading) homeScreenLoading,
      FutureOr<void> Function(ChatScreenPage) chatScreenPage,
      FutureOr<void> Function(PotetentialMeetsPage) potetentialMeetsPage,
      FutureOr<void> Function(ProfileScreenpage) profileScreenpage}) {
    assert(() {
      if (homeScreenLoading == null &&
          chatScreenPage == null &&
          potetentialMeetsPage == null &&
          profileScreenpage == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _HomeState.HomeScreenLoading:
        if (homeScreenLoading == null) break;
        return homeScreenLoading(this as HomeScreenLoading);
      case _HomeState.ChatScreenPage:
        if (chatScreenPage == null) break;
        return chatScreenPage(this as ChatScreenPage);
      case _HomeState.PotetentialMeetsPage:
        if (potetentialMeetsPage == null) break;
        return potetentialMeetsPage(this as PotetentialMeetsPage);
      case _HomeState.ProfileScreenpage:
        if (profileScreenpage == null) break;
        return profileScreenpage(this as ProfileScreenpage);
    }
  }

  @override
  List get props => const [];
}

@immutable
class HomeScreenLoading extends HomeState {
  const HomeScreenLoading._() : super(_HomeState.HomeScreenLoading);

  factory HomeScreenLoading() {
    _instance ??= HomeScreenLoading._();
    return _instance;
  }

  static HomeScreenLoading _instance;
}

@immutable
class ChatScreenPage extends HomeState {
  const ChatScreenPage._() : super(_HomeState.ChatScreenPage);

  factory ChatScreenPage() {
    _instance ??= ChatScreenPage._();
    return _instance;
  }

  static ChatScreenPage _instance;
}

@immutable
class PotetentialMeetsPage extends HomeState {
  const PotetentialMeetsPage._() : super(_HomeState.PotetentialMeetsPage);

  factory PotetentialMeetsPage() {
    _instance ??= PotetentialMeetsPage._();
    return _instance;
  }

  static PotetentialMeetsPage _instance;
}

@immutable
class ProfileScreenpage extends HomeState {
  const ProfileScreenpage._() : super(_HomeState.ProfileScreenpage);

  factory ProfileScreenpage() {
    _instance ??= ProfileScreenpage._();
    return _instance;
  }

  static ProfileScreenpage _instance;
}
