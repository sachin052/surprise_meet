import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';
part 'home_state.g.dart';
//@immutable
//abstract class HomeState {}
//
//class InitialHomeState extends HomeState {}
//
//class ChatScreen extends HomeState{
//}


@superEnum
enum _HomeState{
  @object
  HomeScreenLoading,
  @object
  ChatScreenPage,
  @object
  PotetentialMeetsPage,
  @object
  ProfileScreenpage
}