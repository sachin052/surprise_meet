// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reset_password_state.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class ResetPasswordState<T> extends Equatable {
  const ResetPasswordState(this._type);

  factory ResetPasswordState.initialState() = InitialState<T>;

  factory ResetPasswordState.loadingState() = LoadingState<T>;

  factory ResetPasswordState.errorState({@required String error}) =
      ErrorState<T>;

  factory ResetPasswordState.successState({@required T data}) = SuccessState<T>;

  final _ResetPasswordState _type;

//ignore: missing_return
  R when<R>(
      {@required FutureOr<R> Function(InitialState<T>) initialState,
      @required FutureOr<R> Function(LoadingState<T>) loadingState,
      @required FutureOr<R> Function(ErrorState<T>) errorState,
      @required FutureOr<R> Function(SuccessState<T>) successState}) {
    assert(() {
      if (initialState == null ||
          loadingState == null ||
          errorState == null ||
          successState == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _ResetPasswordState.InitialState:
        return initialState(this as InitialState);
      case _ResetPasswordState.LoadingState:
        return loadingState(this as LoadingState);
      case _ResetPasswordState.ErrorState:
        return errorState(this as ErrorState);
      case _ResetPasswordState.SuccessState:
        return successState(this as SuccessState);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(InitialState<T>) initialState,
      FutureOr<R> Function(LoadingState<T>) loadingState,
      FutureOr<R> Function(ErrorState<T>) errorState,
      FutureOr<R> Function(SuccessState<T>) successState,
      @required FutureOr<R> Function(ResetPasswordState<T>) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _ResetPasswordState.InitialState:
        if (initialState == null) break;
        return initialState(this as InitialState);
      case _ResetPasswordState.LoadingState:
        if (loadingState == null) break;
        return loadingState(this as LoadingState);
      case _ResetPasswordState.ErrorState:
        if (errorState == null) break;
        return errorState(this as ErrorState);
      case _ResetPasswordState.SuccessState:
        if (successState == null) break;
        return successState(this as SuccessState);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(InitialState<T>) initialState,
      FutureOr<void> Function(LoadingState<T>) loadingState,
      FutureOr<void> Function(ErrorState<T>) errorState,
      FutureOr<void> Function(SuccessState<T>) successState}) {
    assert(() {
      if (initialState == null &&
          loadingState == null &&
          errorState == null &&
          successState == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _ResetPasswordState.InitialState:
        if (initialState == null) break;
        return initialState(this as InitialState);
      case _ResetPasswordState.LoadingState:
        if (loadingState == null) break;
        return loadingState(this as LoadingState);
      case _ResetPasswordState.ErrorState:
        if (errorState == null) break;
        return errorState(this as ErrorState);
      case _ResetPasswordState.SuccessState:
        if (successState == null) break;
        return successState(this as SuccessState);
    }
  }

  @override
  List get props => const [];
}

@immutable
class InitialState<T> extends ResetPasswordState<T> {
  const InitialState._() : super(_ResetPasswordState.InitialState);

  factory InitialState() {
    _instance ??= InitialState._();
    return _instance;
  }

  static InitialState _instance;
}

@immutable
class LoadingState<T> extends ResetPasswordState<T> {
  const LoadingState._() : super(_ResetPasswordState.LoadingState);

  factory LoadingState() {
    _instance ??= LoadingState._();
    return _instance;
  }

  static LoadingState _instance;
}

@immutable
class ErrorState<T> extends ResetPasswordState<T> {
  const ErrorState({@required this.error})
      : super(_ResetPasswordState.ErrorState);

  final String error;

  @override
  String toString() => 'ErrorState(error:${this.error})';
  @override
  List get props => [error];
}

@immutable
class SuccessState<T> extends ResetPasswordState<T> {
  const SuccessState({@required this.data})
      : super(_ResetPasswordState.SuccessState);

  final T data;

  @override
  String toString() => 'SuccessState(data:${this.data})';
  @override
  List get props => [data];
}
