import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';

part 'reset_password_state.g.dart';

@superEnum
enum _ResetPasswordState {
  @object
  InitialState,
  @object
  LoadingState,
  @Data(fields: [DataField<String>("error")])
  ErrorState,
  @generic
  @Data(fields: [DataField<Generic>("data")])
  SuccessState,
}
