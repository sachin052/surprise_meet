import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/resetpasswordusecase.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/basebloc.dart';
import 'package:surprise_meet/main.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import './bloc.dart';

class ResetPasswordBloc extends Bloc<ResetPasswordEvent, ResetPasswordState>   implements BaseBloc{
  final _emailController = BehaviorSubject<String>();
  TextEditingController emailTextController = TextEditingController();

  ResetPasswordBloc(this._resetPasswordUseCase);
  // change data
  Function(String) get changeEmail => _emailController.sink.add;
  Stream<String> get email => _emailController.stream.transform(validateEmail);

  // UseCase
  final ResetPasswordUseCase _resetPasswordUseCase;
  @override
  ResetPasswordState get initialState => ResetPasswordState.initialState();

  @override
  Stream<ResetPasswordState> mapEventToState(
    ResetPasswordEvent event,
  ) async* {
    if(event is ResetPasswordEvent){
      if(emailTextController.text.isValidEmail){
        yield ResetPasswordState.loadingState();
      var response=await _resetPasswordUseCase(emailTextController.text);
      yield response.fold(
          (failure)=>ResetPasswordState.errorState(error: failure.errorMessage),
          (success)=>ResetPasswordState.successState(data: success));
      }
      else{
//       yield ResetPasswordState.errorState(error: "Invalid Email");
      _emailController.addError("Enter a valid email");
      }
    }
  }

  @override
  void dispose() {
   _emailController.close();
  }
}
