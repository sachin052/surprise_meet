import 'package:meta/meta.dart';

@immutable
abstract class ResetPasswordEvent {}
class ResetPassword extends ResetPasswordEvent{}