import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flushbar/flushbar.dart';
import 'package:surprise_meet/main.dart';
import 'package:country_pickers/country.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/addpotentialmeet.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/deletpotentialmeets.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/getpotentialmeets.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/updatepotentialmeet.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/basebloc.dart';

import './bloc.dart';

class ManagePotentialMeetBloc
    extends Bloc<ManagePotentialMeetEvent, ManagePotentialMeetState>
    implements BaseBloc {

  final _currentPageController = BehaviorSubject<int>.seeded(0);
  Function(int) get changePage => _currentPageController.sink.add;
  Stream<int> get currentPage => _currentPageController.stream;

  final _emailController = BehaviorSubject<String>();
  Stream<String> get email => _emailController.stream.transform(validateEmail);
  Function(String) get changeEmail => _emailController.sink.add;

  final _phoneController = BehaviorSubject<String>();
  Stream<String> get phone => _phoneController.stream.transform(phoneValidator);
  Function(String) get changePhone => _phoneController.sink.add;

  // used for skip button
  final _visibilityController = BehaviorSubject<bool>.seeded(true);
  Function(bool) get chaneVisibility => _visibilityController.sink.add;
  Stream<bool> get visibility => CombineLatestStream.combine2(
      _currentPageController,
      _visibilityController,
          (currentPage, visibility) => currentPage == 3 ? false : visibility);

  final _countryCodeController = BehaviorSubject<Country>.seeded(Country(phoneCode: "1"));
  Stream<Country> get countryCode=>_countryCodeController.stream;
  Function(Country) get changeCountryCode => _countryCodeController.sink.add;


  final _dataController = BehaviorSubject<String>.seeded("");
  Function(String) get textChanged => _dataController.sink.add;
  Stream<String> get text => _dataController.stream;

  // UseCases
  final GetPotentialMeetsUseCase _getPotentialMeetsUseCase;
  final AddPotentialMeetsUseCase _addPotentialMeetsUseCase;
  final DeletePotentialMeetsUseCase _deletePotentialMeets;
  final UpdatePotentialMeetUseCase _updatePotentialMeetUseCase;

  TextEditingController nameController;
  TextEditingController phoneController;
  TextEditingController emailController;
  TextEditingController facebookController;
  TextEditingController instaController;

  List<TextEditingController> controllers;

  // slider controller
  PageController controller= PageController(initialPage: 0);


  final openEditModel=BehaviorSubject<bool>.seeded(false);
  Function(bool) get changeEditMode=>openEditModel.sink.add;
  Stream<bool> get editMode=>openEditModel.stream;

  ManagePotentialMeetBloc(this._addPotentialMeetsUseCase,

      this._deletePotentialMeets, this._getPotentialMeetsUseCase, this._updatePotentialMeetUseCase) {
    nameController = TextEditingController();
    phoneController = TextEditingController();
    emailController = TextEditingController();
    facebookController = TextEditingController();
    instaController = TextEditingController();
    controllers = List<TextEditingController>();
    controllers.add(nameController);
    controllers.add(phoneController);
    controllers.add(emailController);
//    controllers.add(facebookController);
    controllers.add(instaController);
  }
  @override
  ManagePotentialMeetState get initialState =>
      ManagePotentialMeetState.initialManageMeet();

  @override
  Stream<ManagePotentialMeetState> mapEventToState(
    ManagePotentialMeetEvent event,
  ) async* {
    if (event is AddPotentialMeet) {
      print("added");
      if (nameController.text.isEmpty) {
        controller.animateToPage(0,
            curve: Curves.fastOutSlowIn,
            duration: Duration(milliseconds: 1000));
        yield ManagePotentialMeetState.errorPotentialMeetState(
            error: "Enter a valid name");
        // if user taps on same button it will emit error state, but:
        // yielding initial state, because it won't trigger the error state again if it has already an error state
        yield ManagePotentialMeetState.initialManageMeet();
      }
//       else if(phoneController.text.isEmpty){
//         controller.animateToPage(1,
//             curve: Curves.fastOutSlowIn,
//             duration: Duration(milliseconds: 1000)).whenComplete((){
// //          changePage(_currentPageController.value);
//         });
//         yield ManagePotentialMeetState.errorPotentialMeetState(error: "Enter a valid phone");
//         yield ManagePotentialMeetState.initialManageMeet();
//         return;
//       }
      else if(emailController.text.isNotEmpty&&!emailController.text.isValidEmail){
        controller.animateToPage(2,
            curve: Curves.fastOutSlowIn,
            duration: Duration(milliseconds: 1000)).whenComplete((){

        });
        yield ManagePotentialMeetState.errorPotentialMeetState(
            error: "Enter a valid email");
        yield ManagePotentialMeetState.initialManageMeet();
        return;
      }
      else if(_currentPageController.value!=3){
        print(_currentPageController.value);

        controller.animateToPage(_currentPageController.value+1,
            curve: Curves.fastOutSlowIn,
            duration: Duration(milliseconds: 1000)).whenComplete((){

        }).whenComplete((){
          changePage(_currentPageController.value);
        });

        return;
      }
      else {
        var map = {
          "name": nameController.text,
          "email": emailController.text,
//          "facebook_username": facebookController.text,
          "facebook_username": "",
          "insta_handler": instaController.text
        };
        if(phoneController.text.isEmpty){
          map.addAll({
            "phone": ""
          });
        }
        else if(int.tryParse(phoneController.text)!=null){
          map.addAll({
            "phone": "+"+_countryCodeController.value.phoneCode.replaceAll("-","")+"-"+phoneController.text
          });
        }
        yield ManagePotentialMeetState.loadintManageMeet();
        var response = await _addPotentialMeetsUseCase(map);
        yield* mapAddMeetToStream(response);
      }
    }
    else if (event is GetPotentialMeets) {
      yield ManagePotentialMeetState.loadintManageMeet();
      var response = await _getPotentialMeetsUseCase("");
      yield* mapGetMeetEventToStream(response);
    }
    else if (event is DeletePotentialMeet) {
      yield ManagePotentialMeetState.loadintManageMeet();
      var response = await _deletePotentialMeets(event.meetId);
      yield* mapEventDeleteToStream(response);
    }
    else if(event is UpdatePotentialMeet){
       Map<String,dynamic> map = {
         "potential_meet_id":event.meetId,
         "name":nameController.text,
         "phone": phoneController.text.isNotEmpty?"+"+_countryCodeController.value.phoneCode.replaceAll("-","")+"-"+phoneController.text:"",
         "email": emailController.text,
         "facebook_username": "",
//         "facebook_username": facebookController.text,
         "insta_handler": instaController.text
       };
      yield ManagePotentialMeetState.loadintManageMeet();
       var response = await _updatePotentialMeetUseCase(map);
       yield* mapUpdateMeetToStream(response);
    }
  }

  @override
  void dispose() {
    _currentPageController.close();
    _visibilityController.close();
    _countryCodeController.close();
    _emailController.close();
    _phoneController.close();
    _dataController.close();
  }

  Stream<ManagePotentialMeetState> mapEventDeleteToStream(
      Either<Failure, PotentialMeetEntity> response) async* {

    yield response.fold(
        (failure) => ManagePotentialMeetState.errorPotentialMeetState(
            error: failure.errorMessage),
        (success) => ManagePotentialMeetState.meetDeleted());
  }

  Stream<ManagePotentialMeetState> mapAddMeetToStream(
      Either<Failure, PotentialMeetEntity> response) async* {
    yield response.fold(
        (failure) => ManagePotentialMeetState.errorPotentialMeetState(
            error: failure.errorMessage),
        (success) => ManagePotentialMeetState.meetAdded());
  }

  Stream<ManagePotentialMeetState> mapUpdateMeetToStream(
      Either<Failure, PotentialMeetEntity> response) async* {
    yield response.fold(
            (failure) => ManagePotentialMeetState.errorPotentialMeetState(
            error: failure.errorMessage),
            (success) => ManagePotentialMeetState.updateMeet());
  }

  Stream<ManagePotentialMeetState> mapGetMeetEventToStream(
      Either<Failure, ListOfPotentialMeets> response) async* {
    yield response.fold((failure) {
      if (failure is NoDataFound) {
        return ManagePotentialMeetState.noDataPotenaialMeetState();
      } else if (failure is UnAuthorized) {
        return ManagePotentialMeetState.errorPotentialMeetState(
            error: failure.errorMessage);
      } else {
        return ManagePotentialMeetState.errorPotentialMeetState(
            error: failure.errorMessage);
      }
    },
        (success) =>
            ManagePotentialMeetState.hasDataPotentialMeetState(data: success));
  }

  void navigateToNext() {
    print("Pressed");

    if(nameController.text.isEmpty){
    }
//    if(){
//
//    }
//    currentPage != list.length - 1
//        ? controller.animateToPage(
//        currentPage + 1,
//        curve: Curves.fastOutSlowIn,
//        duration:
//        Duration(milliseconds: 500))
//        : _managePotentialMeetBloc
//        .add(AddPotentialMeet());
  }
}
