import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';

part 'managepotentialmeet_state.g.dart';

//@immutable
//abstract class ManagepotentialmeetState {}
//
//class InitialManagepotentialmeetState extends ManagepotentialmeetState {}
@superEnum
enum _ManagePotentialMeetState {
  @object
  LoadintManageMeet,
  @object
  InitialManageMeet,
  @Data(fields: [DataField<String>("error")])
  ErrorPotentialMeetState,
  @object
  UpdateMeet,
  @object
  MeetAdded,
  @object
  MeetDeleted,
  @Data(fields: [DataField<ListOfPotentialMeets>("data")])
  HasDataPotentialMeetState,
  @object
  NoDataPotenaialMeetState,
}
