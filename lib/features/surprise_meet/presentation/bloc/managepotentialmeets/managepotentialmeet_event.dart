import 'dart:math';

import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';

@immutable
abstract class ManagePotentialMeetEvent extends Equatable {
  ManagePotentialMeetEvent([List props = const <String>[]]) : super();
}
class GetPotentialMeets extends ManagePotentialMeetEvent{
  @override
  // TODO: implement props
  List<String> get props =>  [];

}
class AddPotentialMeet extends ManagePotentialMeetEvent{
  @override
  // TODO: implement props
  List<String> get props =>  [Random().nextInt(100).toString()];
}
class UpdatePotentialMeet extends ManagePotentialMeetEvent{
  final String meetId;
  UpdatePotentialMeet(this.meetId);

  @override
  // TODO: implement props
  List<String> get props => [];
}
class DeletePotentialMeet extends ManagePotentialMeetEvent{
  final String meetId;
  DeletePotentialMeet(this.meetId);

  @override
  // TODO: implement props
  List<String> get props =>  [meetId];
}