// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'managepotentialmeet_state.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class ManagePotentialMeetState extends Equatable {
  const ManagePotentialMeetState(this._type);

  factory ManagePotentialMeetState.loadintManageMeet() = LoadintManageMeet;

  factory ManagePotentialMeetState.initialManageMeet() = InitialManageMeet;

  factory ManagePotentialMeetState.errorPotentialMeetState(
      {@required String error}) = ErrorPotentialMeetState;

  factory ManagePotentialMeetState.updateMeet() = UpdateMeet;

  factory ManagePotentialMeetState.meetAdded() = MeetAdded;

  factory ManagePotentialMeetState.meetDeleted() = MeetDeleted;

  factory ManagePotentialMeetState.hasDataPotentialMeetState(
      {@required ListOfPotentialMeets data}) = HasDataPotentialMeetState;

  factory ManagePotentialMeetState.noDataPotenaialMeetState() =
      NoDataPotenaialMeetState;

  final _ManagePotentialMeetState _type;

//ignore: missing_return
  R when<R>(
      {@required
          FutureOr<R> Function(LoadintManageMeet) loadintManageMeet,
      @required
          FutureOr<R> Function(InitialManageMeet) initialManageMeet,
      @required
          FutureOr<R> Function(ErrorPotentialMeetState) errorPotentialMeetState,
      @required
          FutureOr<R> Function(UpdateMeet) updateMeet,
      @required
          FutureOr<R> Function(MeetAdded) meetAdded,
      @required
          FutureOr<R> Function(MeetDeleted) meetDeleted,
      @required
          FutureOr<R> Function(HasDataPotentialMeetState)
              hasDataPotentialMeetState,
      @required
          FutureOr<R> Function(NoDataPotenaialMeetState)
              noDataPotenaialMeetState}) {
    assert(() {
      if (loadintManageMeet == null ||
          initialManageMeet == null ||
          errorPotentialMeetState == null ||
          updateMeet == null ||
          meetAdded == null ||
          meetDeleted == null ||
          hasDataPotentialMeetState == null ||
          noDataPotenaialMeetState == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _ManagePotentialMeetState.LoadintManageMeet:
        return loadintManageMeet(this as LoadintManageMeet);
      case _ManagePotentialMeetState.InitialManageMeet:
        return initialManageMeet(this as InitialManageMeet);
      case _ManagePotentialMeetState.ErrorPotentialMeetState:
        return errorPotentialMeetState(this as ErrorPotentialMeetState);
      case _ManagePotentialMeetState.UpdateMeet:
        return updateMeet(this as UpdateMeet);
      case _ManagePotentialMeetState.MeetAdded:
        return meetAdded(this as MeetAdded);
      case _ManagePotentialMeetState.MeetDeleted:
        return meetDeleted(this as MeetDeleted);
      case _ManagePotentialMeetState.HasDataPotentialMeetState:
        return hasDataPotentialMeetState(this as HasDataPotentialMeetState);
      case _ManagePotentialMeetState.NoDataPotenaialMeetState:
        return noDataPotenaialMeetState(this as NoDataPotenaialMeetState);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(LoadintManageMeet) loadintManageMeet,
      FutureOr<R> Function(InitialManageMeet) initialManageMeet,
      FutureOr<R> Function(ErrorPotentialMeetState) errorPotentialMeetState,
      FutureOr<R> Function(UpdateMeet) updateMeet,
      FutureOr<R> Function(MeetAdded) meetAdded,
      FutureOr<R> Function(MeetDeleted) meetDeleted,
      FutureOr<R> Function(HasDataPotentialMeetState) hasDataPotentialMeetState,
      FutureOr<R> Function(NoDataPotenaialMeetState) noDataPotenaialMeetState,
      @required FutureOr<R> Function(ManagePotentialMeetState) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _ManagePotentialMeetState.LoadintManageMeet:
        if (loadintManageMeet == null) break;
        return loadintManageMeet(this as LoadintManageMeet);
      case _ManagePotentialMeetState.InitialManageMeet:
        if (initialManageMeet == null) break;
        return initialManageMeet(this as InitialManageMeet);
      case _ManagePotentialMeetState.ErrorPotentialMeetState:
        if (errorPotentialMeetState == null) break;
        return errorPotentialMeetState(this as ErrorPotentialMeetState);
      case _ManagePotentialMeetState.UpdateMeet:
        if (updateMeet == null) break;
        return updateMeet(this as UpdateMeet);
      case _ManagePotentialMeetState.MeetAdded:
        if (meetAdded == null) break;
        return meetAdded(this as MeetAdded);
      case _ManagePotentialMeetState.MeetDeleted:
        if (meetDeleted == null) break;
        return meetDeleted(this as MeetDeleted);
      case _ManagePotentialMeetState.HasDataPotentialMeetState:
        if (hasDataPotentialMeetState == null) break;
        return hasDataPotentialMeetState(this as HasDataPotentialMeetState);
      case _ManagePotentialMeetState.NoDataPotenaialMeetState:
        if (noDataPotenaialMeetState == null) break;
        return noDataPotenaialMeetState(this as NoDataPotenaialMeetState);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(LoadintManageMeet) loadintManageMeet,
      FutureOr<void> Function(InitialManageMeet) initialManageMeet,
      FutureOr<void> Function(ErrorPotentialMeetState) errorPotentialMeetState,
      FutureOr<void> Function(UpdateMeet) updateMeet,
      FutureOr<void> Function(MeetAdded) meetAdded,
      FutureOr<void> Function(MeetDeleted) meetDeleted,
      FutureOr<void> Function(HasDataPotentialMeetState)
          hasDataPotentialMeetState,
      FutureOr<void> Function(NoDataPotenaialMeetState)
          noDataPotenaialMeetState}) {
    assert(() {
      if (loadintManageMeet == null &&
          initialManageMeet == null &&
          errorPotentialMeetState == null &&
          updateMeet == null &&
          meetAdded == null &&
          meetDeleted == null &&
          hasDataPotentialMeetState == null &&
          noDataPotenaialMeetState == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _ManagePotentialMeetState.LoadintManageMeet:
        if (loadintManageMeet == null) break;
        return loadintManageMeet(this as LoadintManageMeet);
      case _ManagePotentialMeetState.InitialManageMeet:
        if (initialManageMeet == null) break;
        return initialManageMeet(this as InitialManageMeet);
      case _ManagePotentialMeetState.ErrorPotentialMeetState:
        if (errorPotentialMeetState == null) break;
        return errorPotentialMeetState(this as ErrorPotentialMeetState);
      case _ManagePotentialMeetState.UpdateMeet:
        if (updateMeet == null) break;
        return updateMeet(this as UpdateMeet);
      case _ManagePotentialMeetState.MeetAdded:
        if (meetAdded == null) break;
        return meetAdded(this as MeetAdded);
      case _ManagePotentialMeetState.MeetDeleted:
        if (meetDeleted == null) break;
        return meetDeleted(this as MeetDeleted);
      case _ManagePotentialMeetState.HasDataPotentialMeetState:
        if (hasDataPotentialMeetState == null) break;
        return hasDataPotentialMeetState(this as HasDataPotentialMeetState);
      case _ManagePotentialMeetState.NoDataPotenaialMeetState:
        if (noDataPotenaialMeetState == null) break;
        return noDataPotenaialMeetState(this as NoDataPotenaialMeetState);
    }
  }

  @override
  List get props => const [];
}

@immutable
class LoadintManageMeet extends ManagePotentialMeetState {
  const LoadintManageMeet._()
      : super(_ManagePotentialMeetState.LoadintManageMeet);

  factory LoadintManageMeet() {
    _instance ??= LoadintManageMeet._();
    return _instance;
  }

  static LoadintManageMeet _instance;
}

@immutable
class InitialManageMeet extends ManagePotentialMeetState {
  const InitialManageMeet._()
      : super(_ManagePotentialMeetState.InitialManageMeet);

  factory InitialManageMeet() {
    _instance ??= InitialManageMeet._();
    return _instance;
  }

  static InitialManageMeet _instance;
}

@immutable
class ErrorPotentialMeetState extends ManagePotentialMeetState {
  const ErrorPotentialMeetState({@required this.error})
      : super(_ManagePotentialMeetState.ErrorPotentialMeetState);

  final String error;

  @override
  String toString() => 'ErrorPotentialMeetState(error:${this.error})';
  @override
  List get props => [error];
}

@immutable
class UpdateMeet extends ManagePotentialMeetState {
  const UpdateMeet._() : super(_ManagePotentialMeetState.UpdateMeet);

  factory UpdateMeet() {
    _instance ??= UpdateMeet._();
    return _instance;
  }

  static UpdateMeet _instance;
}

@immutable
class MeetAdded extends ManagePotentialMeetState {
  const MeetAdded._() : super(_ManagePotentialMeetState.MeetAdded);

  factory MeetAdded() {
    _instance ??= MeetAdded._();
    return _instance;
  }

  static MeetAdded _instance;
}

@immutable
class MeetDeleted extends ManagePotentialMeetState {
  const MeetDeleted._() : super(_ManagePotentialMeetState.MeetDeleted);

  factory MeetDeleted() {
    _instance ??= MeetDeleted._();
    return _instance;
  }

  static MeetDeleted _instance;
}

@immutable
class HasDataPotentialMeetState extends ManagePotentialMeetState {
  const HasDataPotentialMeetState({@required this.data})
      : super(_ManagePotentialMeetState.HasDataPotentialMeetState);

  final ListOfPotentialMeets data;

  @override
  String toString() => 'HasDataPotentialMeetState(data:${this.data})';
  @override
  List get props => [data];
}

@immutable
class NoDataPotenaialMeetState extends ManagePotentialMeetState {
  const NoDataPotenaialMeetState._()
      : super(_ManagePotentialMeetState.NoDataPotenaialMeetState);

  factory NoDataPotenaialMeetState() {
    _instance ??= NoDataPotenaialMeetState._();
    return _instance;
  }

  static NoDataPotenaialMeetState _instance;
}
