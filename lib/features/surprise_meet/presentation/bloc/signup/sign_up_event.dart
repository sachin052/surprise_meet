import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';
part 'sign_up_event.g.dart';
@superEnum
enum _SignUpEvent{
  @object
  SignUpUserEvent,
}
