// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_up_state.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class SignUpState extends Equatable {
  const SignUpState(this._type);

  factory SignUpState.signUpInitial() = SignUpInitial;

  factory SignUpState.signUpLoading() = SignUpLoading;

  factory SignUpState.agreeTerms({@required int value}) = AgreeTerms;

  factory SignUpState.signUpErrorState({@required String error}) =
      SignUpErrorState;

  factory SignUpState.signUpSuccessState({@required SignUpResponse data}) =
      SignUpSuccessState;

  final _SignUpState _type;

//ignore: missing_return
  R when<R>(
      {@required FutureOr<R> Function(SignUpInitial) signUpInitial,
      @required FutureOr<R> Function(SignUpLoading) signUpLoading,
      @required FutureOr<R> Function(AgreeTerms) agreeTerms,
      @required FutureOr<R> Function(SignUpErrorState) signUpErrorState,
      @required FutureOr<R> Function(SignUpSuccessState) signUpSuccessState}) {
    assert(() {
      if (signUpInitial == null ||
          signUpLoading == null ||
          agreeTerms == null ||
          signUpErrorState == null ||
          signUpSuccessState == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _SignUpState.SignUpInitial:
        return signUpInitial(this as SignUpInitial);
      case _SignUpState.SignUpLoading:
        return signUpLoading(this as SignUpLoading);
      case _SignUpState.AgreeTerms:
        return agreeTerms(this as AgreeTerms);
      case _SignUpState.SignUpErrorState:
        return signUpErrorState(this as SignUpErrorState);
      case _SignUpState.SignUpSuccessState:
        return signUpSuccessState(this as SignUpSuccessState);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(SignUpInitial) signUpInitial,
      FutureOr<R> Function(SignUpLoading) signUpLoading,
      FutureOr<R> Function(AgreeTerms) agreeTerms,
      FutureOr<R> Function(SignUpErrorState) signUpErrorState,
      FutureOr<R> Function(SignUpSuccessState) signUpSuccessState,
      @required FutureOr<R> Function(SignUpState) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _SignUpState.SignUpInitial:
        if (signUpInitial == null) break;
        return signUpInitial(this as SignUpInitial);
      case _SignUpState.SignUpLoading:
        if (signUpLoading == null) break;
        return signUpLoading(this as SignUpLoading);
      case _SignUpState.AgreeTerms:
        if (agreeTerms == null) break;
        return agreeTerms(this as AgreeTerms);
      case _SignUpState.SignUpErrorState:
        if (signUpErrorState == null) break;
        return signUpErrorState(this as SignUpErrorState);
      case _SignUpState.SignUpSuccessState:
        if (signUpSuccessState == null) break;
        return signUpSuccessState(this as SignUpSuccessState);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(SignUpInitial) signUpInitial,
      FutureOr<void> Function(SignUpLoading) signUpLoading,
      FutureOr<void> Function(AgreeTerms) agreeTerms,
      FutureOr<void> Function(SignUpErrorState) signUpErrorState,
      FutureOr<void> Function(SignUpSuccessState) signUpSuccessState}) {
    assert(() {
      if (signUpInitial == null &&
          signUpLoading == null &&
          agreeTerms == null &&
          signUpErrorState == null &&
          signUpSuccessState == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _SignUpState.SignUpInitial:
        if (signUpInitial == null) break;
        return signUpInitial(this as SignUpInitial);
      case _SignUpState.SignUpLoading:
        if (signUpLoading == null) break;
        return signUpLoading(this as SignUpLoading);
      case _SignUpState.AgreeTerms:
        if (agreeTerms == null) break;
        return agreeTerms(this as AgreeTerms);
      case _SignUpState.SignUpErrorState:
        if (signUpErrorState == null) break;
        return signUpErrorState(this as SignUpErrorState);
      case _SignUpState.SignUpSuccessState:
        if (signUpSuccessState == null) break;
        return signUpSuccessState(this as SignUpSuccessState);
    }
  }

  @override
  List get props => const [];
}

@immutable
class SignUpInitial extends SignUpState {
  const SignUpInitial._() : super(_SignUpState.SignUpInitial);

  factory SignUpInitial() {
    _instance ??= SignUpInitial._();
    return _instance;
  }

  static SignUpInitial _instance;
}

@immutable
class SignUpLoading extends SignUpState {
  const SignUpLoading._() : super(_SignUpState.SignUpLoading);

  factory SignUpLoading() {
    _instance ??= SignUpLoading._();
    return _instance;
  }

  static SignUpLoading _instance;
}

@immutable
class AgreeTerms extends SignUpState {
  const AgreeTerms({@required this.value}) : super(_SignUpState.AgreeTerms);

  final int value;

  @override
  String toString() => 'AgreeTerms(value:${this.value})';
  @override
  List get props => [value];
}

@immutable
class SignUpErrorState extends SignUpState {
  const SignUpErrorState({@required this.error})
      : super(_SignUpState.SignUpErrorState);

  final String error;

  @override
  String toString() => 'SignUpErrorState(error:${this.error})';
  @override
  List get props => [error];
}

@immutable
class SignUpSuccessState extends SignUpState {
  const SignUpSuccessState({@required this.data})
      : super(_SignUpState.SignUpSuccessState);

  final SignUpResponse data;

  @override
  String toString() => 'SignUpSuccessState(data:${this.data})';
  @override
  List get props => [data];
}
