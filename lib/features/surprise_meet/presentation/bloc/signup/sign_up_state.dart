import 'package:meta/meta.dart';
import 'package:super_enum/super_enum.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/signupresponse.dart';

part 'sign_up_state.g.dart';

@superEnum
enum _SignUpState {
  @object
  SignUpInitial,
  @object
  SignUpLoading,
  @Data(fields: [DataField<int>("value")])
  AgreeTerms,
  @Data(fields: [DataField<String>("error")])
  SignUpErrorState,
  @Data(fields: [DataField<SignUpResponse>("data")])
  SignUpSuccessState,
}
