import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:country_pickers/country.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:surprise_meet/core/error/failure.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import 'package:surprise_meet/core/utils/validators.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/signupresponse.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/signupusecase.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/basebloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/signup/sign_up_event.dart';
import 'package:surprise_meet/main.dart';

import '../../bloc/signup/bloc.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> implements BaseBloc {
  // Name Field
  BehaviorSubject<String> _nameController = BehaviorSubject<String>();

  Function(String) get changeName => _nameController.sink.add;

  Stream<String> get name =>
      _nameController.stream.transform(notEmptyValidator).asBroadcastStream();
  TextEditingController nameTextController = TextEditingController();

  // Email Field
  final _emailController = BehaviorSubject<String>();

  Function(String) get changeEmail => _emailController.sink.add;

  Stream<String> get email => _emailController.stream.transform(validateEmail).asBroadcastStream();
  TextEditingController emailTextController = TextEditingController();

  // Phone Field
  final _phoneController = BehaviorSubject<String>();
  final _countryCodeController = BehaviorSubject<Country>.seeded(Country(phoneCode: "1"));

  Function(String) get changePhone => _phoneController.sink.add;
  Function(Country) get changeCountryCode => _countryCodeController.sink.add;

  Stream<String> get phone =>
      _phoneController.stream.transform(notEmptyValidator);
  TextEditingController phoneTextController = TextEditingController();
  Stream<Country> get countryCode=>_countryCodeController.stream;
  // Password Field
  final _passwordController = BehaviorSubject<String>();

  Function(String) get changePassword => _passwordController.sink.add;

  Stream<String> get password =>
      _passwordController.stream.transform(validatePassword);
  TextEditingController passwordTextController;

  //Confirm Password
  final _cPasswordController = BehaviorSubject<String>();

  Function(String) get changeConfirmPassword => _cPasswordController.sink.add;

  // Used combineLatest2Stream for validations
  Stream<String> get confirmPassword =>
      Rx.combineLatest2<String,String,String>(_passwordController, _cPasswordController,
              (password, cpassword) {
            if (password == cpassword) return cpassword;
            _cPasswordController.addError("Password Mismatched");
            return null;
          }).asBroadcastStream();
  TextEditingController confirmPassTextController = TextEditingController();

  // Agree Terms and condition
  final _agreedTerms = BehaviorSubject<bool>.seeded(false);

  Function(bool) get changeTerms => _agreedTerms.sink.add;

  Stream<bool> get isAgreeTerms => _agreedTerms.stream;

  Stream<bool> get validCredentials=>Rx.combineLatest5<String,String,String,String,String,bool>(name,email,phone, confirmPassword,password, (a,b,c,d,e) => true).asBroadcastStream();

  //UseCases
  final SignUpUseCase userSignUp;

  // Animation Controller
  AnimationController controller;

  @override
  SignUpState get initialState => SignUpState.signUpInitial();

  SignUpBloc(this.userSignUp) {
    passwordTextController = TextEditingController();
  }

  @override
  Stream<SignUpState> mapEventToState(
      SignUpEvent event,
      ) async* {
    if (event is SignUpUserEvent) {
      var value = detailIsValid;
      if (value) {
        if (!_agreedTerms.value) {
          yield SignUpState.agreeTerms(value: Random(2).nextInt(10000));
          yield SignUpState.signUpInitial();
          return;
        }
        yield SignUpState.signUpLoading();
        var map = {
          "email": emailTextController.text,
          "password": passwordTextController.text,
          "name": nameTextController.text,
          "phone": "+"+_countryCodeController.value.phoneCode.replaceAll("-","")+"-"+phoneTextController.text,
        };
        var response = await userSignUp(map);
        yield* _eitherLoadedOrErrorState(response);
      } else {
//        yield SignUpState.signUpErrorState(error: "Validation Error");
      }
    }
  }

  Stream<SignUpState> _eitherLoadedOrErrorState(
      Either<Failure, SignUpResponse> failureOrTrivia,
      ) async* {
    yield failureOrTrivia.fold(
          (failure) => SignUpState.signUpErrorState(error: failure.errorMessage),
          (success) => SignUpState.signUpSuccessState(data: success),
    );
  }

  @override
  void dispose() {
    _nameController.close();
    _emailController.close();
    _phoneController.close();
    _passwordController.close();
    _cPasswordController.close();
    _countryCodeController.close();
    _agreedTerms.close();
  }

  bool get detailIsValid {
    if (nameTextController.text.isNotEmpty &&
        emailTextController.text.isValidEmail &&
        phoneTextController.text.isNotEmpty &&
        passwordTextController.text == confirmPassTextController.text&&
        passwordTextController.text.length>7&&confirmPassTextController.text.length>7) {
      return true;
    } else {
      if (nameTextController.text.isEmpty) {
        _nameController.addError("Name must not be empty");
      }
      if (emailTextController.text.isEmpty) {
        _emailController.addError("Enter a valid email");
      }
      if (phoneTextController.text.isEmpty) {
        _phoneController.addError("Phone must not be empty");
      }
      if (passwordTextController.text.isEmpty) {
        _passwordController.addError("Password must not be empty");
      }
      if (confirmPassTextController.text.isEmpty) {
        _cPasswordController.addError("Confirm Password must not be empty");
      }

      return false;
    }
  }
  addEmptyError(){
    if(nameTextController.text.isEmpty){
      _nameController.addError("Name must not be empty;");
    }
    if (emailTextController.text.isEmpty) {
      _emailController.addError("Enter a valid email");
    }
    if (passwordTextController.text.isEmpty) {
      _passwordController.addError("Req min 8 Chars");
    }
    if(phoneTextController.text.isEmpty){
      _phoneController.addError("Phone must not be empty");
    }
    if(confirmPassTextController.text.isEmpty){
      _cPasswordController.addError("Confirm Password must not be empty");
    }
    if(detailIsValid){
      add(SignUpEvent.signUpUserEvent());
    }
  }
}
