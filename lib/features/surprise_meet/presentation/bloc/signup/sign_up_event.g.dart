// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_up_event.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class SignUpEvent extends Equatable {
  const SignUpEvent(this._type);

  factory SignUpEvent.signUpUserEvent() = SignUpUserEvent;

  final _SignUpEvent _type;

//ignore: missing_return
  R when<R>({@required FutureOr<R> Function(SignUpUserEvent) signUpUserEvent}) {
    assert(() {
      if (signUpUserEvent == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _SignUpEvent.SignUpUserEvent:
        return signUpUserEvent(this as SignUpUserEvent);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(SignUpUserEvent) signUpUserEvent,
      @required FutureOr<R> Function(SignUpEvent) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _SignUpEvent.SignUpUserEvent:
        if (signUpUserEvent == null) break;
        return signUpUserEvent(this as SignUpUserEvent);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(SignUpUserEvent) signUpUserEvent}) {
    assert(() {
      if (signUpUserEvent == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _SignUpEvent.SignUpUserEvent:
        if (signUpUserEvent == null) break;
        return signUpUserEvent(this as SignUpUserEvent);
    }
  }

  @override
  List get props => const [];
}

@immutable
class SignUpUserEvent extends SignUpEvent {
  const SignUpUserEvent._() : super(_SignUpEvent.SignUpUserEvent);

  factory SignUpUserEvent() {
    _instance ??= SignUpUserEvent._();
    return _instance;
  }

  static SignUpUserEvent _instance;
}
