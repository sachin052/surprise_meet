
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoncarview.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
class CompleteProfileDialog extends StatefulWidget {
  final double profileCompletedPer;
  final VoidCallback onClick;
  final VoidCallback onSkipClick;
  const CompleteProfileDialog({Key key,
    @required this.profileCompletedPer,
    @required this.onClick,
    @required this.onSkipClick}) : super(key: key);
  @override
  _CompleteProfileDialogState createState() => _CompleteProfileDialogState();
}

class _CompleteProfileDialogState extends State<CompleteProfileDialog> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;
  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 700));
    animation = Tween<double>(begin: 0, end: widget.profileCompletedPer).animate(controller)
      ..addListener(() {
        setState(() {});
      });
    controller.forward();
  }
  @override
  Widget build(BuildContext context) {
    return CommonCard(
      topContainerChild: CircularPercentIndicator(
        radius: 80.0,
        lineWidth: 8.0,
        circularStrokeCap: CircularStrokeCap.round,
        backgroundColor: AppColors.colorPrimary.withOpacity(.6),
        percent: animation.value / 100,
        center: new Text("${animation.value.floor()}%",style: Theme.of(context).textTheme.body2.copyWith(color: Colors.white),),
        progressColor: Colors.white,
      ),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: SizeConfig.blockSizeWidth * 4,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0),
            child: Text(
              "Your Profile is ${animation.value.floor()}% Complete",
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headline
                  .copyWith(
                  fontSize:
                  17.0),
            ),
          ),
          SizedBox(
            height: SizeConfig.blockSizeWidth * 4,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:32.0),
            child: Text(
              "Complete your profile would help you to connect to your flame easily",
              style: Theme.of(context).textTheme.body2.copyWith(fontSize: 15.0),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: SizeConfig.blockSizeWidth * 4,
          ),
          RaisedGradientButton(
            gradient: AppTheme.gradient(),
            isDialog: true,
            text: "Complete",
            onPressed: widget.onClick
//                       MyRouter.navigatorKey.currentState.pop(state.data.message);
            ,
          ),
          FlatButton(
            child: Text(
              "Skip",
              style: Theme.of(context)
                  .textTheme
                  .body2
                  .copyWith(
                  color: Colors.black.withOpacity(.5)),
            ),
            onPressed: widget.onSkipClick,
          )
        ],
      ),
    );
  }
}
