import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildBottomView.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

class BuildTopView extends StatelessWidget {
  final String title;
  final String subtitle;

  const BuildTopView({Key key, @required this.title, @required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buildTopView(context, title, subtitle);
  }
}

Container buildTopView(BuildContext context, String title, String subTitle) {
  return Container(
//    padding: const EdgeInsets.only(left: 32.0),
//    alignment: Alignment(-1, -1),
//  color: Colors.red,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        getBottomSizedBox(),
        SvgPicture.asset(
          Images.appIcon,
          semanticsLabel: 'Acme Logo',
        ),
        SizedBox(
          height: SizeConfig.safeBlockHorizontal * 3,
        ),
//        Text(
//          title,
//          textAlign: TextAlign.center,
//          style: Theme.of(context).textTheme.headline,
//        ),
        SizedBox(
          height: SizeConfig.safeBlockHorizontal,
        ),
        Text(
          subTitle,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subhead,
        ),
      ],
    ),
  );
}
