import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/login/login_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/login/login_event.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/style/strings.dart';

class BuildBottomView extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;


  const BuildBottomView({Key key, this.scaffoldKey}) : super(key: key);@override
  Widget build(BuildContext context) {
    return buildBottomView(context,scaffoldKey);
  }
  Container buildBottomView(
      BuildContext context, GlobalKey<ScaffoldState> scaffoldKey) {
    var bloc = BlocProvider.of<LoginBloc>(context);
    return Container(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Visibility(visible:false,child: Text(Strings.visSocialMedial)),
          Visibility(visible:false,child: getBottomSizedBox()),
          Visibility(visible:false,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
//              GestureDetector(
//                onTap: () {
//                  bloc.add(LoginEvent.checkFbUserName());
//                },
//                child: Material(
//                  elevation: 3.0,
//                  borderRadius: BorderRadius.circular(50.0),
//                  child: SvgPicture.asset(
//                    Images.fbIcon,
//                    height: 50.0,
//                  ),
//                ),
//              ),
//              SizedBox(width: SizeConfig.safeBlockHorizontal * 8),
                GestureDetector(
                  onTap: () async{
//
                    var token=await MyRouter.navigatorKey.currentState.pushNamed(MyRouter.instaWebView);
                    print("token is"+token);
                    if(token!=null)
                      bloc.add(LoginEvent.instaLogin(token: token));
                  },
                  child: Material(
                    elevation: 3.0,
                    borderRadius: BorderRadius.circular(50.0),
                    child: SvgPicture.asset(
                      Images.instaIcon,
                      height: 50.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
          getBottomSizedBox(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Don't have an account? "),
              GestureDetector(
                onTap: () async {
                  var result = await MyRouter.navigator.pushNamed(MyRouter.signUp);
                  if (result != null) {
                    showSnackBar(context, result, false);
                  }
                },
                child: Container(
                  padding: const EdgeInsets.only(top: 5.0),
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: AppColors.colorAccent,width: 1.0)
                  )
                  ),
                  child: Text(
                    "Sign Up",
                    style: Theme.of(context).textTheme.headline.copyWith(
                        fontSize: 15.0),
                  ),
                ),
              ),
            ],
          ),
          getBottomSizedBox(),
        ],
      ),
    );
  }
}




Widget getBottomSizedBox() {
  return SizedBox(height: SizeConfig.safeBlockHorizontal * 6);
}
