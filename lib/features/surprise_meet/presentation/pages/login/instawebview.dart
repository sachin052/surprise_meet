
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:convert' as convert;
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

import 'package:http/http.dart' as http;


class InstaWebView extends StatefulWidget {
  final String otherUrl;
  final String title;
  const InstaWebView({Key key, this.otherUrl,this.title}) : super(key: key);

  @override
  _InstaWebViewState createState() => _InstaWebViewState();
}

class _InstaWebViewState extends State<InstaWebView> {
  static String _appId="743095956217626";
  static String _appSecret="0286716f4e2c73467e8f085b042e8759";
  bool resultOk=false;
  String url =
      "https://api.instagram.com/oauth/authorize?client_id=$_appId&redirect_uri=https://www.google.com/&response_type=code&scope=user_profile,user_media";
//  final Completer<WebViewPlusController> _controller =
//  Completer<WebViewPlusController>();
  bool webPageLoaded=false;
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    final flutterWebviewPlugin = new FlutterWebviewPlugin();

    flutterWebviewPlugin.onUrlChanged.listen((String url) async{
      if(url.contains("https://www.google.com/?code")){
             var token =await fetchToken(url.split("code=")[1].split("#_")[0],context);
             if(!resultOk){
               resultOk=true;
               MyRouter.navigatorKey.currentState.pop(token);
             }

            }
    });
  }
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: widget.otherUrl??url,
      appBar: AppBar(title: Text(widget.title??"Sign in",style: Theme.of(context).textTheme.body2.copyWith(
          color: Colors.black,
          fontSize: SizeConfig.blockSizeWidth * 5)),centerTitle: true,elevation: 0.0,),
      withZoom: true,
      withLocalStorage: true,
      hidden: true,

      initialChild: Container(
        color: Colors.white,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  } //  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(title: Text(widget.title??"Sign in",),centerTitle: true,elevation: 0.0,),
//      body: Container(
//        color: Colors.white,
//        child: WebViewPlus(
//          initialUrl: widget.otherUrl??url,
//          javascriptMode: JavascriptMode.unrestricted,
//         onWebViewCreated: (c){
//           _controller.complete(c);
//         },
//
//          onPageStarted: (String url) async{
//            print('Page started loading: $url');
//
//          },
//          onPageFinished: (String url) async{
//            print('Page finished loading: $url');
//            if(url.contains("https://www.google.com/?code")){
//             var token =await fetchToken(url.split("code=")[1].split("#_")[0],context);
//             if(!resultOk){
//               resultOk=true;
//               MyRouter.navigatorKey.currentState.pop(token);
//             }
//
//            }
//          },
//          gestureNavigationEnabled: true,
//        ),
//      )
//    );
//  }

  Future<String> fetchToken(String code,BuildContext context) async{
    final  response = await http.post(
        "https://api.instagram.com/oauth/access_token",
        body: {"client_id": _appId, "redirect_uri": "https://www.google.com/", "client_secret": _appSecret,
          "code": code, "grant_type": "authorization_code","scopes":"[user_profile]"});
    print(response.body);
    if(convert.jsonDecode(response.body)["access_token"]!=null)
      return convert.jsonDecode(response.body)["access_token"];
          else
            return null;
  }
}
