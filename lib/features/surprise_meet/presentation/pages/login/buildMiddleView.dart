import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surprise_meet/core/utils/animteddialog.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/login/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/login/login_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/forgotpasswordui.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonpasswordfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

class BuildMiddleView extends StatefulWidget {
  @override
  _BuildMiddleViewState createState() => _BuildMiddleViewState();
}

class _BuildMiddleViewState extends State<BuildMiddleView> {
  bool visibility = true;
  FocusNode emailNode = FocusNode();
  FocusNode passNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return _buildMiddleView(context);
  }

  Container _buildMiddleView(BuildContext context) {
    var bloc = BlocProvider.of<LoginBloc>(context);
    return Container(
//      padding: const EdgeInsets.only(left: 32.0),
      child: Column(
        children: <Widget>[
          StreamBuilder(
            stream: bloc.email,
            builder: (context, snapshot) {
              return CommonInputField(
                controller: bloc.emailTextController,
                errorText: snapshot.error,
                icon: FontAwesomeIcons.envelope,
                textChanged: bloc.changeEmail,
                labelText: "Email",
                node: emailNode,
                onSubmit: (value) {
                  FocusScope.of(context).requestFocus(passNode);
                },
                action: TextInputAction.next,
              );
            },
          ),
          SizedBox(
            height: SizeConfig.safeBlockHorizontal,
          ),
          StreamBuilder(
            stream: bloc.password,
            builder: (context, snapshot) {
              return StreamBuilder<bool>(
                stream: bloc.validCredentials,
                builder: (context, valid) {
                  return CommonPasswordField(
                    controller: bloc.passController,
                    labelText: "Password",
                    errorText: snapshot.error,
                    onSubmit: (value) {
                      if(valid.data!=null&&valid.data)
                        bloc.add(UserLogin());
                      else bloc.addEmptyError();
                    },
                    changepass: bloc.changePassword,
                    action: TextInputAction.done,
                    node: passNode,
                  );
                }
              );
            },
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    showAnimatedDialog(context, passwordDialog(context));
                  },
                  child: Container(
                    decoration: BoxDecoration(border: Border(bottom: BorderSide(color: AppColors.colorAccent,width: 1.0)
                    )
                    ),
//              alignment: Alignment(1, 0),
                   // padding: const EdgeInsets.all(12.0),
                    child: Text(
                      "Forgot Password ?",
                      style: Theme.of(context)
                          .textTheme
                          .body1
//                    .copyWith(decoration: TextDecoration.underline,),
                    ),
                  ),
                ),
              ],
            ),
          ),
          StreamBuilder<bool>(
            stream: bloc.validCredentials,
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal:16.0),
                child: RaisedGradientButton(
                  gradient: AppTheme.gradient(),
                  onPressed: () {

//              MyRouter.navigator.pushNamed(MyRouter.connectFacebook,arguments: ConnectSocialPlatformsArguments(isFacebook: true,isInsta: true));
                  if(snapshot.data!=null&&snapshot.data)
                    bloc.add(UserLogin());
                  else bloc.addEmptyError();
                  },
                  text: "SIGN IN",
                  isDialog: false,
                ),
              );
            }
          ),
        ],
      ),
    );
  }
}
