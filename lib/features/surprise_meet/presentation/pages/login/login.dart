import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/utils/animteddialog.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/login/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/resetpassword/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/completeprofile/completeprofileDialog.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/injection_container.dart' as di;
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/style/strings.dart';

import 'buildBottomView.dart';
import 'buildMiddleView.dart';
import 'buildTopView.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;
  LoginBloc loginBloc;
  ResetPasswordBloc resetPasswordBloc;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isDev=ApiConstants.baseIp==ApiConstants.devIp;
  @override
  void initState() {
    super.initState();

    loginBloc = injector<LoginBloc>();
    // loginBloc.emailTextController.text="santchanana93@gmail.com";
    // loginBloc.passController.text="qwerty@123";
    // loginBloc.add(LoginEvent.userLogin());
    resetPasswordBloc = injector<ResetPasswordBloc>();
    controller =
        AnimationController(duration: Duration(seconds: 1), vsync: this);
    animation = Tween(begin: 0.1, end: 0.0).animate(
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn));
    controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      key: scaffoldKey,
      body: MultiBlocProvider(
          providers: [
            BlocProvider<LoginBloc>(
              create: (c) => loginBloc,
            ),
            BlocProvider<ResetPasswordBloc>(
              create: (c) => resetPasswordBloc,
            )
          ],
          child: MultiBlocListener(
            child: SafeArea(
              child: BlocBuilder<LoginBloc, LoginState>(
                builder: (context, state) {
                  return state.when(
                      completeProfile: (c) => buildHomeWidget(context),
                      loginInitial: (c) => buildHomeWidget(context),
                      loginLoading: (c) => Center(
                            child: CircularProgressIndicator(),
                          ),
                      loginErrorState: (c) => buildHomeWidget(context),
                      loginSuccessState: (c) => buildHomeWidget(context),
                      getFaceBookUserName: (c) => buildHomeWidget(context),
                      fBUserNameRegistered: (c) =>
                          buildHomeWidget(context));
                },
              ),
            ),
            listeners: [
              BlocListener<LoginBloc, LoginState>(
                listener: (context, state) async {
                  if (state is LoginSuccessState) {
//                    showSnackBar(scaffoldKey,state.data.message,false);
                    injector.reset();
                    await di.init();
                    MyRouter.navigatorKey.currentState.popUntil((screen) {
                      if (screen.settings.name != MyRouter.home) {
                        MyRouter.navigatorKey.currentState
                            .pushReplacementNamed(MyRouter.home);
                      }
                      return true;
                    });
                  } else if (state is LoginErrorState) {
                    showSnackBar(context, state.error, true);
                  } else if (state is CompleteProfile) {
                    injector.reset();
                    await di.init();
                    var user = state.data;
                    double profileCompletedPer = 0;
                    if ( !user.isInsta) {
                      profileCompletedPer = 75;
                    }
//                    else {
//                      profileCompletedPer = 75;
//                    }
                    //navigate to complete profile
//                    MyRouter.navigator.pushNamed(MyRouter.connectFacebook,arguments: ConnectSocialPlatformsArguments(isFacebook: true,isInsta: true));

                    showAnimatedDialog(
                        context,
                        CompleteProfileDialog(
                          profileCompletedPer: profileCompletedPer,
                          onClick: () {
                            navigateAccordingToState(state);
                          },
                          onSkipClick: () {
                            MyRouter.navigatorKey.currentState
                                .pushNamedAndRemoveUntil(
                                    MyRouter.home, (MyRouter) => false);
                          },

                        ),barrierDismissible: false);
                  } else if (state is FBUserNameRegistered) {
                    loginBloc.add(FBLogin());
                  } else if (state is GetFaceBookUserName) {
                    showModalBottomSheet<void>(
                      isScrollControlled: true,
                      context: context,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30.0),
                            topRight: Radius.circular(30.0)),
                      ),
                      builder: (BuildContext context) {
                        return Padding(
                            padding: MediaQuery.of(context).viewInsets,
                            child: Container(
                                child: Wrap(
                              children: <Widget>[

                                Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      child: const Icon(
                                        FontAwesomeIcons.facebook,
                                        color: Colors.white,
                                        size: 45.0,
                                      ),
                                      width: SizeConfig.screenWidth,
                                      padding: const EdgeInsets.all(16.0),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(30.0),
                                              topRight: Radius.circular(30.0)),
                                          gradient: AppTheme.gradient()),
                                    ),
                                    getBottomSizedBox(),
                                    Container(
                                      width: SizeConfig.screenWidth,
//                                          color: Colors.red,
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Add facebook username to get started",
                                        style: Theme.of(context)
                                            .textTheme
                                            .title
                                            .copyWith(fontSize: 18.0),
                                      ),
                                    ),
                                  ],
                                ),
                                getBottomSizedBox(),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: StreamBuilder<String>(
                                      stream: loginBloc.userName,
                                      builder: (context, snapshot) {
                                        return CommonInputField(
                                          controller:
                                              loginBloc.userNameController,
                                          onSubmit: (value) {
                                            if (loginBloc.userNameController
                                                .text.isNotEmpty) {
                                              MyRouter.navigatorKey.currentState
                                                  .pop();
                                            }
                                            loginBloc.add(LoginEvent.fBLogin());
                                          },
                                          errorText: snapshot.error != null
                                              ? "Username ${snapshot.error}"
                                              : null,
                                          textChanged: loginBloc.changeUserName,
                                          icon: FontAwesomeIcons.facebookF,
                                          hasSuffix: true,
                                          action: TextInputAction.done,
                                          labelText: "Facebook Username",
                                        );
                                      }),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: RaisedGradientButton(
                                    onPressed: () {
                                      if (loginBloc.userNameController.text.isNotEmpty) {
                                        MyRouter.navigatorKey.currentState
                                            .pop();
                                      }
                                      loginBloc.add(LoginEvent.fBLogin());
                                    },
                                    text: "Submit",
                                    isDialog: true,
                                  ),
                                )
                              ],
                            )));
                      },
                    );
                  }
                },
              ),
              BlocListener<ResetPasswordBloc, ResetPasswordState>(
                listener: (context, state) {
                  if (state is SuccessState) {
                    Navigator.pop(context);
                    resetPasswordBloc.emailTextController.clear();
                    showSnackBar(context, state.data.message, false);
                  } else if (state is LoadingState) {
                    Navigator.pop(context);
                    showDialog(
                        context: context,
                        child: Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ));
                  } else if (state is ErrorState) {
                    Navigator.pop(context);
                    showSnackBar(context, state.error, true);
                  }
                },
              )
            ],
          )),
    );
  }

  Widget buildHomeWidget(BuildContext context) {
    controller.forward();
    return Center(
      child: SingleChildScrollView(
        child: Column(
//                mainAxisAlignment: MainAxisAlignment.center,
//
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            // CheckboxListTile(value: isDev, onChanged: (bool value) {
            //   setState(()  {
            //     isDev=value;
            //     if(isDev){
            //       ApiConstants.baseIp=ApiConstants.devIp;
            //     }
            //     else ApiConstants.baseIp=ApiConstants.liveIp;
            //     di.injector.reset();
            //     di.init();
            //     showSnackBar(context, ApiConstants.baseIp, false);
            //   });
            // },title: Text("Dev URL"),),
            buildSizedBox(),
            Flexible(
              flex: 1,
              fit: FlexFit.loose,
              child: Transform(
                  transform: Matrix4.translationValues(
                      0, animation.value * SizeConfig.screenHeight, 0),
                  child: BuildTopView(
                    title: Strings.loginTitle,
                    subtitle: Strings.signInToContinue,
                  )),
            ),
            buildSizedBox(),
            Flexible(
              flex: 1,
              child: Transform(
                child: BuildMiddleView(),
                transform: Matrix4.translationValues(
                    0, animation.value * SizeConfig.screenHeight, 0),
              ),
            ),
            buildSizedBox(),
            Flexible(
              flex: 1,
              child: Transform(
                child: BuildBottomView(scaffoldKey:scaffoldKey),
                transform: Matrix4.translationValues(
                    0, animation.value * SizeConfig.screenHeight, 0),
              ),
            )
          ],
        ),
      ),
    );
  }

  SizedBox buildSizedBox() {
    return SizedBox(
      height: SizeConfig.blockSizeHeight * 5,
    );
  }

  @override
  void dispose() {
    controller.dispose();
    loginBloc.dispose();
    resetPasswordBloc.dispose();
    super.dispose();
  }

  void navigateAccordingToState(CompleteProfile state) {
    var user = state.data;
//    if (!user.isFacebook && !user.isInsta) {
//      // profile is 50% complete
//      // navigate to connect social networks
//      navigate(true, true);
//    } else if (user.isInsta && !user.isFacebook) {
//      // profile is 75% complete
//      // navigate to connect instagram
//      navigate(false, true);
//    } else {
//      // profile is 75% complete
//      // navigate to connect facebook
//      navigate(true, false);
//    }
  if(!user.isInsta){
    navigate(true, false);
  }
  }

  void navigate(bool isInsta, bool isFacebook) {
    MyRouter.navigatorKey.currentState.pushReplacementNamed(
        MyRouter.connectFacebook,
        arguments: ConnectSocialPlatformsArguments(
            isInsta: isInsta,
            isFacebook: isFacebook,
            fromCompleteDialog: true));
  }
}
