import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/resetpassword/resetpassword_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/resetpassword/resetpassword_event.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoncarview.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

Widget passwordDialog(BuildContext context) {
  var bloc = BlocProvider.of<ResetPasswordBloc>(context);
  return CommonCard(
    topContainerChild: Icon(
      Icons.lock,
      color: Colors.white,
      size: 60.0,
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(32.0),
          child: Text(
            'Please enter your registered email address to reset your password',
//                style: Styles.semiBoldHeadingFour(),
            style: Theme.of(context).textTheme.body2,
            textAlign: TextAlign.center,
          ),
        ),

        StreamBuilder<String>(
          stream: bloc.email,
          builder: (context, snapshot) => CommonInputField(
            node: null,

            controller: bloc.emailTextController,
            errorText: snapshot.error,
            icon: FontAwesomeIcons.envelope,
            textChanged: bloc.changeEmail,
            labelText: "Email",
            onSubmit: (value) {},
            action: TextInputAction.done,
          ),
        ),
        Padding(
          padding:  EdgeInsets.symmetric(horizontal: SizeConfig.blockSizeWidth*4),
          child: RaisedGradientButton(
            gradient: AppTheme.gradient(),
            onPressed: () {
              bloc.add(ResetPassword());
            },
            text: "Send Email",
            isDialog: true,
          ),
        ),
        SizedBox(height: SizeConfig.safeBlockVertical * 2),
//              RaisedGradientButton(widget.key, () {
//                if (emailID != null &&
//                    _emailError == null &&
//                    emailID.isNotEmpty &&
//                    EmailValidator.validate(emailID)) {
//                  loginBloc.forgetPassword(context, emailID);
//                } else {
//                  setEmaiError(emailID);
//                }
//              }, "Reset Password")
      ],
    ),
  );
}
