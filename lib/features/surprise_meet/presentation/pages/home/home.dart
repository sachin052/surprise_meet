import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/home_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/managepotentialmeet_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/managepotentialmeet_event.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/chat/chathomescreen.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/homepage/potentialmeet.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/profile/profile.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/main.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

class Home extends StatefulWidget {
  final bool fromPush;

  const Home({Key key, this.fromPush = false}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  HomeBloc _homeBloc;
  ManagePotentialMeetBloc _managePotentialMeetBloc;
  List<Widget> pages= [ProfilePageWidget(),PotentialMeetsWidget(),ChatPageWidget()];
  ProfilePageWidget profilePageWidget;
  PotentialMeetsWidget potentialMeetsWidget;
  ChatPageWidget chatPageWidget;
  Animation _colorTween;
  AnimationController _animationController;
  @override
  void initState() {
    _homeBloc = injector<HomeBloc>();
    // _homeBloc.changeNotificationCount(0);
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 100));
    _colorTween = ColorTween(begin: AppColors.redTextColor, end: AppColors.scaffoldBGColor).animate(_animationController);
    _managePotentialMeetBloc=injector<ManagePotentialMeetBloc>();
    if (widget.fromPush) {
      _homeBloc.add(PageTapped(CurrentPageScreen.chatScreen()));
    }
    profilePageWidget = ProfilePageWidget();
    potentialMeetsWidget = PotentialMeetsWidget();
    chatPageWidget = ChatPageWidget();

    _homeBloc.currentPageIndex.listen((data){
      print("Current page"+data.toString());
    });
    _homeBloc.currentDataStatus.listen((event) {
       print('current event is $event');
       if(event){
         _animationController.reverse();
       }
       else{
         _animationController.forward();
       }
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SizeConfig().init(context);
    return MultiBlocProvider(
      providers:[
        BlocProvider(create: (c)=>_homeBloc,),
        BlocProvider(create: (c)=>_managePotentialMeetBloc,),
      ],
      child: StreamBuilder<bool>(
          stream: _homeBloc.currentDataStatus,
          builder: (context, snapshot) {
            print("data is" + snapshot.data.toString());
            return WillPopScope(
              onWillPop: () {
                SystemNavigator.pop(animated: true);
                return;
              },
              child: AnimatedBuilder(
                animation: _colorTween,
               builder: (_,i)=> Scaffold(
                 // backgroundColor: snapshot?.data == true
                 // ? AppColors.redTextColor
                 // : AppColors.scaffoldBGColor,
                   backgroundColor: _colorTween.value,
                   floatingActionButtonLocation:
                   FloatingActionButtonLocation.centerDocked,
                   floatingActionButton: StreamBuilder<int>(
                       stream: _homeBloc.currentPageIndex,
                       builder: (context, snapshot) {
                         return GestureDetector(
                           onTap: () {
                             // notification.add("reset");
                             // _homeBloc.changeNotificationCount(0);
                             _managePotentialMeetBloc.add(GetPotentialMeets());
                             _homeBloc.changePage(1);
                             _homeBloc
                                 .add(PageTapped(CurrentPageScreen.potentialMeets()));
                           },
                           child: Padding(
                             padding: const EdgeInsets.all(4.0),
                             child: Container(
//                  margin: EdgeInsets.all(5.0),
                               padding: const EdgeInsets.all(16.0),
                               decoration: BoxDecoration(
                                   shape: BoxShape.circle,
                                   gradient: snapshot?.data == 1

                                       ?null:AppTheme.gradient(),
                                   color: snapshot?.data == 1 ? Colors.white : null,
                                   boxShadow: [
                                     BoxShadow(
                                         offset: Offset(-2, -2),
                                         color: Colors.black.withOpacity(.3),
                                         blurRadius: 10.0)
                                   ]),
                               child: SvgPicture.asset(snapshot.data==1?Images.heartOn:Images.heartOff),
                             ),
                           ),
                         );
                       }
                   ),
                   bottomNavigationBar: MyBottomBar(),
                   body: IndexedStack(
                     index: 0,
                     children: [
                       BlocBuilder<HomeBloc, HomeState>(
                         builder: (context, state) {
                           if (state == null) {
                             return Center(
                               child: CircularProgressIndicator(),
                             );
                           }
                           if (state is HomeScreenLoading) {
                             return Center(
                               child: CircularProgressIndicator(),
                             );
                           } else if (state is ChatScreenPage) {
                             return pages[2];
                           } else if (state is PotetentialMeetsPage) {
                             return pages[1];
                           } else if (state is ProfileScreenpage) {
                             return pages[0];
                           } else {
                             return Center(
                               child: CircularProgressIndicator(),
                             );
                           }
                         },
                       )
                     ],
                   )),
              ),
            );
          }),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    _homeBloc.dispose();
    _homeBloc.close();
    super.dispose();
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}
class MyBottomBar extends StatefulWidget {
  @override
  _MyBottomBarState createState() => _MyBottomBarState();
}

class _MyBottomBarState extends State<MyBottomBar> with AutomaticKeepAliveClientMixin {
  HomeBloc _homeBloc;
  @override
  void initState() {
    _homeBloc=BlocProvider.of<HomeBloc>(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: StreamBuilder<int>(
          stream: _homeBloc.currentPageIndex,
          builder: (context, snapshot) {
            return Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: FlatButton(
                      onPressed: () {
                        notification.add("reset");
                        _homeBloc.changeNotificationCount(0);
                        _homeBloc.changePage(0);
                        _homeBloc.add(PageTapped(
                            CurrentPageScreen.chatScreen()));
                      },
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Stack(
                            overflow: Overflow.visible,
                            children: <Widget>[
                              SvgPicture.asset(
                                  Images.chatIcon,
                                  height: SizeConfig.blockSizeHeight * 2.5,
                                  color: snapshot?.data == 0
                                      ? AppColors.redTextColor
                                      : Colors.black.withOpacity(.5)),
                              // Icon(
                              //   FontAwesomeIcons.comment,
                              //   size: SizeConfig.blockSizeWidth * 5,
                              //   color: snapshot?.data == 0
                              //       ? AppColors.redTextColor
                              //       : Colors.black.withOpacity(.5),
                              // ),
                              StreamBuilder<int>(
                                  stream: _homeBloc.currentNotificationCount.distinct(),
                                  builder: (context, snapshot) {
                                    print("notification count"+snapshot.data.toString());
                                    return Visibility(
                                      visible: snapshot.data!=null?snapshot.data>0:false,
                                      child: Positioned(
                                        right: -5.0,
                                        child: new Container(
                                          padding: EdgeInsets.all(1),
                                          decoration: new BoxDecoration(
                                            color: Colors.red,
                                            borderRadius: BorderRadius.circular(7),
                                          ),
                                          constraints: BoxConstraints(
                                            minWidth: 14,
                                            minHeight: 14,
                                          ),
                                          child: new Text(
                                            snapshot.data.toString(),
                                            style: new TextStyle(
                                              color: Colors.white,
                                              fontSize: 8,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    );
                                  }
                              )
                            ],
                          ),
                          SizedBox(height: 2.0,),
                          Text("Matches",style: Theme.of(context).textTheme.body2.copyWith(fontSize: 13.0,color:snapshot?.data == 0
                              ? AppColors.redTextColor
                              : Colors.black.withOpacity(.5) ),)
                        ],
                      ),
                    )),
                Expanded(
                  flex: 1,
                  child: FlatButton(
                    onPressed: () {},
                    child: Text(""),
                  ),
                ),
                Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: FlatButton(
                        onPressed: () {
                          _homeBloc.changePage(2);
                          _homeBloc.add(PageTapped(
                              CurrentPageScreen.profile()));
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SvgPicture.asset(
                              Images.userIcon,
                              height: SizeConfig.blockSizeHeight * 2.5,
                              color: snapshot?.data == 2
                                  ? AppColors.redTextColor
                                  : Colors.black.withOpacity(.5),
                            ),
                            SizedBox(height: 2.0,),
                            Text("Profile",style: Theme.of(context).textTheme.body2.copyWith(fontSize: 13.0,color:snapshot?.data == 2
                                ? AppColors.redTextColor
                                : Colors.black.withOpacity(.5) ),)
                          ],
                        ),
                      ),
                    )),
              ],
            );
          }),
      shape: CircularNotchedRectangle(),
      color: Colors.white,
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
