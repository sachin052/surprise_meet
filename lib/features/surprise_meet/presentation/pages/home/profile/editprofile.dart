import 'dart:io';

import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/dialogs.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/utils/numberkeyboard.dart';

class EditProfile extends StatefulWidget {
  final Userprofile userProfile;

  const EditProfile({Key key, this.userProfile}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  UpdateProfileBloc _profileBloc;
  FocusNode phoneNode;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _profileBloc = injector<UpdateProfileBloc>();
    _profileBloc = injector<UpdateProfileBloc>();
    phoneNode=FocusNode();
//    phoneNode.addListener((){
//      if(phoneNode.hasFocus&&Platform.isIOS){
//        showOverLay(context);
//      }
//      else{
//        removeOverLay();
//      }
//    });
    _profileBloc.nameController.text = widget.userProfile.name;
    _profileBloc.facebookController.text = widget.userProfile.facebookId;
    _profileBloc.instaController.text = widget.userProfile.instaId;
    _profileBloc.emailController.text = widget.userProfile.email;
    print(widget.userProfile.phone);
    try {
      if(widget.userProfile.phone!=null&&widget.userProfile.phone.isNotEmpty){
            _profileBloc.phoneController.text = widget.userProfile.phone!=null&&widget.userProfile.phone.isNotEmpty?widget?.userProfile?.phone?.split("-")[1]:"";
            _profileBloc.changeCountryCode(Country(phoneCode: widget?.userProfile?.phone?.split("-")[0].replaceAll("+", "")??"1"));
          }
    } catch (e) {
      print(e);
    }

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        removeOverLay();
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            widget.userProfile.name??"No Name",
            style: Theme.of(context)
                .textTheme
                .headline
                .copyWith(fontSize: SizeConfig.blockSizeWidth * 5),
          ),
          centerTitle: true,
        ),
        body: MultiBlocProvider(
          child: BlocListener<UpdateProfileBloc, UpdateProfileState>(
            listener: (context, state) {
              if (state is SuccessProfileState) {
                MyRouter.navigatorKey.currentState.pop("profileUpdate");
              }
            },
            child: BlocBuilder<UpdateProfileBloc, UpdateProfileState>(
              builder: (context, state) {
                return state.when(
                  getFbUserName: (c)=>buildHomeWidget(context),
                    intialProfileState: (c) => buildHomeWidget(context),
                    loadingProfileState: (c) => Center(
                          child: CircularProgressIndicator(),
                        ),
                    errorProfileState: (error) => Center(
                          child: Text(error.error),
                        ),
                    faceBookVerified: (c) {},
                    instaVerified: (c) {},
                    successProfileState: (c) => buildHomeWidget(context), fbUserNameRegistered: (FbUserNameRegistered ) =>buildHomeWidget(context));
              },
            ),
          ),
          providers: [
            BlocProvider<UpdateProfileBloc>(
              create: (c) => _profileBloc,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHomeWidget(BuildContext context) {
    return KeyboardActions(
      config: KeyboardActionsConfig(keyboardActionsPlatform: KeyboardActionsPlatform.ALL,actions: [
        KeyboardActionsItem(focusNode: FocusNode())
      ]),
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 42.0, top: 16.0),
            child: RichText(
                textAlign: TextAlign.start,
                text: TextSpan(children: [
                  TextSpan(
                    text: "Make it more",
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .copyWith(fontSize: SizeConfig.blockSizeWidth * 6.5),
                  ),
                  TextSpan(
                    text: " Alluring!",
                    style: Theme.of(context).textTheme.title.copyWith(
                        fontSize: SizeConfig.blockSizeWidth * 6.5,
                        color: Color(0xFFE4184D)),
                  ),
                ])),
          ),
          SizedBox(
            height: SizeConfig.blockSizeWidth * 15,
          ),
          CommonInputField(
            icon: FontAwesomeIcons.userAlt,
            errorText: null,
            labelText: "Name",
            controller: _profileBloc.nameController,
            node: null,
            textChanged: (value) {},
            onSubmit: (value) {
              FocusScope.of(context).requestFocus(phoneNode);
//                moveNode(facebookNode);
            },
          ),
//          SizedBox(
//            height: SizeConfig.blockSizeWidth * 3,
//          ),
//          CommonInputField(
//            icon: FontAwesomeIcons.facebookF,
//            errorText: null,
//            labelText: "Facebook",
//            isReadOnly: true,
//            controller: _profileBloc.facebookController,
////              node: facebookNode,
//            textChanged: (value) {},
//            onSubmit: (value) {
////                moveNode(instagramNode);
//            },
//          ),
          SizedBox(
            height: SizeConfig.blockSizeWidth * 3,
          ),
          CommonInputField(
            icon: FontAwesomeIcons.instagram,
            errorText: null,
            labelText: "Instagram",
            isReadOnly: true,
//              node: instagramNode,
            controller: _profileBloc.instaController,
            textChanged: (value) {},
            onSubmit: (value) {
//                moveNode(emailNode);
            },
          ),
          SizedBox(
            height: SizeConfig.blockSizeWidth * 3,
          ),
          CommonInputField(
            icon: FontAwesomeIcons.envelope,
            errorText: null,
            labelText: "Email",
            isReadOnly: true,
            controller: _profileBloc.emailController,
//              node: emailNode,
            textChanged: (value) {},
            onSubmit: (value) {
//                moveNode(phoneNode);
            },
          ),
          SizedBox(
            height: SizeConfig.blockSizeWidth * 3,
          ),
          StreamBuilder<Country>(
            stream: _profileBloc.countryCode,
            builder: (context, snapshot) {
              return CommonInputField(
                errorText: null,
                textChanged: null,
                node: phoneNode,
                action: TextInputAction.done,
                controller: _profileBloc.phoneController,
                countryCode: snapshot?.data?.phoneCode,
                onCountryTap: (){
                  FocusScope.of(context).requestFocus(FocusNode());
                  showCountryPicker(context, _profileBloc.changeCountryCode);

                },
                inputType: TextInputType.number,
//              node: phoneNode,
                onSubmit: (value) {
//                moveNode(passNode);
                },

//              errorText: snapshot.error,
                labelText: "Phone",
//              changeEmail: bloc.changePhone,
                icon: FontAwesomeIcons.phoneAlt,
              );
            }
          ),
          SizedBox(
            height: SizeConfig.blockSizeWidth * 3,
          ),
          RaisedGradientButton(
            isDialog: true,
            onPressed: () {
              if(_profileBloc.nameController.text.isEmpty){
                showSnackBar(context, "Name must not be empty", true);
              }
              else if(_profileBloc.phoneController.text.isEmpty){
                showSnackBar(context, "Phone must not be empty", true);
              }
              else _profileBloc.add(UpdateUserProfile());
            },
            text: "Update",
          )
        ],
      ),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    removeOverLay();
    super.dispose();
  }
}
