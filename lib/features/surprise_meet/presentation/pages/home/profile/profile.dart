import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:division/division.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/profile_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/profile_event.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/profile_state.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/connectfacebook.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/updatepassword/updatepasswordscreen.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/showErrorView.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePageWidget extends StatefulWidget {
  @override
  _ProfilePageWidgetState createState() => _ProfilePageWidgetState();
}

class _ProfilePageWidgetState extends State<ProfilePageWidget>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;
  UpdateProfileBloc _profileBloc;
  SharedPrefHelper _sharedPrefHelperImpl;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _profileBloc = injector<UpdateProfileBloc>();
    _sharedPrefHelperImpl=injector<SharedPrefHelper>();
    _profileBloc.add(UpdateProfileEvent.getUserProfile());
    controller = AnimationController(
        duration: Duration(milliseconds: 1500), vsync: this);
    animation = Tween(begin: 0.2, end: 0.0).animate(CurvedAnimation(
        parent: controller, curve: Curves.fastLinearToSlowEaseIn));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (c) => _profileBloc,
      child: BlocListener<UpdateProfileBloc, UpdateProfileState>(
        listener: (context, state) {
          if (state is FaceBookVerified || state is InstaVerified) {
            _profileBloc.add(GetUserProfile());
          }
        },
        child: BlocBuilder<UpdateProfileBloc, UpdateProfileState>(
          builder: (context, state) {
            return state.when(
              faceBookVerified: (c) => buildHomeWidget(context),
              intialProfileState: (c) => Center(
                child: CircularProgressIndicator(),
              ),
              loadingProfileState: (c) => Center(
                child: CircularProgressIndicator(),
              ),
              errorProfileState: (error) => showErrorView(error.error, () {
                _profileBloc.add(GetUserProfile());
              }),
              successProfileState: (state) =>
                  buildHomeWidget(context, state: state),
              instaVerified: (instaVerified) {},
              fbUserNameRegistered: (FbUserNameRegistered) =>
                  buildHomeWidget(context),
              getFbUserName: (GetFbUserName) =>buildHomeWidget(context),
            );
          },
        ),
      ),
    );
  }

  Widget buildHomeWidget(BuildContext context, {SuccessProfileState state}) {
    var data = state?.data?.userprofile;
    return RefreshIndicator(
      onRefresh: () async {
        _profileBloc.add(GetUserProfile());
      },
      child: AnimatedBuilder(
        animation: controller,
        builder: (context, child) {
          return Transform(
            transform: Matrix4.translationValues(
                animation.value * SizeConfig.screenWidth, 0, 0),
            child: AnimatedOpacity(
              duration: Duration(milliseconds: 200),
              opacity: 1.0-animation.value*4,
              child: child,
            ),
          );
        },
        child: Container(
          alignment: Alignment.center,
          color: Color(0XFFEFF3F6),
          padding: EdgeInsets.only(left: 32.0),
          child: SafeArea(
            child: ListView(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                        child: Container(
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.all(16.0),
                          child: GestureDetector(
                            onTap: () async {
                              var result = await MyRouter.navigatorKey.currentState
                                  .pushNamed(MyRouter.editProfile,
                                  arguments: EditProfileArguments(
                                      userProfile: state.data.userprofile));
                              if (result != null) {
                                _profileBloc.add(GetUserProfile());
                              }
                            },
                            child: SvgPicture.asset(Images.pencilIcon),
//                 shape: CircleBorder(),
                          ),
                        ))
                  ],
                ),
                Text(
                  "Let's have a Look at your",
                  style: Theme.of(context).textTheme.title,
                ),
                Text(
                  "Profile",
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .copyWith(color: AppColors.redTextColor),
                ),
                SizedBox(
                  height: SizeConfig.blockSizeWidth * 10,
                ),
                Card(
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                  margin: EdgeInsets.only(right: 32.0),
                  child: Container(

                    padding: const EdgeInsets.all(24.0),
                    decoration: BoxDecoration(
                        gradient: AppTheme.gradient(),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: Column(
                      children: <Widget>[
                        Text(
                          state?.data?.userprofile?.name ?? "Loading ..",
                          style: Theme.of(context)
                              .textTheme
                              .title
                              .copyWith(color: Colors.white),
                        ),
                        Text(
                          state?.data?.userprofile?.email ?? "Loading ..",
                          style: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.blockSizeWidth * 10,
                ),
                Column(
                  children: <Widget>[
//                    buildProfileTile(
//                        Container(
//                          child: Icon(
//                            FontAwesomeIcons.facebookF,
//                            color: Colors.white,
//                          ),
//                          decoration: BoxDecoration(
//                            borderRadius: BorderRadius.circular(9.0),
//                            color: Color(0xFF4260A9),
//                          ),
//                          padding: const EdgeInsets.all(8.0),
//                        ),
//                        data?.facebookId?.isNotEmpty == true
//                            ? data.facebookId
//                            : " Connect facebook", () async {
//                      connectFaceBook(state.data.userprofile);
//                    }),
                    buildProfileTile(
                        Container(
                          child: Icon(
                            FontAwesomeIcons.instagram,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(9.0),
                              gradient: LinearGradient(colors: [
                                Color(0xFF6057C9),
                                Color(0xFF9244A4),
                                Color(0xFFCA2E7B),
                                Color(0xFFFECB6F),
                              ])),
                          padding: const EdgeInsets.all(8.0),
                        ),
                        data?.instaId?.isNotEmpty == true
                            ? data?.instaId
                            : " Connect Instagram", () async {
                      connectInstagram(state.data.userprofile);
                    }),
                    Visibility(
                      visible: state.data.userprofile.hasPassword,
                      child: buildProfileTile(
                          ShaderMask(
                            shaderCallback: (rect)=>AppTheme.reverseGradient().createShader(rect),
                            blendMode: BlendMode.srcATop,
                            child: Container(
                              child: Icon(
                                Icons.lock,
                                color: Colors.grey,
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(9.0),
                                  border: Border.all(color: Colors.grey, width: 2.0)),
                              padding: const EdgeInsets.all(3.0),
                            ),
                          ),
                          " Change Password",
                              () async {
                            MyRouter.navigatorKey.currentState.pushNamed(MyRouter.updatePasswordScreen);
                          },
                          enableArrow: true),
                    ),
                    buildProfileTile(
                        ShaderMask(
                          shaderCallback: (rect)=>AppTheme.reverseGradient().createShader(rect),
                          blendMode: BlendMode.srcATop,
                          child: Container(
                            child: Icon(
                              FontAwesomeIcons.shieldAlt,
                              color: Colors.grey,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                border: Border.all(color: Colors.grey, width: 2.0)),
                            padding: const EdgeInsets.all(3.0),
                          ),
                        ),
                        " Privacy Policy",
                            () {
                          MyRouter.navigator.pushNamed(MyRouter.instaWebView,arguments:InstaWebViewArguments(otherUrl: ApiConstants.privacyPolicy,title: "Privacy Policy"));
                        },
                        enableArrow: true),
                    buildProfileTile(
                        ShaderMask(
                          shaderCallback: (rect)=>AppTheme.reverseGradient().createShader(rect),
                          blendMode: BlendMode.srcATop,
                          child: Container(
                            child: Icon(
                              FontAwesomeIcons.book,
                              color: Colors.grey,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                border: Border.all(color: Colors.grey, width: 2.0)),
                            padding: const EdgeInsets.all(3.0),
                          ),
                        ),
                        " Terms & Conditions",
                            () {
                          MyRouter.navigator.pushNamed(MyRouter.instaWebView,arguments:InstaWebViewArguments(otherUrl: ApiConstants.termsCondition,title: "Terms & Conditions"));
                        },
                        enableArrow: true),
                    buildProfileTile(
                        ShaderMask(
                          shaderCallback: (rect)=>AppTheme.reverseGradient().createShader(rect),
                          blendMode: BlendMode.srcATop,
                          child: Container(
                            child: Icon(
                              FontAwesomeIcons.exclamation,
                              color: Colors.grey,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                border: Border.all(color: Colors.grey, width: 2.0)),
                            padding: const EdgeInsets.all(3.0),
                          ),
                        ),
                        " About Us",
                            () {
                          MyRouter.navigator.pushNamed(MyRouter.instaWebView,arguments:InstaWebViewArguments(otherUrl: ApiConstants.aboutUs,title: "About Us"));
                        },
                        enableArrow: true),
                    buildProfileTile(
                        ShaderMask(
                          shaderCallback: (rect)=>AppTheme.reverseGradient().createShader(rect),
                          blendMode: BlendMode.srcATop,
                          child: Container(
                            child: Icon(
                              Icons.share,
                              color: Colors.grey,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                border: Border.all(color: Colors.grey, width: 2.0)),
                            padding: const EdgeInsets.all(3.0),
                          ),
                        ),
                        " Share",
                            ()  {
                          Share.share('Simply download and install SurpriseMeet, to find matches you always wished https://mobile.surprisemeet.com/App/meet', subject: 'Surprise Meet');
                        },
                        enableArrow: true),
                    buildProfileTile(
                        ShaderMask(
                          shaderCallback: (rect)=>AppTheme.reverseGradient().createShader(rect),
                          blendMode: BlendMode.srcATop,
                          child: Container(
                            child: Icon(
                              Icons.people_outline,
                              color: Colors.grey,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                border: Border.all(color: Colors.grey, width: 2.0)),
                            padding: const EdgeInsets.all(3.0),
                          ),
                        ),
                        " Contact Us",
                            () async {
                          String version="App Version: ";
                          String osVersion="Os Version: ";
                          String deviceName="Device: ";
                          String userName="User Name: ";
                          String userID="User ID: ";
                          DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
                          PackageInfo packageInfo = await PackageInfo.fromPlatform();
                          version=version+packageInfo.version;
                          if(Platform.isAndroid){
                            AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
                            deviceName=deviceName+androidInfo.model;
                            osVersion=osVersion+androidInfo.version.release;
                          }
                          else{
                            IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
                            deviceName=deviceName+iosInfo.utsname.machine;
                            osVersion=osVersion+iosInfo.systemVersion;
                          }
                          userName=userName+_sharedPrefHelperImpl.getUserData().name;
                          userID=userID+_sharedPrefHelperImpl.getUserData().id.toString();
                          var platform = Platform.isAndroid
                              ? " Android Mobile App"
                              : "IOS Mobile App";
                          _launchURL("surprisemeet@gmail.com", "Support - $platform",
                              "$version \n $osVersion \n $deviceName \n $userName $userID");
                        },
                        enableArrow: true),
                    buildProfileTile(
                        ShaderMask(
                          shaderCallback: (rect)=>AppTheme.reverseGradient().createShader(rect),
                          blendMode: BlendMode.srcATop,
                          child: Container(
                            child: Icon(
                              Icons.star,
                              color: Colors.grey,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                border: Border.all(color: Colors.grey, width: 2.0)),
                            padding: const EdgeInsets.all(3.0),
                          ),
                        ),
                        " Rate Us",
                            () async {
                          var url;
                          String packageName;
                          PackageInfo.fromPlatform().then((PackageInfo packageInfo) async{
                            packageName= packageInfo.packageName;
                            if(Platform.isAndroid){
                              url="http://play.google.com/store/apps/details?id=$packageName";
                            }
                            else{
                              url="itms-apps://itunes.apple.com/app/1500714056?ls";
                            }
                            if(await canLaunch(url)){
                              await launch(url);
                            }
                          });

                        },
                        enableArrow: true),
                    FlatButton(
                        onPressed: () {
                          _profileBloc.add(UpdateProfileEvent.logOutUser());
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            SvgPicture.asset(Images.signOutSvg),
                            SizedBox(
                              width: 5.0,
                            ),
                            Text("Sign Out",
                                style: Theme.of(context)
                                    .textTheme
                                    .body2
                                    .copyWith(fontSize: 16.0)),
                          ],
                        ))
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildProfileTile(Widget icon, String text, VoidCallback onClick,
      {bool enableArrow = false}) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        alignment: Alignment.center,
        margin: const  EdgeInsets.fromLTRB(8.0,8.0,.0,8.0),
        padding: const EdgeInsets.all(8.0),
        decoration: const BoxDecoration(
            color: Color(0XFFEFF3F6),
            borderRadius: BorderRadius.only(topLeft: const Radius.circular(9.0),bottomLeft: const Radius.circular(9.0)),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  offset: Offset(2, 2),
                  blurRadius: 6.0,
                  spreadRadius: 3.0
              ),
              BoxShadow(
                  color: Color.fromRGBO(255, 255, 255, 1.0),
                  offset: Offset(-2, -2),
                  blurRadius: 6.0,
                  spreadRadius: 3.0
              )
            ]),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                icon,
                SizedBox(width: SizeConfig.blockSizeWidth * 4),
                Text(
                  text,
                  style: Theme.of(context).textTheme.body2,
                ),
                Visibility(
                  visible: enableArrow,
                  child: Expanded(
                      child: Container(
                        alignment: Alignment(.90, 0),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          size: SizeConfig.blockSizeHeight * 2.5,
                          color: Colors.grey,
                        ),
                      )),
                )
              ],
            ),
//          Padding(
//            padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
//            child: Container(
//              height: 1.0,
//              width: SizeConfig.screenWidth,
//              color: Colors.grey,
//            ),
//          ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }

  void connectFaceBook(Userprofile data) async {
    // Navigate to add or update facebook account and check the results
    var result;
    if (data?.facebookId?.isEmpty == true) {
//                        _profileBloc.add(UpdateProfileEvent.addFacebook());
      result = await MyRouter.navigatorKey.currentState.pushNamed(
          MyRouter.connectFacebook,
          arguments: ConnectSocialPlatformsArguments(
              isFacebook: true, addRequest: true));
    } else {
//                        _profileBloc.add(UpdateProfileEvent.updateFacebook());
      result = await MyRouter.navigatorKey.currentState.pushNamed(
          MyRouter.connectFacebook,
          arguments: ConnectSocialPlatformsArguments(
              isFacebook: true, addRequest: false));
    }
    if (result != null) {
      _profileBloc.add(GetUserProfile());
    }
  }

  void connectInstagram(Userprofile data) async {
    // Navigate to add or update insta account and check the results
    Widget screen=BlocProvider.value(value: BlocProvider.of<HomeBloc>(context),child: ConnectSocialPlatforms(),);


    if (data?.instaId?.isEmpty == true) {
//                        _profileBloc.add(UpdateProfileEvent.addFacebook());
      screen=BlocProvider.value(value: BlocProvider.of<HomeBloc>(context),child: ConnectSocialPlatforms(isInsta: true, addRequest: true),);
    } else {
//                        _profileBloc.add(UpdateProfileEvent.updateFacebook());
//       result = await MyRouter.navigatorKey.currentState.pushNamed(
//           MyRouter.connectFacebook,
//           arguments: ConnectSocialPlatformsArguments(
//               isInsta: true, addRequest: false));
      screen=BlocProvider.value(value: BlocProvider.of<HomeBloc>(context),child: ConnectSocialPlatforms( isInsta: true, addRequest: false),);
    }
    var result= await MyRouter.navigatorKey.currentState.push(CupertinoPageRoute(
        builder: (_)=>screen
    ));
    if (result != null) {
      print("request profile");
      _profileBloc.add(GetUserProfile());
    }
  }
  _launchURL(String toMailId, String subject, String body) async {
    String uri = 'mailto:surprisemeet@gmail.com?subject=${Uri.encodeComponent(subject)}&body=${Uri.encodeComponent(body)}';
    try {
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        showSnackBar(context, "Kindly check your email setup", true);
      }
    } catch (e) {
      showSnackBar(context, "Kindly check your email setup", true);
    }
  }
}

