

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/home_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/managepotentialmeet_event.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/showErrorView.dart';
import 'package:surprise_meet/injection_container.dart';

import 'buildhomescreen.dart';
import 'emptywidget.dart';

class PotentialMeetsWidget extends StatefulWidget {
  @override
  _PotentialMeetsWidgetState createState() => _PotentialMeetsWidgetState();
}

class _PotentialMeetsWidgetState extends State<PotentialMeetsWidget>
    with SingleTickerProviderStateMixin,AutomaticKeepAliveClientMixin {
  ManagePotentialMeetBloc _managePotentialMeetBloc;
  GlobalKey<AnimatedListState> animatedKey = GlobalKey<AnimatedListState>();
  int count=0;
  HomeBloc _homeBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    _managePotentialMeetBloc.dispose();
//    _managePotentialMeetBloc.close();
    _homeBloc=BlocProvider.of<HomeBloc>(context);
    _managePotentialMeetBloc =BlocProvider.of<ManagePotentialMeetBloc>(context);
    _homeBloc.changeDataStatus(false);
    _homeBloc.changeNotificationCount(0);
    _managePotentialMeetBloc.add(GetPotentialMeets());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ManagePotentialMeetBloc, ManagePotentialMeetState>(
      listener: (context, state) {
        if (state is MeetDeleted) {
          _managePotentialMeetBloc.add(GetPotentialMeets());
        } else if (state is HasDataPotentialMeetState) {
         _homeBloc.changeDataStatus(true);
        } else if (state is NoDataPotenaialMeetState) {
          _homeBloc.changeDataStatus(false);
        }
      },
      child: BlocBuilder<ManagePotentialMeetBloc, ManagePotentialMeetState>(
        condition: (state,newstate){
          return state.whenOrElse(orElse: (c)=>true,hasDataPotentialMeetState: (date){
            return newstate.whenOrElse(orElse: (c)=>true,hasDataPotentialMeetState: (newDate){
              return date.data!=newDate.data;
            });
          });
      },
        builder: (context, state) {
          return state.when(
            loadintManageMeet: (c) => Center(
              child: CircularProgressIndicator(),
            ),
            initialManageMeet: (c) => Center(
              child: CircularProgressIndicator(),
            ),
//                errorPotentialMeetState: (c) => c.error.contains("unauthorized")
//                    ?
//                    : Center(
//                        child: Text(c.error),
//                      ),
            hasDataPotentialMeetState: (response) {

              if(_homeBloc.notificationCount!=response.data.notificationCount){
                _homeBloc.notificationCount=response.data.notificationCount;
                _homeBloc.changeNotificationCount(response.data.notificationCount);
              }
              else{
                _homeBloc.changeNotificationCount(response.data.notificationCount);
              }

              return BuildHomeScreen(
                data: response.data.potentialMeetEntityList,
              );
            },
            noDataPotenaialMeetState: (c) => EmptyPotentialMeetWidget(),
            meetAdded: (meetAdded) {},
            meetDeleted: (meetDeleted) {},
            updateMeet: (updateMeet) {},
            errorPotentialMeetState: (error) =>
                showErrorView(error.error, () {
              _managePotentialMeetBloc.add(GetPotentialMeets());
            }),
          );
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
