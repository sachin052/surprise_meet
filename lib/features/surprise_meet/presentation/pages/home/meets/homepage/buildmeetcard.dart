import 'package:division/division.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/utils/animteddialog.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/homepage/editpotentialmeet.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'deletedialog.dart';

class BuildMeetList extends StatefulWidget {
  final PotentialMeetEntity potentialMeetEntity;

  const BuildMeetList({Key key, this.potentialMeetEntity}) : super(key: key);

  @override
  _BuildMeetListState createState() => _BuildMeetListState();
}

class _BuildMeetListState extends State<BuildMeetList>
    with SingleTickerProviderStateMixin {
  var meetEntity;
  AnimationController editDeleteController;
  Animation editDeleteAnimation;
  bool pressed = false;
  ManagePotentialMeetBloc _managePotentialMeetBloc ;
  @override
  void initState() {
    // TODO: implement initState
    meetEntity = widget.potentialMeetEntity;
    super.initState();
    _managePotentialMeetBloc= BlocProvider.of<ManagePotentialMeetBloc>(context);
    editDeleteController =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    editDeleteAnimation = Tween(begin: 0.0, end: 35.0).animate(CurvedAnimation(
        parent: editDeleteController,
        curve: Curves.fastLinearToSlowEaseIn,
        reverseCurve: Curves.fastOutSlowIn));
   _managePotentialMeetBloc.editMode.listen((event) {
     if(event)editDeleteController.forward();
     else editDeleteController.reverse();
   });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      overflow: Overflow.clip,
      children: <Widget>[
        BuildMeetItem(
          detectGesture: (details) {
            if (details.delta.dx > 0) {
              // swiping in right direction
              editDeleteController.reverse();
            } else {
              editDeleteController.forward();
            }
          },
          onLongPress: () {
            print("Long Press" + editDeleteController.value.toString());
            if (editDeleteAnimation.value > 10) {
              editDeleteController.reverse();
            } else {
              editDeleteController.forward();
            }
          },
          title: widget.potentialMeetEntity.meetName,
          onClick: () async {
            editMeetEntity();
          },
        ),
        Align(
          child: AnimatedBuilder(
            animation: editDeleteController,
            builder: (context, child) {
              return AnimatedContainer(
                duration: (Duration(milliseconds: 200)),
                height: SizeConfig.blockSizeHeight * 10,
                margin: EdgeInsets.only(
                  top: 10,
                ),
                width: SizeConfig.blockSizeWidth * editDeleteAnimation.value,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    color: Color(0xFFE8E8E8)),
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          child: Icon(
                            Icons.edit,
                            size: 0.7 * editDeleteAnimation.value,
                            color: Colors.black.withOpacity(.8),
                          ),
                          onTap: () async {
                            editMeetEntity();
                          },
                        ),
                      ),
                      Expanded(
                          child: GestureDetector(
                              onTap: () {
                                showAnimatedDialog(
                                    context,
                                    deleteDialog(context, () {
                                      MyRouter.navigatorKey.currentState.pop();
                                      _managePotentialMeetBloc.add(
                                          DeletePotentialMeet(
                                              meetEntity.meetId.toString()));
                                    }));
                              },
                              child: Icon(
                                Icons.delete,
                                size: 0.7 * editDeleteAnimation.value,
                                color: Colors.black.withOpacity(.6),
                              ))),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                  ),
                ),
              );
            },
          ),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }

  @override
  void dispose() {
    editDeleteController.dispose();
    super.dispose();
  }

  void editMeetEntity() async {
    var managePotentialMeetBloc =
        BlocProvider.of<ManagePotentialMeetBloc>(context);
//    var result = await MyRouter.navigatorKey.currentState
//        .pushNamed(MyRouter.editPotentialMeet,
//        arguments:
//        EditPotentialMeetArguments(userData: meetEntity)
    var result = await MyRouter.navigatorKey.currentState.push(CupertinoPageRoute(
      builder: (c) => BlocProvider.value(
        value: BlocProvider.of<ManagePotentialMeetBloc>(context),
        child: EditPotentialMeet(
          userData: meetEntity,
        ),
      ),
    ));
    if (result != null) {
      managePotentialMeetBloc.add(GetPotentialMeets());
    }
  }
}

class BuildMeetItem extends StatefulWidget {
  BuildMeetItem(
      {@required this.detectGesture,
      @required this.onLongPress,
      @required this.title,
      @required this.onClick});

  final Function(DragUpdateDetails) detectGesture;
  final VoidCallback onLongPress;
  final String title;
  final VoidCallback onClick;

  @override
  _BuildMeetItemState createState() => _BuildMeetItemState();
}

class _BuildMeetItemState extends State<BuildMeetItem> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: settingsItemStyle(pressed),
      gesture: Gestures()
        ..onTap(widget.onClick)
        ..isTap((isTapped) => setState(() => pressed = isTapped))
        ..onPanUpdate(widget.detectGesture)
        ..onLongPress(widget.onLongPress),
      child: Container(
          height: SizeConfig.blockSizeHeight * 10,
          alignment: Alignment.centerLeft,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Text(
                  widget.title,
                  style: Theme.of(context).textTheme.body2,
                ),
              ),
              Icon(Icons.arrow_forward_ios,size: 14.0,)
            ],
          )),
    );
  }

  final settingsItemStyle = (pressed) => ParentStyle()
    ..elevation(pressed ? 0 : 50, color: Colors.grey)
    ..scale(pressed ? 0.95 : 1.0)
    ..height(SizeConfig.blockSizeHeight * 10)
    ..margin(vertical: 10)
    ..alignment.centerLeft(true)
    ..width(SizeConfig.screenWidth)
    ..padding(all: 16.0)
    ..borderRadius(all: 15)
    ..background.hex('#ffffff')
    ..ripple(true)
    ..animate(150, Curves.easeOut);

  final TxtStyle itemTitleTextStyle = TxtStyle()
    ..bold()
    ..fontSize(16);
}
