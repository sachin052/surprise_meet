import 'dart:io';

import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/dialogs.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/utils/numberkeyboard.dart';

class EditPotentialMeet extends StatefulWidget {
  final PotentialMeetEntity userData;

  const EditPotentialMeet({Key key, this.userData}) : super(key: key);

  @override
  _EditPotentialMeetState createState() => _EditPotentialMeetState();
}

class _EditPotentialMeetState extends State<EditPotentialMeet>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;
  ManagePotentialMeetBloc _managePotentialMeetBloc;
  GlobalKey<ScaffoldState> key=GlobalKey<ScaffoldState>();
  FocusNode facebookNode;
  FocusNode instagramNode;
  FocusNode emailNode;
  FocusNode phoneNode;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   facebookNode=FocusNode();
    instagramNode=FocusNode();
    phoneNode=FocusNode();
    emailNode=FocusNode();
    _managePotentialMeetBloc=injector<ManagePotentialMeetBloc>();
    _managePotentialMeetBloc.nameController.text=widget.userData.meetName;
//    if(widget.userData.phone!=null&&widget.userData.phone.isNotEmpty)
////    _managePotentialMeetBloc.phoneController.text=widget?.userData?.phone?.split("-")[1]??"";
////    else

    _managePotentialMeetBloc.facebookController.text=widget.userData.facebookId;
    _managePotentialMeetBloc.instaController.text=widget.userData.instgramUsername;
    _managePotentialMeetBloc.emailController.text=widget.userData.emilId;
    if(widget.userData.phone!=null){
      if(widget.userData.phone.isNotEmpty){
        _managePotentialMeetBloc.changeCountryCode(Country(phoneCode: widget?.userData?.phone?.split("-")[0].replaceAll("+", "")??"+1"));
        _managePotentialMeetBloc.phoneController.text=widget.userData.phone.split("-")[1];
      }
    }
//  phoneNode.addListener((){
//    if(phoneNode.hasFocus&&Platform.isIOS){
//      showOverLay(context);
//    }
//    else{
//      removeOverLay();
//    }
//  });
    controller =
        AnimationController(duration: Duration(seconds: 1), vsync: this);
    animation = Tween(begin: 0.2, end: 0.0).animate(
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn));
    controller.forward();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    removeOverLay();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ManagePotentialMeetBloc>(
      create: (c)=>_managePotentialMeetBloc,
      child: BlocListener<ManagePotentialMeetBloc,ManagePotentialMeetState>(
        listener: (context,state){
          if(state is LoadintManageMeet){
            showDialog(context: context,child: Center(child: CircularProgressIndicator(),));
          }
          else if(state is ErrorPotentialMeetState){
            MyRouter.navigatorKey.currentState.pop();
            showSnackBar(context, state.error, true);
          }
          else if (state is UpdateMeet){
            MyRouter.navigatorKey.currentState.pop();
            MyRouter.navigatorKey.currentState.pop("updated");
//            var managePotentialMeetBloc =
//            BlocProvider.of<ManagePotentialMeetBloc>(context);
//            managePotentialMeetBloc.add(GetPotentialMeets());
          }
        },
        child: Scaffold(
          key: key,
          appBar: AppBar(
            elevation: 0.0,
            title: Text(
              widget.userData.meetName,
              style: Theme.of(context)
                  .textTheme
                  .headline
                  .copyWith(fontSize: SizeConfig.blockSizeWidth * 5),
            ),
            centerTitle: true,
          ),
          body: AnimatedBuilder(
            animation: controller,
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 42.0, top: 16.0),
                  child: RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(children: [
                        TextSpan(
                          text: "Would you like to update \nyour",
                          style: Theme.of(context).textTheme.title.copyWith(
                              fontSize: SizeConfig.blockSizeWidth * 6.5),
                        ),
                        TextSpan(
                          text: " Surprise Meet!",
                          style: Theme.of(context).textTheme.title.copyWith(
                              fontSize: SizeConfig.blockSizeWidth * 6.5,
                              color: Color(0xFFE4184D)),
                        ),
                        TextSpan(
                          text: " person details.",
                          style: Theme.of(context).textTheme.title.copyWith(
                              fontSize: SizeConfig.blockSizeWidth * 6.5),
                        ),
                      ])),
                ),
                SizedBox(
                  height: SizeConfig.blockSizeWidth * 15,
                ),
                CommonInputField(
                  icon: FontAwesomeIcons.userAlt,
                  errorText: null,
                  labelText: "Name",
                  controller: _managePotentialMeetBloc.nameController,
                  node: null,
                  textChanged: (value) {},
                  onSubmit: (value) {
                    moveNode(facebookNode);
                  },
                ),
//                CommonInputField(
//                  icon: FontAwesomeIcons.facebookF,
//                  errorText: null,
//                  labelText: "Facebook",
//                  controller: _managePotentialMeetBloc.facebookController,
//                  node: facebookNode,
//                  textChanged: (value) {},
//                  onSubmit: (value) {
//                    moveNode(instagramNode);
//                  },
//                ),
                CommonInputField(
                  icon: FontAwesomeIcons.instagram,
                  errorText: null,
                  labelText: "Instagram",
                  node: instagramNode,
                  controller: _managePotentialMeetBloc.instaController,
                  textChanged: (value) {},
                  onSubmit: (value) {
                    moveNode(emailNode);
                  },
                ),
                CommonInputField(
                  icon: FontAwesomeIcons.envelope,
                  errorText: null,
                  labelText: "Email",
                  controller: _managePotentialMeetBloc.emailController,
                  node: emailNode,
                  textChanged: (value) {},
                  onSubmit: (value) {
                    moveNode(phoneNode);
                  },
                ),
                StreamBuilder<Country>(
                  stream: _managePotentialMeetBloc.countryCode,
                  builder: (context, snapshot) {
//                    print("code is"+snapshot.data.phoneCode);
                    return CommonInputField(
                      icon: FontAwesomeIcons.phoneAlt,
                      errorText: null,
                      labelText: "Phone",
                      onCountryTap: (){
                        showCountryPicker(context, _managePotentialMeetBloc.changeCountryCode);
                      },
                      countryCode: snapshot?.data?.phoneCode??"+1",
                      action: TextInputAction.done,
                      inputType: TextInputType.number,
                      controller: _managePotentialMeetBloc.phoneController,
                      node: phoneNode,
                      textChanged: (value) {},
                      onSubmit: (value) {},
                    );
                  }
                ),
                RaisedGradientButton(
                  gradient: AppTheme.gradient(),
                  text: "Update",
                  onPressed: () {
                    _managePotentialMeetBloc.add(UpdatePotentialMeet(widget.userData.meetId.toString()));
                  },
                  isDialog: true,
                )
              ],
            ),
            builder: (context, child) => Transform(
              transform: Matrix4.translationValues(
                  animation.value * SizeConfig.screenWidth, 0, 0),
              child: AnimatedOpacity(
                duration: Duration(milliseconds: 400),
                opacity: 1.0-animation.value*4,
                child: child,
              ),
            ),
          ),
        ),
      ),
    );
  }
  moveNode(FocusNode node) {
    FocusScope.of(context).requestFocus(node);
  }

}
