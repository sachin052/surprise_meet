import 'package:flutter/material.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoncarview.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

Widget deleteDialog(BuildContext context,VoidCallback onDelete,{String title,String subTitle}){
  return CommonCard(
    topContainerChild:
    Icon(
      Icons.delete,
      size: SizeConfig
          .blockSizeWidth *
          15,
      color: Colors
          .white,
    ),
    child: Column(
      children: <
          Widget>[
        SizedBox(
          height:
          SizeConfig.blockSizeWidth *
              4,
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(

            title!=null?title:"You are about to delete a Meet!",
            textAlign: TextAlign.center,
            style: Theme.of(
                context)
                .textTheme
                .headline
                .copyWith(
                fontSize:17.0),
          ),
        ),
        SizedBox(
          height:
          SizeConfig.blockSizeWidth *
              4,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal:32.0),
          child: Text(
            title==null?"Are you sure about blocking Match? \n Blocking it will remove the match and all its chat!":""+"Blocking it will remove the match and all its chat!",
            style: Theme.of(
                context)
                .textTheme
                .body2.copyWith(fontSize: 15.0),
            textAlign:
            TextAlign
                .center,
          ),
        ),
        SizedBox(
          height:
          SizeConfig.blockSizeWidth *
              4,
        ),
        RaisedGradientButton(
          gradient:
          AppTheme.gradient(),
          isDialog:
          true,
          text:
          "Delete",
          onPressed: onDelete

//                       MyRouter.navigatorKey.currentState.pop(state.data.message);
          ,
        ),
        FlatButton(
          child: Text(
            "Cancel",
            style: Theme.of(
                context)
                .textTheme
                .body2
                .copyWith(
                color: Colors.black.withOpacity(.5)),
          ),
          onPressed:
              () {
            MyRouter.navigatorKey.currentState.pop();
              },
        )
      ],
    ),
  );
}