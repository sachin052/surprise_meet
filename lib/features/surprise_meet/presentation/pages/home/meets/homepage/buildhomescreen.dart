import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/managepotentialmeet_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/managepotentialmeet_event.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/addpotentialmeet/addpotentialmeethomescreen.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/addpotentialmeet/addpotentialmeets.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/homepage/buildmeetcard.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

class BuildHomeScreen extends StatefulWidget {
  final List<PotentialMeetEntity> data;

  const BuildHomeScreen({Key key, this.data}) : super(key: key);

  @override
  _BuildHomeScreenState createState() => _BuildHomeScreenState();
}

class _BuildHomeScreenState extends State<BuildHomeScreen>
    with TickerProviderStateMixin {
  AnimationController controller;
  AnimationController editDeleteController;
  Animation editDeleteAnimation;
  Animation animation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
        duration: Duration(milliseconds: 1500), vsync: this);
    animation = Tween(begin: 0.1, end: 0.0).animate(CurvedAnimation(
        parent: controller, curve: Curves.fastLinearToSlowEaseIn));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return buildHomeScreen(context, widget.data);
  }

  Widget buildHomeScreen(
      BuildContext context, List<PotentialMeetEntity> listOfMeets) {
    var _managePotentialMeetBloc =
        BlocProvider.of<ManagePotentialMeetBloc>(context);
    return Stack(
      children: <Widget>[
        Positioned.fill(
            child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [AppColors.redTextColor, AppColors.colorPrimary],
                  end: Alignment.topRight,
                  begin: Alignment.bottomCenter)),
        )),
        Positioned(
          top: MediaQuery.of(context).viewPadding.top + 56.0,
          left: 0.0,
          right: 0.0,
          bottom: 0.0,
//          height: SizeConfig.screenHeight,
          child: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
//          margin:  EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            padding:
                const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
            child: RefreshIndicator(
                child: ListView(
                  children: <Widget>[
                    AnimatedBuilder(
                      animation: controller,
                      builder: (context, child) => Transform(
                        transform: Matrix4.translationValues(
                            0, animation.value * SizeConfig.screenHeight, 0),
                        child: child,
                      ),
                      child: Column(
                        children: [
                          AnimatedList(
                            initialItemCount: listOfMeets.length,
//                    key: animatedKey,
                            physics: ScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, i, animation) =>
                                BuildMeetList(
                              potentialMeetEntity: listOfMeets[i],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                onRefresh: () async =>
                    getPotentialMeet(_managePotentialMeetBloc)),
          ),
        ),
        Positioned(
//          height: 56.0,
          width: SizeConfig.screenWidth,
          child: AppBar(
            elevation: 0.0,
            automaticallyImplyLeading: false,
            title: Text(
              "Potential Meets",
              style: Theme.of(context).textTheme.body2.copyWith(
                  color: Colors.white, fontSize: SizeConfig.blockSizeWidth * 5),
            ),
            centerTitle: true,
            actions: <Widget>[
              StreamBuilder<bool>(
                stream: _managePotentialMeetBloc.editMode,
                builder: (context, snapshot) {
                  return GestureDetector(
                    onTap: () async {
                     _managePotentialMeetBloc.changeEditMode(!snapshot.data);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Container(
                          padding: const EdgeInsets.all(3.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.transparent,
                          ),
                          child: Icon(
                            Icons.edit,
                            color: Colors.white,
                          )),
                    ),
                  );
                }
              ),
              GestureDetector(
                onTap: () async {
                 var result=await Navigator.of(context).push(CupertinoPageRoute(
                      builder: (_) => BlocProvider.value(
                            value: _managePotentialMeetBloc,
                            child: AddPotentialMeets(),
                          )));

                  // var result = await MyRouter.navigatorKey.currentState
                  //     .pushNamed(MyRouter.addPotentialMeets);
                  if (result == null) {
                    _managePotentialMeetBloc.add(GetPotentialMeets());
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Container(
                      padding: const EdgeInsets.all(3.0),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: Icon(
                        Icons.add,
                        color: AppColors.colorPrimaryDark,
                      )),
                ),
              ),

            ],
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }

  getPotentialMeet(ManagePotentialMeetBloc bloc) {
    bloc.add(GetPotentialMeets());
  }
}
