import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/managepotentialmeet_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/addpotentialmeet/addpotentialmeets.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';

class EmptyPotentialMeetWidget extends StatefulWidget {
  @override
  _EmptyPotentialMeetWidgetState createState() => _EmptyPotentialMeetWidgetState();
}


class _EmptyPotentialMeetWidgetState extends State<EmptyPotentialMeetWidget> with TickerProviderStateMixin{
  AnimationController _controller;
  Animation<double> _animation;
  AnimationController _controller2;
  Animation _animation2;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );
    _controller2 =
        AnimationController(duration: Duration(seconds: 1), vsync: this);
    _animation2 = Tween(begin: 0.1, end: 0.0).animate(
        CurvedAnimation(parent: _controller2, curve: Curves.fastLinearToSlowEaseIn));

    _animation = Tween<double>(begin: 0.9, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: Curves.fastLinearToSlowEaseIn,
        reverseCurve: Curves.bounceIn));
    _controller2.forward();
    _controller.forward();
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _controller.forward();
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller2,
      builder: (c,child)=>Transform( transform: Matrix4.translationValues(
          0, _animation2.value * SizeConfig.screenHeight, 0),child: AnimatedOpacity(
        duration: Duration(milliseconds: 200),
          opacity: _controller2.value,
          child: child),),
      child: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.max,
//        crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SvgPicture.asset(Images.appIcon),
            SizedBox(
              height: SizeConfig.blockSizeWidth * 5,
            ),
            RichText(
                textAlign: TextAlign.center,
                text: TextSpan(children: [
                  TextSpan(
                    text: "It seems you are new to \n",
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .copyWith(fontSize: SizeConfig.blockSizeWidth * 7.5),
                  ),
                  TextSpan(
                    text: "Surprise Meet!",
                    style: Theme.of(context).textTheme.headline.copyWith(
                        fontSize: SizeConfig.blockSizeWidth * 7.5,
                        color: Color(0xFFE4184D)),
                  )
                ])),
            SizedBox(
              height: SizeConfig.blockSizeWidth * 15,
            ),
            AnimatedBuilder(
              animation: _controller,
              builder: (context,child)=>Transform.scale(
                scale: _animation.value,
                child: child,
              ),
              child: GestureDetector(
                onTap: () async{
                  // var result=await MyRouter.navigatorKey.currentState
                  //     .pushNamed(MyRouter.addPotentialMeets);
                  // if(result!=null){
                  //   BlocProvider.of<ManagePotentialMeetBloc>(context).add(GetPotentialMeets());
                  // }
                  ManagePotentialMeetBloc _managePotentialMeetBloc=BlocProvider.of<ManagePotentialMeetBloc>(context);
                  var result=await Navigator.of(context).push(CupertinoPageRoute(
                      builder: (_) => BlocProvider.value(
                        value: _managePotentialMeetBloc,
                        child: AddPotentialMeets(),
                      )));

                  // var result = await MyRouter.navigatorKey.currentState
                  //     .pushNamed(MyRouter.addPotentialMeets);
                  if (result == null) {
                    _managePotentialMeetBloc.add(GetPotentialMeets());
                  }
                },
                child: Container(
                    padding: const EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(.6),
                          blurRadius: 5.0,
                        ),
                      ],
                      color: Colors.grey.withOpacity(.4),
                    ),
                    child: Icon(
                      Icons.add,
                      size: SizeConfig.blockSizeWidth * 8,
                      color: AppColors.colorAccent,
                    )),
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeWidth * 5,
            ),
            Text(
              "Ready to start adding \n your potential meets",
              style: Theme.of(context).textTheme.body2,
            )
          ],
        ),
      ),
    );
  }
  @override
  void dispose() {
    _controller.dispose();
    _controller2.dispose();
    super.dispose();
  }
}
