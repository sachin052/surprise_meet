import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/utils/pop_with_result.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/managepotentialmeet_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/injection_container.dart';

import 'addpotentialmeethomescreen.dart';
import 'addpotentialmeetmodel.dart';

class AddPotentialMeets extends StatefulWidget {
  @override
  _AddPotentialMeetsState createState() => _AddPotentialMeetsState();
}

class _AddPotentialMeetsState extends State<AddPotentialMeets> {

  List<AddPotentialDataModel> list;
  Widget iconWidget;
  bool visibilityBottom = true;
  ManagePotentialMeetBloc _managePotentialMeetBloc;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  @override
  void didChangeDependencies() {
    list = createModelList(context);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    // TODO: implement initState

    _managePotentialMeetBloc = injector<ManagePotentialMeetBloc>();

    super.initState();
//    KeyboardVisibilityNotification().addNewListener(
//      onChange: (value) => _managePotentialMeetBloc.chaneVisibility(!value),
//    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        // return Future.value(true);
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          key: key,
          resizeToAvoidBottomPadding: true,
          body: BlocProvider<ManagePotentialMeetBloc>(
            create: (_) => _managePotentialMeetBloc,
            child:
                BlocListener<ManagePotentialMeetBloc, ManagePotentialMeetState>(
              listener: (context, state) {
                if (state is LoadintManageMeet) {
                  showDialog(
                      context: context,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ));
                } else if (state is ErrorPotentialMeetState) {
                  if(state.error.contains("email")){
                    showSnackBar(context, state.error, true);
                  }
                  else if(state.error.contains("phone")){
                    showSnackBar(context, state.error, true);
                  }
                  else if (state.error .contains("name")) {
                    _managePotentialMeetBloc.controller.animateToPage(0,
                        curve: Curves.fastOutSlowIn,
                        duration: Duration(milliseconds: 1000));
                    showSnackBar(context, state.error, true);

                  } else {
                    MyRouter.navigatorKey.currentState.pop();
                    showSnackBar(context, state.error, true);
                  }
//                MyRouter.navigatorKey.currentState.popUntil(ModalRoute.withName(MyRouter.home));
                } else if (state is MeetAdded) {
                  // MyRouter.navigatorKey.currentState.popUntil((route) => route==MyRouter.signUp)
                  // BlocProvider.of<ManagePotentialMeetBloc>(context).add(GetPotentialMeets());
                  MyRouter.navigatorKey.currentState.popUntil(ModalRoute.withName(MyRouter.home));

                  // MyRouter.navigatorKey.currentState.pop("added");
//                MyRouter.navigatorKey.currentState
//                    .popUntil(ModalRoute.withName(MyRouter.home,),);
                }
              },
              child: AddPotentialMeetHomeScreen(list: list, controller: _managePotentialMeetBloc.controller,),
            ),
          )),
    );
  }
}
