import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surprise_meet/style/images.dart';

class AddPotentialDataModel {
  final String largeIcon;
  final RichText richText;
  final IconData fieldIcon;
  final String fieldLabel;
  final String fieldError;

  AddPotentialDataModel(this.largeIcon, this.richText, this.fieldIcon,
      this.fieldLabel, this.fieldError);
}

List<AddPotentialDataModel> createModelList(BuildContext context) {
  return [
    AddPotentialDataModel(
        Images.userSvgIcon,
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: [
            TextSpan(
                text:
                    "Enter the name of your potential meet, or give them a nick name!",
                style: Theme.of(context).textTheme.body2),
          ]),
        ),
        FontAwesomeIcons.userAlt,
        "Name",
        null),
    AddPotentialDataModel(
        Images.phoneSvgIcon,
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: [
            TextSpan(
                text:
                    "Enter their phone number if you know it, If not just click next",
                style: Theme.of(context).textTheme.body2),
          ]),
        ),
        FontAwesomeIcons.phoneAlt,
        "Phone",
        null),
    AddPotentialDataModel(
      Images.heartEmailIcon,
      RichText(
        textAlign: TextAlign.center,
        text: TextSpan(style: Theme.of(context).textTheme.body2, children: [
          TextSpan(
              text: "Enter their email if you know it, If not click next.",
              style: Theme.of(context).textTheme.body2),
          TextSpan(
              text:
                  "",
              style: Theme.of(context).textTheme.body2),
        ]),
      ),
      FontAwesomeIcons.envelope,
      "Email Address",
      null,
    ),
//    AddPotentialDataModel(
//        Images.facebookSvglIcon,
//        RichText(
//          textAlign: TextAlign.center,
//          text: TextSpan(children: [
//            TextSpan(
//                text: "Yah! You are so close",
//                style: Theme.of(context).textTheme.body2),
//          ]),
//        ),
//        FontAwesomeIcons.facebookF,
//        "Paste Facebook Link",
//        null),
    AddPotentialDataModel(
        Images.instaSvgIcon,
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: [
            TextSpan(
                text:
                    "Enter their Instagram username if you know it, If not just press save. Make sure you entered their information on at least one of the last three screens.",
                style: Theme.of(context).textTheme.body2),
          ]),
        ),
        FontAwesomeIcons.instagram,
        "Instagram Username",
        null),
  ];
}
