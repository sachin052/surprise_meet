import 'dart:io';

import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:flutter_svg/svg.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/utils/debouncer.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/dialogs.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/utils/numberkeyboard.dart';

import 'addpotentialmeetmodel.dart';

class AddPotentialMeetHomeScreen extends StatefulWidget {
  final PageController controller;
  final List<AddPotentialDataModel> list;

   AddPotentialMeetHomeScreen({Key key, this.controller, this.list,})
      : super(key: key);

  @override
  _AddPotentialMeetHomeScreenState createState() => _AddPotentialMeetHomeScreenState();
}

class _AddPotentialMeetHomeScreenState extends State<AddPotentialMeetHomeScreen> {
    FocusNode phoneNode;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    phoneNode=FocusNode();
//    KeyboardVisibilityNotification().addNewListener(
//      onChange: (bool visible) {
//        if(phoneNode.hasFocus){
//          showOverLay(context);
//        }
//        else{
//          removeOverLay();
//        }
//      },
//    );
    phoneNode.addListener((){
      if(phoneNode.hasFocus&&Platform.isIOS){
        showOverLay(context);
      }
      else{
        removeOverLay();
      }
    });

//    phoneNode.addListener((){
//      print("has focus"+phoneNode.hasFocus.toString());
//
//    });
  }
  @override
  Widget build(BuildContext context) {
//    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    var _managePotentialMeetBloc =
        BlocProvider.of<ManagePotentialMeetBloc>(context);
//    FlutterStatusbarcolor.setStatusBarColor(Colors.white);
    return Scaffold(
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: SizeConfig.screenHeight<569?SizeConfig.screenHeight*.98:SizeConfig.screenHeight*0.95,
              child: Stack(
                fit: StackFit.expand,
                overflow: Overflow.clip,
                children: <Widget>[
                  Positioned(
                    left: -100,
                    top: 120,
                    child: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          height: SizeConfig.screenHeight,
                          width: SizeConfig.screenWidth,
                          child: StreamBuilder<int>(
                              stream: _managePotentialMeetBloc.currentPage,
                              builder: (context, snapshot) {
                                int currentPage = snapshot.data ?? 0;
                                return SvgPicture.asset(
                                  widget.list[currentPage].largeIcon,
                                  height: SizeConfig.safeBlockHorizontal * 80,
                                  color: Colors.black.withOpacity(.02),
                                );
                              }),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
         // top: -SizeConfig.blockSizeHeight*40,
                    top: Device.get().isTablet
                        ? -SizeConfig.blockSizeHeight * 50
                        : -SizeConfig.blockSizeHeight * 40,
                    left: Device.get().isTablet
                        ? SizeConfig.blockSizeHeight * 20
                        : SizeConfig.blockSizeHeight * 10,
                    child: GestureDetector(
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                      },
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              AppColors.colorPrimaryDark,
                              AppColors.colorPrimary
                            ], stops: [
                              0.4,
                              .8
                            ]),
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(3, 3),
                                  blurRadius: 10.0,
                                  color: Color(0xFFE40000).withOpacity(.6))
                            ]),
                        height: SizeConfig.screenHeight,
                        width: SizeConfig.screenWidth,
                        child: StreamBuilder<int>(
                            stream: _managePotentialMeetBloc.currentPage,
                            builder: (context, snapshot) {
                              int currentPage = snapshot.data ?? 0;
                              return AnimatedSwitcher(
                                child: SvgPicture.asset(
                                  widget.list[currentPage].largeIcon,
                                  height: SizeConfig.blockSizeWidth * 30,
                                  color: Colors.white,
                                  key: ValueKey(currentPage),
                                ),
                                duration: Duration(milliseconds: 400),
                              );
                            }),
                      ),
                    ),
                  ),
                  Positioned.fill(
                    top: SizeConfig.screenHeight / 2.5,
                    child: PageView.builder(
                      onPageChanged: _managePotentialMeetBloc.changePage,
                      pageSnapping: true,
                      controller: widget.controller,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (c, i) => StreamBuilder<int>(
                          stream: _managePotentialMeetBloc.currentPage,
                          builder: (context, snapshot) {
                            int currentPage = snapshot.data ?? 0;
                            return KeyboardActions(
                              config: KeyboardActionsConfig(keyboardActionsPlatform: KeyboardActionsPlatform.ALL,actions: [
                                KeyboardActionsItem(focusNode: FocusNode())
                              ]),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                                    child: widget.list[currentPage].richText,
                                  ),
                                  SizedBox(
                                    height: SizeConfig.blockSizeWidth * 7,
                                  ),
                                  Stack(
                                    children: <Widget>[
                                      StreamBuilder<Country>(
                                          stream: _managePotentialMeetBloc
                                              .countryCode,
                                          builder: (context, snapshot) {
                                            return CommonInputField(
                                              icon: widget.list[currentPage].fieldIcon,
                                              node: currentPage==1?phoneNode:FocusNode(),
                                              inputType: currentPage == 1
                                                  ? TextInputType.number
                                                  : TextInputType.text,
                                              labelText:
                                              widget.list[currentPage].fieldLabel,
//                                                  errorText:
//                                                  currentPage==2||currentPage==1?emailSnapshot.error:null,
                                              controller: _managePotentialMeetBloc
                                                  .controllers[currentPage],
                                              onSubmit: (string) {},
                                              onCountryTap: () {
                                                showCountryPicker(
                                                    context,
                                                    _managePotentialMeetBloc
                                                        .changeCountryCode);
                                              },
                                              countryCode: currentPage == 1
                                                  ? snapshot?.data?.phoneCode
                                                  : null,
                                              action: TextInputAction.done,
//                                    hasSuffix: currentPage==3?true:false,
                                              textChanged:(value){
                                                _managePotentialMeetBloc.textChanged(value);
                                              },
                                            );
                                          }),
                                    ],
                                  ),
                                  BlocBuilder<ManagePotentialMeetBloc,ManagePotentialMeetState>(
                                    bloc: _managePotentialMeetBloc,
                                    builder: (_,state)=>RaisedGradientButton(
                                      onPressed: () {
                                        _managePotentialMeetBloc
                                            .add(AddPotentialMeet());
                                      },
                                      text: currentPage == widget.list.length - 1
                                          ? "Save"
                                          : "Next",
                                      gradient: AppTheme.gradient(),
                                      isDialog: true,
                                    ),
                                  )
                                ],
                              ),
                            );
                          }),
                      itemCount: widget.list.length,
                    ),
                  ),
                  StreamBuilder<bool>(
                      stream: _managePotentialMeetBloc.visibility,
                      builder: (context, snapshot) {
                        return buildPageViewSlider(
                            snapshot, _managePotentialMeetBloc);
                      }),
                  StreamBuilder<int>(
                      stream: _managePotentialMeetBloc.currentPage,
                      builder: (context, snapshot) {
                        if(snapshot.data!=null&&snapshot.data!=1)removeOverLay();
                        return Wrap(
                          children: <Widget>[
                            AppBar(
                              elevation: 0.0,
                              leading: GestureDetector(
                                  onTap: () {
                                    removeOverLay();
                                    if (snapshot.data - 1 > -1) {
                                      widget.controller.animateToPage(
                                          snapshot.data - 1,
                                          curve: Curves.fastOutSlowIn,
                                          duration:
                                          Duration(milliseconds: 1000)).whenComplete((){
                                        _managePotentialMeetBloc
                                            .changePage(snapshot.data - 1);
                                      });
                                    } else {
                                      MyRouter.navigatorKey.currentState.pop("notload");
                                    }
                                  },
                                  child: Icon(Icons.arrow_back)),
                            ),
                          ],
                        );
                      })
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildPageViewSlider(AsyncSnapshot<bool> snapshot,
      ManagePotentialMeetBloc _managePotentialMeetBloc) {
    return Positioned.fill(
      child: Container(
        padding: const EdgeInsets.all(8.0),
        margin: EdgeInsets.only(bottom: 8.0),
        alignment: Alignment.bottomCenter,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Visibility(
              visible: snapshot.data??true,
              child: FlatButton(
                onPressed: () async {
                  var value = await _managePotentialMeetBloc.currentPage.first;
                  widget.controller.animateToPage(value + 1,
                      curve: Curves.fastOutSlowIn,
                      duration: Duration(milliseconds: 500));
                },
                child: Text("Skip"),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
              ),
            ),
            SmoothPageIndicator(
              controller: widget.controller, // PageController
              count: widget.list.length,
              effect: WormEffect(
                  dotColor: AppColors.colorPrimary.withOpacity(.5),
                  activeDotColor:
                      AppColors.colorPrimary), // your preferred effect
            )
          ],
        ),
      ),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    removeOverLay();
//    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    super.dispose();
  }
}
