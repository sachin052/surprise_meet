import 'package:division/division.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/utils/animteddialog.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/selectchatentity.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/chat/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/home_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/homepage/deletedialog.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/showErrorView.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/main.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import '../../../../../../style/colors.dart';
import '../../../bloc/chat/chat_event.dart';
import '../../../widgets/commonsnackbar.dart';

class SelectedChatScreen extends StatefulWidget {
  final String connectionId;
  final String otherPersonName;
  final String receiverId;
  const SelectedChatScreen({
    Key key,
    this.receiverId,
    this.connectionId, this.otherPersonName})
      : super(key: key);

  @override
  _SelectedChatScreenState createState() => _SelectedChatScreenState();
}

class _SelectedChatScreenState extends State<SelectedChatScreen>
    with WidgetsBindingObserver {
  ChatBloc _chatBloc;
  AppLifecycleState appLifecycleState;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _chatBloc = injector<ChatBloc>();
    _chatBloc.animatedListKey=GlobalKey<AnimatedListState>();
    _chatBloc.connectionId=widget.connectionId;
    _chatBloc.add(GetSelectedChat(widget.connectionId));
    print("bloc has"+_chatBloc.hashCode.toString());
    connectionIdMain.add(widget.connectionId);
    try{
      BlocProvider.of<HomeBloc>(context).changeNotificationCount(0);
    }catch(e){
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _chatBloc.closeMessageSink();
    _chatBloc.close();
    _chatBloc?.animatedListKey?.currentState?.dispose();
    WidgetsBinding.instance.removeObserver(this);
    print("disposed");
    super.dispose();

  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // used for notification handling when app is in background state
    super.didChangeAppLifecycleState(state);

    print(state.toString());
    if (state == AppLifecycleState.resumed&&appLifecycleState!=AppLifecycleState.resumed) {
      _chatBloc.add(GetSelectedChat(widget.connectionId));
    }
    else if (state == AppLifecycleState.paused) {
      _chatBloc.closeMessageSink();
    }
    appLifecycleState=state;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (c)=>_chatBloc,
      child: WillPopScope(
        onWillPop: ()async{
//         super.dispose();
          notification.add("reset");
          BlocProvider.of<HomeBloc>(context).changeNotificationCount(0);
          MyRouter.navigatorKey.currentState.pop(_chatBloc.sentAtLeastOneMessage);
           return false;
        },
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            brightness: Brightness.light,
            title: Text(
              widget.otherPersonName,
              style: Theme.of(context)
                  .textTheme
                  .headline
                  .copyWith(
                  fontSize: SizeConfig.blockSizeWidth * 5
              ),
            ),
            centerTitle: true,
            actions: [
              Theme(
                data: Theme.of(context),
                child: PopupMenuButton<String>(
                  onSelected: (data){
                      if( data=='Report'){
                        _chatBloc.add(ReportUser(widget.receiverId));
                      } else{
                        showAnimatedDialog(context, deleteDialog(context, (){
                          MyRouter.navigatorKey.currentState.pop();
                          _chatBloc.add(DeleteRecentChat(widget.connectionId,type: 1));
                        },title: "Are you sure about blocking ${widget.otherPersonName}?"));
                      }
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  itemBuilder: (BuildContext context) {
                    return {'Report','Block'}.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                                choice,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline
                                  .copyWith(fontSize: SizeConfig.blockSizeWidth * 5),
                            ),
                            Icon(
                              choice=='Report'?Icons.flag_rounded:Icons.block,
                            ),
                          ],
                        ),
                      );
                    }).toList();
                  },
                ),
              ),
            ],
          ),
          body: BlocListener<ChatBloc, ChatState>(
            listener: (context,state){
              if(state is  SuccessSelectedChatState){
//                _chatBloc.connectChatRoom();
              }else if(state is UserReportedState){
                if(state.requestType =='delete'){
                  MyRouter.navigatorKey.currentState.pop(true);
                }else{
                  showSnackBar(context, state.message, false);
                }

              }else if(state is ErrorReportUserState){
                showSnackBar(context, state.error, true);
              }
            },
            child: BlocProvider(
              create: (c) => _chatBloc,
              child: BlocBuilder<ChatBloc, ChatState>(
                builder: (context, state) {
                  return state.when(
                      errorReportUserState: (c)=>buildHomeView(context) ,
                      userReportedState: (c)=>buildHomeView(context),
                      loadingChatState: (c) => Center(
                            child: CircularProgressIndicator(),
                          ),
                      initlaChatState: (c) => Center(
                            child: CircularProgressIndicator(),
                          ),
                      errorChatState: (error) => showErrorView(error.error, () {
                        if(error.error.contains("internet")||error.error.contains("Access denied"))
                            _chatBloc.add(GetSelectedChat(widget.connectionId));
                        else MyRouter.navigatorKey.currentState.pop();
                          }),
                      noDataChatState: (c) => Center(
                            child: Text("No Data Found"),
                          ),
                      successChatState: (c) => Center(
                            child: CircularProgressIndicator(),
                          ),
                      successSelectedChatState: (chatItems) =>
                          buildHomeView(context));
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  buildHomeView(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(bottom: 65.0),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: StreamBuilder<List<SelectedChatEntity>>(
                stream: _chatBloc.allChats,
                builder: (context, snapshot) {
                  List<SelectedChatEntity> chatItems = snapshot?.data;

                  return snapshot.data == null
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : AnimatedList(
                          key: _chatBloc.animatedListKey,
                          controller: _chatBloc.controller,
                          initialItemCount: chatItems.length,
                          reverse: true,
                          itemBuilder: (context, index, animation) {
                            var currentUserIsSender = _chatBloc.currentUserId ==
                                chatItems[index].senderId;
                            return SizeTransition(
                                sizeFactor: animation,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    width: SizeConfig.screenWidth,
                                    alignment: currentUserIsSender
                                        ? Alignment.centerRight
                                        : Alignment.centerLeft,
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      children: <Widget>[
                                        buildChatCard(currentUserIsSender,
                                            chatItems, index, context),
                                      ],
                                    ),
                                  ),
                                ));
                          });
                }),
          ),
        ),
        Align(
          alignment: Alignment(0, .98),
          child: Container(
//            padding: const EdgeInsets.all(7.0),
            margin: const EdgeInsets.all(7.0),
            decoration: BoxDecoration(
                border:
                    Border.all(color: Colors.grey.withOpacity(.5), width: 1.0),
                borderRadius: BorderRadius.circular(20.0)),
            child:  ConstrainedBox(
              constraints:  BoxConstraints(
                maxHeight: 55.0,
              ),
              child: Stack(
                children: [
                  TextField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    controller: _chatBloc.chatTextController,
                    readOnly: false,
                    style: Theme.of(context).textTheme.body2.copyWith(letterSpacing: 0.15),
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "What are your plans this week?",
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 15.0, horizontal: 20.0),
                      suffix: Container(height: 40,width: 20,)
                    ),

                  ),
                  Align(
                    alignment: Alignment(.9,0),
                    child: StreamBuilder<List<SelectedChatEntity>>(
                        stream: _chatBloc.allChats,
                        builder: (context, snapshot) {
                          return SendButton();
                        }),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildChatCard(bool currentUserIsSender,
      List<SelectedChatEntity> chatItems, int index, BuildContext context) {
    return Column(
      crossAxisAlignment: currentUserIsSender
          ? CrossAxisAlignment.end
          : CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Material(
          elevation: 3.0,
          shape: RoundedRectangleBorder( borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18.0),
            topRight: Radius.circular(18.0),
            bottomLeft: currentUserIsSender
                ? Radius.circular(18.0)
                : Radius.circular(0.0),
            bottomRight: currentUserIsSender
                ? Radius.circular(0.0)
                : Radius.circular(18.0),
          )),
          child: Container(
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                gradient: currentUserIsSender ? AppTheme.gradient() : null,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(18.0),
                  topRight: Radius.circular(18.0),
                  bottomLeft: currentUserIsSender
                      ? Radius.circular(18.0)
                      : Radius.circular(0.0),
                  bottomRight: currentUserIsSender
                      ? Radius.circular(0.0)
                      : Radius.circular(18.0),
                ),
                color: currentUserIsSender ? null : Color(0xFFFEEFF2)),
            child: Wrap(
              alignment: WrapAlignment.end,
              children: <Widget>[
                Text(
                  chatItems[index].message,
                  style: Theme.of(context).textTheme.body2.copyWith(
                      color: currentUserIsSender ? Colors.white : Colors.black),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: SizeConfig.blockSizeWidth * 2,
        ),
        Text(
            DateFormat('h:mm a').format(chatItems[index].messageTime.toLocal()))
      ],
    );
  }
}

class SendButton extends StatefulWidget {
  @override
  _SendButtonState createState() => _SendButtonState();
}

class _SendButtonState extends State<SendButton> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    var _chatBloc = BlocProvider.of<ChatBloc>(context);
    return Parent(
      gesture: Gestures()
        ..isTap((value) {
          setState(() {
            pressed = !pressed;
          });
        })
        ..onTap(() {
          if (_chatBloc.chatTextController.text.trim().isNotEmpty) {
            _chatBloc.sendMessage(_chatBloc.chatTextController.text);
          }
        }),
      style: sendButtonStyle(pressed: pressed),
      child: Icon(
        FontAwesomeIcons.paperPlane,
        color: Colors.grey,
      ),
    );
  }

  sendButtonStyle({bool pressed}) => ParentStyle()
    ..height(50.0)
    ..elevation(0.0)
    ..borderRadius(all: 50.0)
    ..scale(pressed ? 0.85 : 1.0)
    ..animate(200, Curves.fastOutSlowIn)
    ..circle(true)
    ..ripple(true);
}
