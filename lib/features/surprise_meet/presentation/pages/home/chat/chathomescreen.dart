import 'package:animations/animations.dart';
import 'package:division/division.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/utils/animteddialog.dart';
import 'package:surprise_meet/extenions.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/chatentity.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/chat/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/home_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/home_event.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/chat/selectedchatscreen.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/homepage/deletedialog.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildBottomView.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/nodatafound.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/showErrorView.dart';
import 'package:surprise_meet/gradientbuttonstyle.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/main.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

class ChatPageWidget extends StatefulWidget {
  @override
  _ChatPageWidgetState createState() => _ChatPageWidgetState();
}

class _ChatPageWidgetState extends State<ChatPageWidget>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;
  ChatBloc _chatBloc;
  ScrollController _scrollController;
  bool toolbarVisibility = true;
  ContainerTransitionType _transitionType = ContainerTransitionType.fade;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _scrollController = ScrollController();
    _scrollController.addListener(() {

      print("offset is" + _scrollController.offset.toString());
      if (_scrollController.offset == 200.0) {
        _scrollController.animateTo(155.0, duration: Duration(milliseconds: 200), curve: Curves.fastOutSlowIn);
      }
    });
    _chatBloc = injector<ChatBloc>();
    _chatBloc.add(GetChat());
    notification.listen((value) {
      BlocProvider.of<HomeBloc>(context).changeNotificationCount(0);
      _chatBloc.add(GetChat());
//      print('in chat'+value.toString());
//      _chatBloc.add(UpdateUserChat(value["connection_id"].toString(),value["body"].toString()));
    });
    controller = AnimationController(
        duration: Duration(milliseconds: 1500), vsync: this);
    animation = Tween(begin: -0.2, end: 0.0).animate(CurvedAnimation(
        parent: controller, curve: Curves.fastLinearToSlowEaseIn));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _chatBloc,
      child: BlocBuilder<ChatBloc, ChatState>(
        builder: (context, state) {
          return state.when(
              loadingChatState: (c) => Center(
                    child: CircularProgressIndicator(),
                  ),
              initlaChatState: (c) => Center(
                    child: CircularProgressIndicator(),
                  ),
              errorChatState: (error) => showErrorView(error.error, () {
                    _chatBloc.add(GetChat());
                  }),
              noDataChatState: (c) => NoDataFoundWidget(
                    errorMessage: "No Matches Found",
                    onBackClick: () {
                      BlocProvider.of<HomeBloc>(context).changePage(1);
                      BlocProvider.of<HomeBloc>(context)
                          .add(PageTapped(CurrentPageScreen.potentialMeets()));
                    },
                  ),
              successChatState: (data) => buildHomeWidget(
                    context,
                    items: data.items,
                  ),
              userReportedState: (s)=>buildHomeWidget(context),
              errorReportUserState: (s)=>buildHomeWidget(context),
              successSelectedChatState: (success) {});
        },
      ),
    );
  }

  Widget buildHomeWidget(BuildContext context, {List<ChatEntity> items}) {
    return Container(
      decoration: BoxDecoration(gradient: AppTheme.gradient()),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: AppColors.scaffoldBGColor,
          body: NestedScrollView(
            physics: BouncingScrollPhysics(),
            headerSliverBuilder: (c,i)=>[SliverAppBar(
              automaticallyImplyLeading: false,
              title: PreferredSize(
                child: new Container(
                  alignment: Alignment.center,
                  width: SizeConfig.screenWidth,
//                  padding: new EdgeInsets.only(
//                      top: MediaQuery.of(context).padding.top),
                  child: new Padding(
                    padding: const EdgeInsets.only(
                        top: 20.0, bottom: 20.0),
                    child: new Text(
                      'Surprise Matches',
                      style: Theme.of(context).textTheme.headline.copyWith(fontSize: 21.0,color: Colors.white),
                    ),
                  ),
                  decoration: new BoxDecoration(
                    gradient: AppTheme.gradient(),
//                    boxShadow: [
//                      new BoxShadow(
//                        color: Colors.grey[500],
//                        blurRadius: 20.0,
//                        spreadRadius: 1.0,
//                      )
//                    ]
                  ),
                ),
                preferredSize: new Size(MediaQuery.of(context).size.width, 150.0),
              ),
              titleSpacing: 0.0,
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              shape: toolbarVisibility
                  ? RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50.0),
                      bottomRight: Radius.circular(50.0)))
                  : RoundedRectangleBorder(),
//            backgroundColor: AppColors.colorPrimary,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
//                title: Text("Surprise Match",
//                    style: Theme.of(context).textTheme.body2.copyWith(color: Colors.white),),
                  background: Container(
                    alignment: Alignment(0, .5),
                    padding: const EdgeInsets.all(32.0),
                    decoration: BoxDecoration(
                        gradient: AppTheme.gradient(),
                        borderRadius: BorderRadius.only(
                            bottomRight: const Radius.circular(30.0),
                            bottomLeft: const Radius.circular(30.0))),
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text:
                          "Kindly don't disclose your identity until you meet the person. Let it be a ",
                          style: Theme.of(context)
                              .textTheme
                              .subhead
                              .copyWith(color: Colors.white, fontSize: 20.0),
                        ),
                        TextSpan(
                            text: "Surprise Meet.",
                            style: Theme.of(context)
                                .textTheme
                                .headline
                                .copyWith(color: Colors.white, fontSize: 20.0))
                      ]),
                    ),
                  )),
            ),],
            body:  RefreshIndicator(
              onRefresh: ()async{
                _chatBloc.add(GetChat());
              },
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 56.0),
                  child: Column(
                    children: List<Widget>.generate(items.length ?? items?.length, (i) {
                    return ChatItem(chatEntity: items[i],);
                  }
                  ),),
                ),
              ),
            ),
            controller: items.length<3?_scrollController:null,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    _chatBloc.dispose();
    super.dispose();
  }
}

class ChatItem extends StatefulWidget {
  final ChatEntity chatEntity;

  const ChatItem({Key key, this.chatEntity}) : super(key: key);

  @override
  _ChatItemState createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    return Parent(
      gesture: Gestures()
        ..isTap((value) {
          setState(() {
            pressed = value;
          });
        })
        ..onTap(() async{
          HomeBloc _homeBloc=BlocProvider.of<HomeBloc>(context);

          _homeBloc.changeNotificationCount(0);
          var currentChat = widget.chatEntity;
          var messageSentOrReceived=await MyRouter.navigatorKey.currentState.push(
              CupertinoPageRoute(builder: (_)=>BlocProvider.value(
                value: _homeBloc,child: SelectedChatScreen(
                receiverId: currentChat.anonymousUserId,
               connectionId: currentChat.connectionId,
                otherPersonName: currentChat.name,
              ))));
          if(messageSentOrReceived){
            BlocProvider.of<ChatBloc>(context).add(GetChat());
          }
        }),
      style: gradientCardStyle(
        pressed,
      ),
      child: buildChatCard(context, widget.chatEntity),
    );
  }

  Widget buildChatCard(BuildContext context, ChatEntity data) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            child: Text(
              data.messageTime.timesAgo,
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white.withOpacity(.7),fontSize: 12.0),
            ),
            alignment: Alignment.centerRight,
          ),
          Text(data.name,
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .copyWith(color: Colors.white,fontSize: 18.0)),
        Visibility(
            visible: data.message.isNotEmpty,
            child: SizedBox(height: SizeConfig.blockSizeHeight*.30,)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Visibility(
                  visible: data.message.isNotEmpty,
                  child: Text(data.message.trim(),
                      maxLines: 1,
                      style: Theme.of(context)
                          .textTheme
                          .body2
                          .copyWith(color: Colors.white,fontSize: 14.0)),
                ),
              ),
              GestureDetector(
                onTap: (){
                  var chatBloc = BlocProvider.of<ChatBloc>(context);
                  showAnimatedDialog(context, deleteDialog(context, (){
                    MyRouter.navigatorKey.currentState.pop();
                    chatBloc.add(DeleteRecentChat(data.connectionId));
                  },title: "Are you sure about blocking ${data.name}?"));
                },
                  child: Icon(Icons.block,color: Colors.white,))
            ],
          ),
          SizedBox(height: SizeConfig.blockSizeHeight*2.0,),
        ],
      ),
    );
  }
}


