

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/profile_bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildBottomView.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonpasswordfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

class UpdatePasswordScreen extends StatefulWidget {
  @override
  _UpdatePasswordScreenState createState() => _UpdatePasswordScreenState();
}

class _UpdatePasswordScreenState extends State<UpdatePasswordScreen> {
  UpdateProfileBloc bloc;
  FocusNode oldPasswordNode=FocusNode();
  FocusNode newPasswordNode=FocusNode();
  FocusNode cPasswordNode=FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = injector<UpdateProfileBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Scaffold(
        backgroundColor: Color(0XFFEFF3F6),
        appBar: AppBar(
          title: Text(
            "Update Password",
            style: Theme.of(context).textTheme.body2.copyWith(
                color: AppColors.colorPrimaryDark,
                fontSize: SizeConfig.blockSizeWidth * 5),
          ),
          centerTitle: true,
          elevation: 0.0,
        ),
        body: BlocListener<UpdateProfileBloc, UpdateProfileState>(
            listener: (context, state) {
              state.whenPartial(
                errorProfileState: (error) {
                  showSnackBar(context, error.error, true);
                },
                successProfileState: (d) {
                  MyRouter.navigatorKey.currentState.pop();
                  showSnackBar(context, "Password udpated successfully", false);
                },
              );
            },
            child: BlocBuilder<UpdateProfileBloc, UpdateProfileState>(
              builder: (context, state) =>
              state == UpdateProfileState.loadingProfileState()
                  ? Center(
                child: CircularProgressIndicator(),
              )
                  : buildUpdatePassHome(),
            )),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    bloc.dispose();
    super.dispose();
  }

  Widget buildUpdatePassHome() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          getBottomSizedBox(),
          StreamBuilder<String>(
              stream: bloc.oldPassword,
              builder: (context, snapshot) {
                return CommonPasswordField(
                  node:oldPasswordNode,
                  controller: bloc.oldPassWordEditingController,
                  onSubmit: (value) {
                    FocusScope.of(context).requestFocus(newPasswordNode);
                  },
                  labelText: "Old Password",
                  changepass: bloc.changeOldPassword,
                  errorText: snapshot.error,
                );
              }),
          SizedBox(
            height: 10.0,
          ),
          StreamBuilder<String>(
              stream: bloc.newPassword,
              builder: (context, snapshot) {
                return CommonPasswordField(
                  node: newPasswordNode,
                  controller: bloc.newPassWordEditingController,
                  onSubmit: (value) {
                    FocusScope.of(context).requestFocus(cPasswordNode);
                  },
                  labelText: "New Password",
                  changepass: bloc.changeNewPassword,
                  errorText: snapshot.error,
                );
              }),
          SizedBox(
            height: 10.0,
          ),
          StreamBuilder<String>(
              stream: bloc.confirmPassword,
              builder: (context, snapshot) {
                return CommonPasswordField(
                  node: cPasswordNode,
                  action: TextInputAction.done,
                  controller: bloc.cPassWordEditingController,
                  onSubmit: (value) {},
                  labelText: "Confirm Password",
                  changepass: bloc.changeConfirmPassword,
                  errorText: snapshot.error,
                );
              }),
          SizedBox(
            height: 10.0,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: StreamBuilder<bool>(
                stream: bloc.isValid,
                builder: (context, snapshot) {
                  return RaisedGradientButton(
                    isDialog: true,
                    text: "Update Password",
                    onPressed: () {
                      if(snapshot.data!=null&&snapshot.data)
                        bloc.add(UpdateProfileEvent.updatePassword());
                      else bloc.addEmptyError();
                    },
                  );
                }
            ),
          )
        ],
      ),
    );
  }
}
