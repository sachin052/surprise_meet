import 'package:country_pickers/country.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/signup/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildTopView.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/signup/terms_of_use.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonpasswordfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commonsnackbar.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/dialogs.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/style/strings.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> with SingleTickerProviderStateMixin {
  FocusNode nameNode;
  FocusNode emailNode;
  FocusNode phoneNode;
  FocusNode passNode;
  FocusNode confirmPassNode;
  bool checkBoxValue = false;
  SignUpBloc bloc;
  AnimationController _controller;
  Animation<double> _animation;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    nameNode = FocusNode();
    emailNode = FocusNode();
    phoneNode = FocusNode();
    passNode = FocusNode();
    confirmPassNode = FocusNode();
    bloc = injector<SignUpBloc>();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );
    _animation = Tween<double>(begin: 1.0, end: 1.2).animate(CurvedAnimation(
        parent: _controller,
        curve: Curves.fastLinearToSlowEaseIn,
        reverseCurve: Curves.bounceIn));
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      }
    });
//    phoneNode.addListener((){
//      if(phoneNode.hasFocus){
//        showOverLay(context);
//      }
//      else{
//        removeOverLay();
//      }
//    });
//    bloc.controller = AnimationController(
//        duration: const Duration(milliseconds: 2000), vsync: this, value: 0.1);
//    _animation = CurvedAnimation(parent: bloc.controller, curve: Curves.bounceInOut);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
//      appBar: AppBar(
//        elevation: 0.0,
//        title: Text("Sign Up"),
//      ),
      body: BlocProvider<SignUpBloc>(
          create: (c) => bloc,
          child: BlocListener<SignUpBloc, SignUpState>(
            listener: (context, state) {
              if (state is SignUpErrorState) {
                showSnackBar(context, state.error, true);
//                Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.error),));
              } else if (state is SignUpSuccessState) {
                showSuccessDialog(context, state: state);
              }
              else if(state is AgreeTerms){
                _controller.forward();
              }
            },
            child: BlocBuilder<SignUpBloc, SignUpState>(
              builder: (context, state) {
                return state.when(
                    signUpInitial: (c) => buildHome(context),
                    signUpLoading: (c) => Center(
                          child: CircularProgressIndicator(),
                        ),
                    signUpErrorState: (c) {
                      return buildHome(context);
                    },
                    signUpSuccessState: (c) => buildHome(context),
                    agreeTerms: (agreeTerms) {
                      return buildHome(context);
                    });
              },
            ),
          )),
    );
  }

  moveNode(FocusNode node) {
    FocusScope.of(context).requestFocus(node);
  }

  Widget buildHome(BuildContext context) {
    return SafeArea(
      child: ListView(
        children: <Widget>[
          BuildTopView(
              title: Strings.signUpTitle, subtitle: Strings.signUpToContinue),
          SizedBox(
            height: SizeConfig.blockSizeHeight * 5,
          ),
          StreamBuilder<String>(
            stream: bloc.name,
            builder: (context, snapshot) => CommonInputField(
              controller: bloc.nameTextController,
              node: nameNode,
              onSubmit: (value) {
//              FocusScope.of(context).requestFocus(emailNode);
                moveNode(emailNode);
              },
              errorText: snapshot.error,
              labelText: "Name",
              textChanged: bloc.changeName,
              icon: FontAwesomeIcons.user,
            ),
          ),
          StreamBuilder<String>(
            stream: bloc.email,
            builder: (context, snapshot) => CommonInputField(
              controller: bloc.emailTextController,
              node: emailNode,
              onSubmit: (value) {
                moveNode(phoneNode);
              },
              errorText: snapshot.error,
              labelText: "Email",
              textChanged: bloc.changeEmail,
              icon: FontAwesomeIcons.envelope,
            ),
          ),
          StreamBuilder<Country>(
            stream: bloc.countryCode,
            builder: (context, country) => StreamBuilder<String>(
              stream: bloc.phone,
              builder: (context, snapshot) => CommonInputField(
                controller: bloc.phoneTextController,
                inputType: TextInputType.number,
                node: phoneNode,
                countryCode: country?.data?.phoneCode?.toString(),
                onSubmit: (value) {
                  moveNode(passNode);
                },
                errorText: snapshot.error,
                onCountryTap: () {
                  showCountryPicker(context, bloc.changeCountryCode);
                },
                labelText: "Phone",
                textChanged: bloc.changePhone,
                icon: FontAwesomeIcons.phoneAlt,
              ),
            ),
          ),
          StreamBuilder<String>(
            stream: bloc.password,
            builder: (context, snapshot) => CommonPasswordField(
              controller: bloc.passwordTextController,
              node: passNode,
              onSubmit: (value) {
                moveNode(confirmPassNode);
              },
              errorText: snapshot.error,
              labelText: "Password",
              changepass: bloc.changePassword,
            ),
          ),
          StreamBuilder<String>(
            stream: bloc.confirmPassword,
            builder: (context, snapshot) => StreamBuilder<bool>(
              stream: bloc.validCredentials,
              builder: (context, valid) {
                return CommonPasswordField(
                  controller: bloc.confirmPassTextController,
                  node: confirmPassNode,
                  onSubmit: (value) {
                    if(valid.data!=null&&valid.data)
                      bloc.add(SignUpUserEvent());
                    else bloc.addEmptyError();

                  },
                  errorText: snapshot.error,
                  action: TextInputAction.done,
                  labelText: "Confirm Password",
                  changepass: bloc.changeConfirmPassword,
                );
              }
            ),
          ),
          StreamBuilder<bool>(
            stream: bloc.validCredentials,
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: RaisedGradientButton(
                  text: "SIGN UP",
                  isDialog: false,
                  onPressed: () {
//              showSuccessDialog(context);
                    if(snapshot.data!=null&&snapshot.data)
                    bloc.add(SignUpUserEvent());
                    else bloc.addEmptyError();
                  },
                  gradient: AppTheme.gradient(),
                ),
              );
            }
          ),
          AnimatedBuilder(
            animation: _controller,
            builder: (context, child) => Transform.scale(
              scale: _animation.value,
              child: child,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                StreamBuilder<bool>(
                  stream: bloc.isAgreeTerms,
                  builder: (context, snapshot) {
                    bool value = snapshot.data == null ? false : snapshot.data;
                    return Checkbox(value: value, onChanged: bloc.changeTerms);
                  },
                ),
                GestureDetector(
                    onTap: (){
                      MyRouter.navigatorKey.currentState.push(
                          CupertinoPageRoute(builder: (_)=>TermsOfUse()));
                    },
                    child: Text(
                        "I Accept Terms and Conditions",
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                        ),
                    )
                )
              ],
            ),
          ),
          SizedBox(
            height: SizeConfig.blockSizeHeight * 4,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text("Already have an account? "),
              GestureDetector(
                  onTap: () {
                    MyRouter.navigatorKey.currentState.pop();
                  },
                  child: Container(
                    padding: const EdgeInsets.only(top: 5.0),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: AppColors.colorAccent, width: 1.0))),
                    child: Text("Sign In",
                        style: Theme.of(context)
                            .textTheme
                            .headline
                            .copyWith(fontSize: 15.0)),
                  ))
            ],
          ),
          SizedBox(
            height: SizeConfig.blockSizeHeight * 4,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    bloc.dispose();
    bloc.close();
    super.dispose();
  }
}
