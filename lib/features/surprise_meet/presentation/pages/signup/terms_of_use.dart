import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class TermsOfUse extends StatefulWidget {
  @override
  _TermsOfUseState createState() => _TermsOfUseState();
}

class _TermsOfUseState extends State<TermsOfUse> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      hidden: true,
      initialChild: Center(
        child: CircularProgressIndicator(),
      ),
      url: "http://165.227.208.94:8000/surprisemeet/terms_of_use/",
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
    );
  }
}
