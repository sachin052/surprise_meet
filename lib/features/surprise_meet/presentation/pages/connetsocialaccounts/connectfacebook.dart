import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildBottomView.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoninputfield.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/showErrorView.dart';
import 'package:surprise_meet/injection_container.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

import '../../../../../main.dart';

class ConnectSocialPlatforms extends StatefulWidget {
  final bool isFacebook;
  final bool isInsta;

  // true = AddRequest
  // false = UpdateRequest
  final bool addRequest;
  final bool fromCompleteDialog;

  const ConnectSocialPlatforms(
      {Key key,
      this.isFacebook = false,
      this.isInsta = false,
      this.fromCompleteDialog = false,
      this.addRequest = false
//      this.socialRequestType
      })
      : super(key: key);

  @override
  _ConnectSocialPlatformsState createState() => _ConnectSocialPlatformsState();
}

class _ConnectSocialPlatformsState extends State<ConnectSocialPlatforms>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;
  UpdateProfileBloc _profileBloc;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: Duration(seconds: 1), vsync: this);
    animation = Tween(begin: 0.1, end: 0.0).animate(
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn));
    controller.forward();
    _profileBloc = injector<UpdateProfileBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (!widget.fromCompleteDialog) {
          return true;
        } else {
          MyRouter.navigatorKey.currentState.popUntil((route) {
            notification.add(0);
            notification.add("reset");
            if (route.settings.name != MyRouter.home) {
              MyRouter.navigatorKey.currentState
                  .pushReplacementNamed(MyRouter.home);
            }
            return true;
          });
          return false;
        }
      },
      child: Scaffold(
        body: BlocProvider(
          create: (c) => _profileBloc,
          child: BlocListener<UpdateProfileBloc, UpdateProfileState>(
            listener: (context, state) {
              if (state is GetFbUserName) {
                showModalBottomSheet<void>(
                  isScrollControlled: true,
                  context: context,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0)),
                  ),
                  builder: (BuildContext context) {
                    return Padding(
                        padding: MediaQuery.of(context).viewInsets,
                        child: Wrap(
                          children: <Widget>[
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: const Icon(
                                FontAwesomeIcons.facebook,
                                color: Colors.white,
                                size: 45.0,
                              ),
                              width: SizeConfig.screenWidth,
                              padding: const EdgeInsets.all(16.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(30.0),
                                      topRight: Radius.circular(30.0)),
                                  gradient: AppTheme.gradient()),
                            ),
                            getBottomSizedBox(),
                            Container(
                              width: SizeConfig.screenWidth,
//                                          color: Colors.red,
                              alignment: Alignment.center,
                              child: Text(
                                "Add facebook username to get started",
                                style: Theme.of(context)
                                    .textTheme
                                    .title
                                    .copyWith(fontSize: 18.0),
                              ),
                            ),
                          ],
                        ),
                        getBottomSizedBox(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: StreamBuilder<String>(
                              stream: _profileBloc.userName,
                              builder: (context, snapshot) {
                                return CommonInputField(
                                  controller:
                                      _profileBloc.userNameController,
                                  onSubmit: (value) {
                                    if (value.isNotEmpty) {
                                      MyRouter.navigatorKey.currentState
                                          .pop();
                                    }
                                      _profileBloc.add(UpdateProfileEvent.loginWithFbUsername(addProfile: widget.isFacebook));
                                  },
                                  errorText: snapshot.error != null
                                      ? "Username ${snapshot.error}"
                                      : null,
                                  textChanged:
                                      _profileBloc.changeUserName,
                                  icon: FontAwesomeIcons.facebookF,
                                  hasSuffix: true,
                                  action: TextInputAction.done,
                                  labelText: "Facebook Username",
                                );
                              }),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16.0),
                          child: RaisedGradientButton(
                            onPressed: () {
                              if (_profileBloc.userNameController.text.isNotEmpty) {
                                MyRouter.navigatorKey.currentState.pop();
                              }
                                _profileBloc.add(UpdateProfileEvent.loginWithFbUsername(addProfile: widget.isFacebook));
                            },
                            text: "Submit",
                            isDialog: true,
                          ),
                        )
                          ],
                        ));
                  },
                );
              }
              if (state is FaceBookVerified) {
                if(widget.fromCompleteDialog&&!widget.isInsta){
                  MyRouter.navigatorKey.currentState
                      .pushReplacementNamed(MyRouter.home);
                }
                else if (widget.isInsta) {
                  MyRouter.navigatorKey.currentState.pushNamed(
                      MyRouter.connectFacebook,
                      arguments: ConnectSocialPlatformsArguments(
                        fromCompleteDialog: true,
                          isFacebook: false, isInsta: true));
                }
                else if (widget.addRequest != null) {
                  MyRouter.navigatorKey.currentState.pop("Added/Udated");
                }  else {
                  MyRouter.navigatorKey.currentState
                      .pushReplacementNamed(MyRouter.home);
                }
              } else if (state is InstaVerified) {
                if (widget.isFacebook) {
                  MyRouter.navigatorKey.currentState.pushNamed(
                      MyRouter.connectFacebook,
                      arguments: ConnectSocialPlatformsArguments(
                          isFacebook: true, isInsta: false));
                }
                else if(widget.fromCompleteDialog){
                  MyRouter.navigatorKey.currentState
                      .pushReplacementNamed(MyRouter.home);
                }
                else {
                  MyRouter.navigatorKey.currentState.pop("Added/Udated");
                }
              }
            },
            child: BlocBuilder<UpdateProfileBloc, UpdateProfileState>(
              builder: (context, state) {
                return state.when(
                    successProfileState: (s) =>buildHomeWidget(context),
                    intialProfileState: (c) => buildHomeWidget(context),
                    loadingProfileState: (c) => Center(
                          child: CircularProgressIndicator(),
                        ),
                    errorProfileState: (c) => showErrorView(c.error, () {
                      // BlocProvider.of<HomeBloc>(context).changePage(2);
                      if(widget.fromCompleteDialog)
                          {
                            MyRouter.navigatorKey.currentState.popUntil((route) {
                              if (route.settings.name != MyRouter.home) {
                                MyRouter.navigatorKey.currentState
                                    .pushReplacementNamed(MyRouter.home);
                              }
                              return true;
                            });
                          }
                      else MyRouter.navigatorKey.currentState.pop();}),
                    instaVerified: (c) => buildHomeWidget(context),
                    faceBookVerified: (c) => buildHomeWidget(context),
                    fbUserNameRegistered: (_) =>buildHomeWidget(context),
                    getFbUserName: (_) =>buildHomeWidget(context));
              },
            ),
          ),
        ),
      ),
    );
  }

  Container buildFaceBookTopView() {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Color(0xFF4260A9),
          boxShadow: [
            BoxShadow(
                offset: Offset(-5, 12),
                blurRadius: 29.0,
                color: Color(0xFF1E3E8B).withOpacity(.6))
          ]),
      height: Device.get().isTablet
          ? SizeConfig.screenHeight * .80
          : SizeConfig.screenHeight * 1.1,
      width: Device.get().isTablet
          ? SizeConfig.screenWidth * .80
          : SizeConfig.screenWidth * 1.1,
      child: Icon(
        FontAwesomeIcons.facebookF,
        size: SizeConfig.safeBlockHorizontal * 40,
        color: Colors.white,
      ),
    );
  }

  Container buildInstaTopView() {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(colors: [
            Color(0xFF6057C9),
            Color(0xFF9244A4),
            Color(0xFFCA2E7B),
//        Color(0xFFFECB6F),
          ]),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 2),
                blurRadius: 4.0,
                color: Colors.black.withOpacity(.5)),
          ]),
      height: Device.get().isTablet
          ? SizeConfig.screenHeight * .80
          : SizeConfig.screenHeight * 1.1,
      width: Device.get().isTablet
          ? SizeConfig.screenWidth * .80
          : SizeConfig.screenWidth * 1.1,
      child: Icon(
        FontAwesomeIcons.instagram,
        size: SizeConfig.safeBlockHorizontal * 40,
        color: Colors.white,
      ),
    );
  }

  Widget buildHomeWidget(BuildContext context) {
    var bloc = BlocProvider.of<UpdateProfileBloc>(context);
    return SafeArea(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            left: -100,
            top: 120,
            child: Container(
              alignment: Alignment.centerLeft,
              height: SizeConfig.screenHeight,
              width: SizeConfig.screenWidth,
              child: Icon(
                widget.isFacebook
                    ? FontAwesomeIcons.facebookF
                    : FontAwesomeIcons.instagram,
                size: SizeConfig.safeBlockHorizontal * 80,
                color: Colors.black.withOpacity(.02),
              ),
            ),
          ),
          Positioned(
            left: Device.get().isTablet
                ? SizeConfig.blockSizeWidth * 40
                : SizeConfig.blockSizeWidth * 10,
            top: Device.get().isTablet
                ? SizeConfig.blockSizeHeight * -30
                : SizeConfig.blockSizeHeight * -40,
            child:
                widget.isFacebook ? buildFaceBookTopView() : buildInstaTopView(),
          ),
          SingleChildScrollView(
            child: AnimatedBuilder(
              animation: controller,
              builder: (context, child) => Transform(
                transform: Matrix4.translationValues(
                    0, animation.value * SizeConfig.screenHeight, 0),
                child: child,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: SizeConfig.screenHeight / 2,
                  ),
                  getBottomSizedBox(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(children: [
                          TextSpan(
                              text:
                                  "Surprise Meet recommends you to provide more information about yourself. This will help you find your",
                              style: Theme.of(context).textTheme.body2),
                          TextSpan(
                              text: " Surprise Meet ",
                              style: Theme.of(context)
                                  .textTheme
                                  .body2
                                  .copyWith(color: Colors.red)),
                          TextSpan(
                              text: "quickly.",
                              style: Theme.of(context).textTheme.body2)
                        ])),
                  ),
                  getBottomSizedBox(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: RaisedGradientButton(
                      text: widget.isFacebook
                          ? "Connect Facebook"
                          : "Connect Instagram",
                      onPressed: () {
//                    MyRouter.navigatorKey.currentState.pushNamed(
//                        MyRouter.connectFacebook,
//                        arguments: ConnectSocialPlatformsArguments(
//                            isFacebook: false, isInsta: true));
                        if (widget.addRequest == null) {
                          widget.isFacebook
                              ? bloc.add(UpdateProfileEvent.addFacebook())
                              : Navigator.of(context).pushNamed(MyRouter.instaWebView).then((token){
                            bloc.add(UpdateProfileEvent.addInsta(token: token));
                          });
                        } else {
                          if (widget.addRequest) {
                            if (widget.isFacebook) {
                              _profileBloc.add(UpdateProfileEvent.addFacebook());
                            } else {
                              MyRouter.navigatorKey.currentState.pushNamed(MyRouter.instaWebView).then((token){
                                bloc.add(UpdateProfileEvent.addInsta(token: token));
                              });
                            }
                          } else {
                            if (widget.isFacebook) {
                              _profileBloc
                                  .add(UpdateProfileEvent.updateFacebook());
                            } else {
                              MyRouter.navigatorKey.currentState.pushNamed(MyRouter.instaWebView).then((token){
                                print("insta token from inten "+token.toString());
                                bloc.add(UpdateProfileEvent.updateInsta(token: token));
                              });
                            }
                          }
                        }
                      },
                      gradient: AppTheme.gradient(),
                      isDialog: true,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32.0),
                    child: Text(
                      "We don't read or store your data. We just want you to connect to ${widget.isFacebook ? "Facebook" : "Instagram"} so that we can fetch your username.",
                      style: Theme.of(context)
                          .textTheme
                          .subhead
                          .copyWith(fontSize: 13.0, fontStyle: FontStyle.italic),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Wrap(
            children: <Widget>[
              AppBar(
                elevation: 0.0,
              )
            ],
          ),
          Positioned.fill(
              child: Align(
            alignment: Alignment(.98, .95),
            child: Visibility(
              visible: widget.fromCompleteDialog,
              child: Container(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: FlatButton(
                    child: Text("Skip"),
                    onPressed: () {
                      if(widget.fromCompleteDialog&&widget.isFacebook&&widget.isInsta){
                        MyRouter.navigatorKey.currentState.pushNamed(
                            MyRouter.connectFacebook,
                            arguments: ConnectSocialPlatformsArguments(
                                isFacebook: false, isInsta: true,fromCompleteDialog: true));
                      }
//                      else if(widget.fromCompleteDialog&&widget.isInsta){
//                        MyRouter.navigatorKey.currentState.pushNamed(MyRouter.instaWebView).then((token){
//                          print("insta token from inten "+token.toString());
//                          bloc.add(UpdateProfileEvent.updateInsta(token: token));
//                        });
//                      }
                     else
                        MyRouter.navigatorKey.currentState.popUntil((route) {
                          if (route.settings.name != MyRouter.home) {
                            MyRouter.navigatorKey.currentState
                                .pushReplacementNamed(MyRouter.home);
                          }
                          return true;
                        });
                    },
                  )),
            ),
          ))
        ],
      ),
    );
  }
}
