// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'socialrequesttype.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class SocialRequestType extends Equatable {
  const SocialRequestType(this._type);

  factory SocialRequestType.addSocialAccount(
      {@required Map<String, String> body}) = AddSocialAccount;

  factory SocialRequestType.updateSocialAccount(
      {@required Map<String, String> body}) = UpdateSocialAccount;

  final _SocialRequestType _type;

//ignore: missing_return
  R when<R>(
      {@required
          FutureOr<R> Function(AddSocialAccount) addSocialAccount,
      @required
          FutureOr<R> Function(UpdateSocialAccount) updateSocialAccount}) {
    assert(() {
      if (addSocialAccount == null || updateSocialAccount == null) {
        throw 'check for all possible cases';
      }
      return true;
    }());
    switch (this._type) {
      case _SocialRequestType.AddSocialAccount:
        return addSocialAccount(this as AddSocialAccount);
      case _SocialRequestType.UpdateSocialAccount:
        return updateSocialAccount(this as UpdateSocialAccount);
    }
  }

  R whenOrElse<R>(
      {FutureOr<R> Function(AddSocialAccount) addSocialAccount,
      FutureOr<R> Function(UpdateSocialAccount) updateSocialAccount,
      @required FutureOr<R> Function(SocialRequestType) orElse}) {
    assert(() {
      if (orElse == null) {
        throw 'Missing orElse case';
      }
      return true;
    }());
    switch (this._type) {
      case _SocialRequestType.AddSocialAccount:
        if (addSocialAccount == null) break;
        return addSocialAccount(this as AddSocialAccount);
      case _SocialRequestType.UpdateSocialAccount:
        if (updateSocialAccount == null) break;
        return updateSocialAccount(this as UpdateSocialAccount);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(AddSocialAccount) addSocialAccount,
      FutureOr<void> Function(UpdateSocialAccount) updateSocialAccount}) {
    assert(() {
      if (addSocialAccount == null && updateSocialAccount == null) {
        throw 'provide at least one branch';
      }
      return true;
    }());
    switch (this._type) {
      case _SocialRequestType.AddSocialAccount:
        if (addSocialAccount == null) break;
        return addSocialAccount(this as AddSocialAccount);
      case _SocialRequestType.UpdateSocialAccount:
        if (updateSocialAccount == null) break;
        return updateSocialAccount(this as UpdateSocialAccount);
    }
  }

  @override
  List get props => const [];
}

@immutable
class AddSocialAccount extends SocialRequestType {
  const AddSocialAccount({@required this.body})
      : super(_SocialRequestType.AddSocialAccount);

  final Map<String, String> body;

  @override
  String toString() => 'AddSocialAccount(body:${this.body})';
  @override
  List get props => [body];
}

@immutable
class UpdateSocialAccount extends SocialRequestType {
  const UpdateSocialAccount({@required this.body})
      : super(_SocialRequestType.UpdateSocialAccount);

  final Map<String, String> body;

  @override
  String toString() => 'UpdateSocialAccount(body:${this.body})';
  @override
  List get props => [body];
}
