import 'package:super_enum/super_enum.dart';
part 'socialrequesttype.g.dart';
@superEnum
enum _SocialRequestType {
  @Data(fields: [DataField<Map<String,String>>('body')])
  AddSocialAccount,
  @Data(fields: [DataField<Map<String,String>>('body')])
  UpdateSocialAccount,
}
