import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:surprise_meet/utils/numberkeyboard.dart';

class CommonInputField extends StatefulWidget {
  final Function(String) textChanged;
  final String errorText;
  final String labelText;
  final IconData icon;
  final TextInputAction action;
  final Function(String) onSubmit;
  final FocusNode node;
  final TextInputType inputType;
  final TextEditingController controller;
  final bool isReadOnly;
  final bool hasSuffix;
  final VoidCallback onCountryTap;
  final String countryCode;

  const CommonInputField(
      {Key key,
      @required this.textChanged,
       this.errorText,
      @required this.labelText,
      @required this.icon,
      this.inputType = TextInputType.text,
      this.action = TextInputAction.next,
      @required this.onSubmit,
      this.node,
      this.controller,
      this.isReadOnly = false,
      this.hasSuffix = false,
      this.onCountryTap,
      this.countryCode})
      : super(key: key);

  @override
  _CommonInputFieldState createState() => _CommonInputFieldState();
}

class _CommonInputFieldState extends State<CommonInputField> {
  bool pressed = false;
  OverlayEntry overlayEntry;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.inputType);
    if (widget.inputType == TextInputType.number && Platform.isIOS) {
      widget?.node?.addListener((){
        if(widget?.node?.hasFocus==true){
          if(Platform.isIOS)
          showOverLay(context);
        }
        else{
          removeOverLay();
        }
      });
    }
//      KeyboardVisibilityNotification().addNewListener(
//        onChange: (bool visible) {
//          if (widget?.node?.hasFocus == true) {
//            showOverLay(context);
//          } else {
//            removeOverLay();
//          }
//        },
//      );
    }


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 32.0),
      child: Row(
        children: <Widget>[

          GestureDetector(
            onTap: widget.onCountryTap,
            child: AbsorbPointer(
              child: Visibility(
                visible: widget.inputType == TextInputType.number,
                child: Container(
                  alignment: Alignment.topCenter,
//                  height: 0.0,
//                  constraints: BoxConstraints(maxHeight: 60.0),
                  margin: EdgeInsets.only(right: 8.0),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 0.0),
                  decoration: BoxDecoration(
//                      color: Colors.red,
                      border: Border(
                          right: BorderSide(
                              color: Colors.black.withOpacity(.5),
                              width: 1.0))
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(widget.countryCode != null
                          ? "+" + widget.countryCode
                          : ""),
                      Icon(Icons.arrow_drop_down)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            child: TextField(
              maxLength: 32,

              controller: widget.controller,
              keyboardType: widget.inputType == TextInputType.phone
                  ? TextInputType.numberWithOptions(signed: true, decimal: true)
                  : widget.inputType,
              focusNode: widget.node,
              enabled: true,
              readOnly: widget.isReadOnly,
              inputFormatters: <TextInputFormatter>[
                widget.inputType == TextInputType.number
                    ? WhitelistingTextInputFormatter.digitsOnly
                    : BlacklistingTextInputFormatter.singleLineFormatter,
              ],
              textInputAction: widget.action,
              onSubmitted: widget.onSubmit,
              style: Theme.of(context).textTheme.body2,
              autofocus: false,
              onChanged: widget.textChanged,
              decoration: InputDecoration(
                  counter: Offstage(),
                  suffixIcon: Visibility(
                    visible: widget.hasSuffix,
                    child: GestureDetector(
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        showFacebookPopUp(context);
                      },
                      child: AbsorbPointer(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Icon(
                              FontAwesomeIcons.exclamation,
                              color: Colors.grey,
                              size: 12.0,
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: Colors.grey, width: 2.0)),
                            padding: const EdgeInsets.all(4.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  labelText: widget.labelText,
                  errorText: widget.errorText,
                  prefixIcon: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(16.0),
                        child: Icon(
                          widget.icon,
                          size: 18.0,
                        ),
                      ),

                    ],
                  )),
            ),
          ),
        ],
      ),
    );
  }

  void showFacebookPopUp(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(20.0),
                topLeft: Radius.circular(20.0))),
        useRootNavigator: true,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Wrap(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(16.0),
                height: SizeConfig.screenHeight / 5,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20.0),
                        topLeft: Radius.circular(20.0)),
                    gradient: AppTheme.gradient()),
                child: Icon(
                  FontAwesomeIcons.facebookF,
                  color: Colors.white,
                  size: SizeConfig.blockSizeWidth * 20,
                ),
              ),
              Column(
                children: <Widget>[
                  SizedBox(
                    height: SizeConfig.blockSizeWidth * 4,
                  ),
                  Text(
                    "Don't worry, if you don't \n have your flame's username ",
                    style: Theme.of(context)
                        .textTheme
                        .headline
                        .copyWith(fontSize: SizeConfig.blockSizeWidth * 4.5),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: SizeConfig.blockSizeWidth * 4,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorPrimaryDark,
                          ),
                          child: Text(
                            "1",
                            style: Theme.of(context)
                                .textTheme
                                .body2
                                .copyWith(color: Colors.white),
                          ),
                          padding: const EdgeInsets.all(6.0),
                        ),
                        SizedBox(
                          width: SizeConfig.blockSizeWidth * 2,
                        ),
                        Text(
                          "Search the profile",
                          style: Theme.of(context).textTheme.body2,
                        )
                      ],
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.ideographic,
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.blockSizeWidth * 4,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorPrimaryDark,
                          ),
                          child: Text(
                            "2",
                            style: Theme.of(context)
                                .textTheme
                                .body2
                                .copyWith(color: Colors.white),
                          ),
                          padding: const EdgeInsets.all(6.0),
                        ),
                        SizedBox(
                          width: SizeConfig.blockSizeWidth * 2,
                        ),
                        Text(
                          "If you are on the computer or \n laptop  just copy the profile's  \n link and paste it",
                          style: Theme.of(context).textTheme.body2,
                        )
                      ],
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.ideographic,
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.blockSizeWidth * 4,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorPrimaryDark,
                          ),
                          child: Text(
                            "3",
                            style: Theme.of(context)
                                .textTheme
                                .body2
                                .copyWith(color: Colors.white),
                          ),
                          padding: const EdgeInsets.all(6.0),
                        ),
                        SizedBox(
                          width: SizeConfig.blockSizeWidth * 2,
                        ),
                        Text(
                          "If you are on the mobile just go \n to profile > more copy URL link \n and then paste it",
                          style: Theme.of(context).textTheme.body2,
                        )
                      ],
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.ideographic,
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.blockSizeWidth * 4,
                  ),
                ],
              )
            ],
          );
        });
  }
}
