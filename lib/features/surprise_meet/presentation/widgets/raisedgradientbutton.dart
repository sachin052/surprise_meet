import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'package:surprise_meet/gradientbuttonstyle.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
import 'package:tap_debouncer/tap_debouncer.dart';

class RaisedGradientButton extends StatelessWidget {
  final String text;
  final Gradient gradient;
  final double width;
  final Function onPressed;
  final bool isDialog;

  const RaisedGradientButton({
    Key key,
    this.gradient,
    this.width = double.infinity,
    this.isDialog = false,
    this.onPressed,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CommonGradientButton(text, onPressed, isDialog);
  }
}

class CommonGradientButton extends StatefulWidget {
  CommonGradientButton(this.title, this.onPressed, this.isDialog);

  final String title;
  final Function onPressed;
  final bool isDialog;

  @override
  _CommonGradientButtonState createState() => _CommonGradientButtonState();
}

class _CommonGradientButtonState extends State<CommonGradientButton> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    return TapDebouncer(
      cooldown: const Duration(milliseconds: 1000),
      onTap: widget.onPressed,
      builder: (BuildContext context, Future<void> Function() onTap) { return Parent(
      style: gradientCardStyle(pressed),
      gesture: Gestures()
        ..onTap(onTap)
        ..isTap((isTapped) => setState(() => pressed = isTapped)),
      child: Container(
        width: SizeConfig.screenWidth,
        alignment: Alignment.center,
        child: Text(
          widget.title,
          style: widget.isDialog
              ? Theme.of(context).textTheme.body2.copyWith(color: Colors.white)
              : Theme.of(context)
              .textTheme
              .headline
              .copyWith(color: Colors.white,fontSize: 18.0),
        ),
      ),
    ); },
    );
  }

  final TxtStyle itemTitleTextStyle = TxtStyle()
    ..bold()
    ..fontSize(16);
}
