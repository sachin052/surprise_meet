
import 'package:flutter/material.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildBottomView.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
class NoInternetConnection extends StatelessWidget {
  final VoidCallback onRetry;

  const NoInternetConnection({Key key, @required this.onRetry}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: onRetry,
      child: ListView(
        children: <Widget>[
          Container(
            height: SizeConfig.screenHeight*.80,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AspectRatio(
                    aspectRatio: 2.8,
                    child: Image.asset(Images.noInternet,)),
                getBottomSizedBox(),
                Text("Whoops!",style: Theme.of(context).textTheme.title,),
                getBottomSizedBox(),
                Text("Your connection seems sleeping \n Please check your internet setting \n and try again later. ",style: Theme.of(context).textTheme.body2,textAlign: TextAlign.center,),
                getBottomSizedBox(),
                FlatButton(
                    onPressed: onRetry,
                    child: Text("Retry",style: Theme.of(context).textTheme.body2.copyWith(color: AppColors.colorPrimaryDark,decoration: TextDecoration.underline),)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
