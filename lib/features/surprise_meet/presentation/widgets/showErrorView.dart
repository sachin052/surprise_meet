import 'package:flutter/material.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/nointernet.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/unauthorizedaccess.dart';

import 'nodatafound.dart';

Widget showErrorView(String error,VoidCallback onRetry){
  if(error.contains("internet")){
    return NoInternetConnection(onRetry: onRetry,);
  }
  else if(error.contains("Access denied")){
    return UnAuthorizedAccess();
  }
  return NoDataFoundWidget(errorMessage: error,onBackClick: onRetry,);
}