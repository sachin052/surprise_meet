import 'package:flutter/material.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildBottomView.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';
class NoDataFoundWidget extends StatelessWidget {
  final String errorMessage;
  final VoidCallback onBackClick;
  const NoDataFoundWidget({Key key, @required this.errorMessage, this.onBackClick}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.screenHeight*.80,
      alignment: Alignment.center,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AspectRatio(
                aspectRatio: 2.8,
                child: Image.asset(Images.wenWrong,)),
            getBottomSizedBox(),
            Text("Whoops!",style: Theme.of(context).textTheme.title,),
            getBottomSizedBox(),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(errorMessage,textAlign: TextAlign.center,style: Theme.of(context).textTheme.body2,),
            ),
            getBottomSizedBox(),
            FlatButton(
                onPressed: onBackClick==null?(){MyRouter.navigatorKey.currentState.pop();}:onBackClick,
                child: Text("Back",style: Theme.of(context).textTheme.body2.copyWith(color: AppColors.colorPrimaryDark,decoration: TextDecoration.underline),)),
          ],),
      ),
    );
  }
}
