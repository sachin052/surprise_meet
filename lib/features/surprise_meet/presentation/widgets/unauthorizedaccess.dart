import 'package:flutter/material.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/buildBottomView.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

import '../../../../injection_container.dart';

class UnAuthorizedAccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AspectRatio(
                    aspectRatio: 2.5,
                    child: Image.asset(
                      Images.wenWrong,
                    )),
                getBottomSizedBox(),
                Text(
                  "Whoops!",
                  style: Theme.of(context).textTheme.title,
                ),
                getBottomSizedBox(),
                Text(
                  "Unauthorized Access ! Login Again.",
                  style: Theme.of(context).textTheme.body2,
                ),
                getBottomSizedBox(),
                RaisedGradientButton(
                  onPressed: () async {
                    await injector<SharedPrefHelper>().clearData();
                    MyRouter.navigatorKey.currentState
                        .popUntil(ModalRoute.withName(MyRouter.loginPage));
                  },
                  gradient: AppTheme.gradient(),
                  text: "Login",
                  isDialog: true,
                )
              ],
            ),
          ),
        ),
//        AppBar(elevation: 0.0,)
      ],
    );
  }
}
