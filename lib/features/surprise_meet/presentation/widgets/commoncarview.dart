import 'package:flutter/material.dart';
import 'package:surprise_meet/style/apptheme.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

class CommonCard extends StatelessWidget {
  final Widget child;
  final Widget topContainerChild;
  const CommonCard({Key key, this.child, this.topContainerChild}) : super(key: key);
  @override
  Widget build(BuildContext context) {
     return Dialog(
       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
       child: SingleChildScrollView(
         physics: ClampingScrollPhysics(),
         child: ClipRRect(
           borderRadius: BorderRadius.circular(18.0),
           child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: SizeConfig.safeBlockVertical*20,
                child: topContainerChild,
                alignment: Alignment.center,
                decoration: BoxDecoration(gradient: AppTheme.gradient(),borderRadius: BorderRadius.only(topLeft:Radius.circular(18.0),topRight: Radius.circular(18.0)),),
              ),
              child
            ],
    ),
         ),
       ),
     );
  }
}

