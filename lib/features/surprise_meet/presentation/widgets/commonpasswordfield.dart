import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surprise_meet/style/colors.dart';
import 'package:surprise_meet/style/images.dart';
import 'package:flutter/services.dart';
class CommonPasswordField extends StatefulWidget {
  final Function(String) changepass;
  final String errorText;
  final String labelText;
  final TextInputAction action;
  final Function(String) onSubmit;
  final FocusNode node;
  final TextEditingController controller;
  const CommonPasswordField({
    Key key,
    @required this.changepass,
    @required this.errorText,
    @required this.labelText,
    this.action = TextInputAction.next,
    @required this.onSubmit,
    @required this.node, this.controller,
  }) : super(key: key);

  @override
  _CommonPasswordFieldState createState() => _CommonPasswordFieldState();
}

class _CommonPasswordFieldState extends State<CommonPasswordField> {

  bool visibility=true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget?.node?.addListener((){setState(() {

    });});
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:32.0),
      child: TextField(
        controller: widget.controller,
        focusNode: widget.node,
        textInputAction: widget.action,
        style: Theme.of(context).textTheme.body2,
        autofocus: false,
        obscureText: visibility,
        onChanged: widget.changepass,
        onSubmitted: widget.onSubmit,
        decoration: InputDecoration(
//          contentPadding: EdgeInsets.only(left:64.0),
//          contentPadding: const EdgeInsets.only(left:80.0),
            labelText: widget.labelText,
            errorText: widget.errorText,
            suffix: Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      setState(() {
                       visibility = !visibility;
                      });
                    },
                    child: Icon(!visibility?FontAwesomeIcons.eye:FontAwesomeIcons.eyeSlash, size: 18.0),
                ),
            ),
          prefixIcon:Icon(Icons.lock_outline,),
        ),
      ),
    );
  }
}
