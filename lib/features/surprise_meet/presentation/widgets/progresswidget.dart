import 'dart:math' as math;
import 'package:flutter/cupertino.dart';

class ProgressBarWidget extends CustomPainter{
  Color lineColor;
  Color completeColor;
  double completePercent;
  double width;
  ProgressBarWidget({this.lineColor,this.completeColor,this.completePercent,this.width});
  @override
  void paint(Canvas canvas, Size size) {
    Paint line = new Paint()
      ..color = lineColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;
    Paint complete = new Paint()
      ..color = completeColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;
    Offset center  = new Offset(size.width/3, size.height/3);
    double radius  = math.min(size.width/3,size.height/3);
    canvas.drawCircle(
        center,
        radius,
        line
    );
    double arcAngle = 2*math.pi* (completePercent/100);
    canvas.drawArc(
        new Rect.fromCircle(center: center,radius: radius),
        -math.pi/2,
        arcAngle,
        false,
        complete
    );
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}