import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:surprise_meet/style/colors.dart';

showSnackBar(BuildContext context, String text, bool isError) {
//  key.currentState.showSnackBar(SnackBar(
//      backgroundColor: isError ? Colors.red : AppColors.colorAccent,
//      content: Wrap(
//        children: <Widget>[
//          Row(
//            children: <Widget>[
//              Icon(
//                isError ? Icons.error : Icons.done,
//                color: Colors.white,
//              ),
//              SizedBox(
//                width: SizeConfig.safeBlockVertical,
//              ),
//              Text(
//                text,
//              )
//            ],
//          ),
//        ],
//      )));
  Flushbar(
    margin: EdgeInsets.all(8),
    borderRadius: 8,
    backgroundColor: isError ? Colors.red : AppColors.colorAccent,
   flushbarStyle: FlushbarStyle.FLOATING,
    icon: Icon(
      isError ? Icons.error : Icons.done,
      color: Colors.white,
    ),
    message: text,
    duration: Duration(seconds: 3),
  )..show(context);
}
