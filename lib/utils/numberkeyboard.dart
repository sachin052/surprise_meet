
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:surprise_meet/style/colors.dart';

import '../main.dart';

class InputDoneView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      padding: const EdgeInsets.only(right: 10.0),
      child: Align(
        alignment: Alignment.topRight,
        child: OutlineButton(
          highlightedBorderColor: AppColors.colorPrimaryDark,
          onPressed: () {
            removeOverLay();
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Text(
            "Done",style: Theme.of(context).textTheme.body2.copyWith(color: AppColors.colorPrimaryDark),
          ),
        ),
      ),
    );
  }
}

showOverLay(BuildContext context) {
  if (overlayEntry != null&&!Platform.isIOS) return;
  OverlayState overlayState = Overlay.of(context);
  overlayEntry?.maintainState=true;
  overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
            bottom: MediaQuery.of(context).viewInsets.bottom,
            right: 0.0,
            left: 0.0,
            child: InputDoneView(),
          ));
 overlayState.insert(overlayEntry);


}

removeOverLay() {
  if (overlayEntry != null) {
    overlayEntry.remove();
    overlayEntry = null;
  }
}
