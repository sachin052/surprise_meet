// Used for dependency injection
import 'package:dio/dio.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surprise_meet/features/surprise_meet/data/api/apihelper.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/chat/chatrepodatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/datasource/localdatasource.dart';
import 'package:surprise_meet/features/surprise_meet/data/repositories/authrepoimpl.dart';
import 'package:surprise_meet/features/surprise_meet/data/repositories/chatrepoimpl.dart';
import 'package:surprise_meet/features/surprise_meet/data/repositories/userdatarepoimpl.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/delete_recent_match.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/getchatusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/getselectedchatusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/chat/report_user.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/addpotentialmeet.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/deletpotentialmeets.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/getpotentialmeets.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/getprofileusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/potentialmeet/updatepotentialmeet.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/checkfbusernameusercase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/logoutusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/updatepasswordusecase.dart';
import 'package:surprise_meet/features/surprise_meet/domain/usecases/user/updateuserprofileusecaes.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/home/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/login/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/managepotentialmeets/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/resetpassword/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/signup/bloc.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/udpateprofile/bloc.dart';
import 'package:surprise_meet/pushnotficationhandler.dart';
import 'features/surprise_meet/data/datasource/authrepo/authrepodatasource.dart';
import 'features/surprise_meet/data/datasource/userdatarepo/userdatasource.dart';
import 'features/surprise_meet/domain/repositories/authrepo.dart';
import 'features/surprise_meet/domain/repositories/chatrepo.dart';
import 'features/surprise_meet/domain/repositories/userdatarepo.dart';
import 'features/surprise_meet/domain/usecases/user/addfacebookusecase.dart';
import 'features/surprise_meet/domain/usecases/user/addinstgramusecase.dart';
import 'features/surprise_meet/domain/usecases/user/instaloginusecase.dart';
import 'features/surprise_meet/domain/usecases/user/loginUseCase.dart';
import 'features/surprise_meet/domain/usecases/user/loginusingfbusecase.dart';
import 'features/surprise_meet/domain/usecases/user/resetpasswordusecase.dart';
import 'features/surprise_meet/domain/usecases/user/signupusecase.dart';
import 'features/surprise_meet/presentation/bloc/chat/chat_bloc.dart';


final injector = GetIt.instance;
Future init()async{

  var dio=Dio();
  injector.registerLazySingleton(()=>dio);
  injector.registerLazySingleton(()=>ApiHelper(injector(),injector()));

//   Local
  injector.registerLazySingleton<SharedPrefHelper>(()=>SharedPrefHelperImpl(injector()));

  //Data Source
  injector.registerLazySingleton<AuthRepoDataSource>(()=>AuthRepoDataSourceImpl(injector(),injector()));
  injector.registerLazySingleton<UserDataSource>(()=>UserDataSourceImpl(injector()));
  injector.registerLazySingleton<ChatDataSource>(()=>ChatDataSourceImpl(injector()));

  // local data source
  final sharedPreferences = await SharedPreferences.getInstance();
  injector.registerLazySingleton(() => sharedPreferences);

  // Repos
  injector.registerLazySingleton<AuthRepo>(()=>AuthRepoImpl(injector(),injector()));
  injector.registerLazySingleton<UserDataRepo>(()=>UserDataRepoImpl(injector()));
  injector.registerLazySingleton<ChatRepo>(()=>ChatRepoImpl(injector()));

  // UserCases
  //Authentication
  injector.registerLazySingleton(()=>SignUpUseCase(injector()));
  injector.registerLazySingleton(()=>LoginUseCase(injector()));
  injector.registerLazySingleton(()=>ResetPasswordUseCase(injector()));
  injector.registerFactory(()=>LoginUsingFBUseCase(injector()));
  injector.registerFactory(()=>LoginUsingInsta(injector()));
  injector.registerLazySingleton(()=>AddUpdateFacebookUseCase(injector()));
  injector.registerLazySingleton(()=>AddUpdateInstaUseCase(injector()));
  injector.registerFactory(()=>LogOutUseCase(injector()));
  injector.registerLazySingleton(()=>CheckFBUserNameUseCase(injector()));
  injector.registerLazySingleton(()=>UpdatePasswordUseCase(injector()));
  // UserCases
  //Potential Meet
  injector.registerLazySingleton(()=>GetPotentialMeetsUseCase(injector()));
  injector.registerLazySingleton(()=>AddPotentialMeetsUseCase(injector()));
  injector.registerLazySingleton(()=>GetProfileUseCase(injector()));
  injector.registerLazySingleton(()=>DeletePotentialMeetsUseCase(injector()));
  injector.registerLazySingleton(()=>UpdatePotentialMeetUseCase(injector()));
  injector.registerLazySingleton(()=>UpdateUserProfileUseCase(injector()));
  injector.registerLazySingleton(()=>ReportUserUseCase(injector()));

  // UserCases
  //Chats
  injector.registerLazySingleton(()=>GetChatUseCase(injector()));
  injector.registerLazySingleton(()=>GetSelectedChatUseCase(injector()));
  injector.registerLazySingleton(() => DeleteRecentMatchUseCase(injector()));
  // Blocs for state management
  injector.registerFactory(()=>LoginBloc(injector(),injector(),injector(),injector()));
  injector.registerFactory(()=>SignUpBloc(injector()));
  injector.registerFactory(()=>ResetPasswordBloc(injector()));
  injector.registerFactory(()=>UpdateProfileBloc(injector(),injector(),injector(),injector(),injector(),injector(),injector()));
  injector.registerFactory(()=>ManagePotentialMeetBloc(injector(),injector(),injector(),injector()));
  injector.registerFactory(()=>HomeBloc());
  injector.registerFactory(()=>ChatBloc(injector(),injector(),injector(),injector(),injector()));
  injector.registerFactory(()=>FlutterLocalNotificationsPlugin());
  injector.registerLazySingleton(()=>PushNotificationHandler(injector(),injector()));
  // PushNotification
   await injector<PushNotificationHandler>().init();

}
Future<bool >getUser() async=> injector<SharedPrefHelper>().getUserData()!=null;