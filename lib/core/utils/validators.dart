import 'dart:async';

import 'package:surprise_meet/main.dart';

  final validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    if (email.isValidEmail) {
      sink.add(email);
    } else {
      sink.addError('Enter a valid email');
    }
  });
  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (string, sink) {
   try{
     if (string.length > 7) {
       sink.add(string);
     } else {
       sink.addError("Req min 8 Chars");
     }
   }catch(e){
     print("Exceptio:"+e.toString());
   }
  });
  final notEmptyValidator = StreamTransformer<String, String>.fromHandlers(
      handleData: (string, sink) {
    if (string.isNotEmpty) {
      sink.add(string);
    } else {
      sink.addError("Must not be empty");
    }
  });

final phoneValidator = StreamTransformer<String, String>.fromHandlers(
    handleData: (string, sink) {
      if (string.length>=10) {
        sink.add(string);
      } else {
        sink.addError("Enter a valid phone");
      }
    });

   confirmPassValidator(String password) => StreamTransformer<String, String>.fromHandlers(
      handleData: (string, sink) {
        print("actual pass"+ password);
        print("c pass"+ string);
        if (string.isNotEmpty&&string==password) {
          sink.add(string);
        } else {
          sink.addError("Password Mismatched");
        }
      });
