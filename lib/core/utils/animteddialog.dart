import 'package:flutter/material.dart';

void showAnimatedDialog(BuildContext context, Widget child,{bool barrierDismissible=true}) {
//  showDialog(context: context,builder: (_)=>ForgetDialog());
  showGeneralDialog(
      barrierColor: Colors.black.withOpacity(0.5),
      transitionBuilder: (context, a1, a2, widget) {
        final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
        return Transform(
          transform: Matrix4.translationValues(0.0, curvedValue * -400, 0.0),
          child: ScaleTransition(
            scale: a1,
            child: Opacity(
                opacity: a1.value,
                child: child),
          ),
        );
      },
      transitionDuration: Duration(milliseconds: 500),
      barrierDismissible: barrierDismissible,
      barrierLabel: '',
      context: context,
      pageBuilder: (context, animation1, animation2) =>Container());
}
