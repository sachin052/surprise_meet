import 'package:auto_route/auto_route_annotations.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/signup/terms_of_use.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/updatepassword/updatepasswordscreen.dart';
import 'package:surprise_meet/main.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/connectfacebook.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/chat/selectedchatscreen.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/home.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/addpotentialmeet/addpotentialmeets.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/homepage/editpotentialmeet.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/profile/editprofile.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/instawebview.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/login.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/signup/signup.dart';

@autoRouter
class $MyRouter {
  @initial
  LoginPage loginPage;
  @CupertinoRoute(title: "Sign Up")
  SignUp signUp;
  @CupertinoRoute()
  ConnectSocialPlatforms connectFacebook;
  @CupertinoRoute()
  Home home;
  @MaterialRoute()
  AddPotentialMeets addPotentialMeets;
  @CupertinoRoute()
  EditPotentialMeet editPotentialMeet;
  @CupertinoRoute()
  EditProfile editProfile;
  @CupertinoRoute()
  SelectedChatScreen chatScreen;
  @CupertinoRoute()
  InstaWebView instaWebView;
  @CupertinoRoute()
  MyApp myApp;
  @CupertinoRoute()
  UpdatePasswordScreen updatePasswordScreen;
  @CupertinoRoute()
  TermsOfUse termsOfUse;
}
