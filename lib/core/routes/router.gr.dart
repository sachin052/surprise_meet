// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/login.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/signup/signup.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/connetsocialaccounts/connectfacebook.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/home.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/addpotentialmeet/addpotentialmeets.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/meets/homepage/editpotentialmeet.dart';
import 'package:surprise_meet/features/surprise_meet/domain/entity/potentailmeetentity.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/profile/editprofile.dart';
import 'package:surprise_meet/features/surprise_meet/data/models/profileresponse.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/home/chat/selectedchatscreen.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/login/instawebview.dart';
import 'package:surprise_meet/main.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/updatepassword/updatepasswordscreen.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/pages/signup/terms_of_use.dart';

class MyRouter {
  static const loginPage = '/';
  static const signUp = '/sign-up';
  static const connectFacebook = '/connect-facebook';
  static const home = '/home';
  static const addPotentialMeets = '/add-potential-meets';
  static const editPotentialMeet = '/edit-potential-meet';
  static const editProfile = '/edit-profile';
  static const chatScreen = '/chat-screen';
  static const instaWebView = '/insta-web-view';
  static const myApp = '/my-app';
  static const updatePasswordScreen = '/update-password-screen';
  static const termsOfUse = '/terms-of-use';
  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<MyRouter>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case MyRouter.loginPage:
        return MaterialPageRoute(
          builder: (_) => LoginPage(),
          settings: settings,
        );
      case MyRouter.signUp:
        return CupertinoPageRoute(
          builder: (_) => SignUp(),
          settings: settings,
          title: 'Sign Up',
        );
      case MyRouter.connectFacebook:
        if (hasInvalidArgs<ConnectSocialPlatformsArguments>(args)) {
          return misTypedArgsRoute<ConnectSocialPlatformsArguments>(args);
        }
        final typedArgs = args as ConnectSocialPlatformsArguments ??
            ConnectSocialPlatformsArguments();
        return CupertinoPageRoute(
          builder: (_) => ConnectSocialPlatforms(
              key: typedArgs.key,
              isFacebook: typedArgs.isFacebook,
              isInsta: typedArgs.isInsta,
              fromCompleteDialog: typedArgs.fromCompleteDialog,
              addRequest: typedArgs.addRequest),
          settings: settings,
        );
      case MyRouter.home:
        if (hasInvalidArgs<HomeArguments>(args)) {
          return misTypedArgsRoute<HomeArguments>(args);
        }
        final typedArgs = args as HomeArguments ?? HomeArguments();
        return CupertinoPageRoute(
          builder: (_) =>
              Home(key: typedArgs.key, fromPush: typedArgs.fromPush),
          settings: settings,
        );
      case MyRouter.addPotentialMeets:
        return MaterialPageRoute(
          builder: (_) => AddPotentialMeets(),
          settings: settings,
        );
      case MyRouter.editPotentialMeet:
        if (hasInvalidArgs<EditPotentialMeetArguments>(args)) {
          return misTypedArgsRoute<EditPotentialMeetArguments>(args);
        }
        final typedArgs =
            args as EditPotentialMeetArguments ?? EditPotentialMeetArguments();
        return CupertinoPageRoute(
          builder: (_) => EditPotentialMeet(
              key: typedArgs.key, userData: typedArgs.userData),
          settings: settings,
        );
      case MyRouter.editProfile:
        if (hasInvalidArgs<EditProfileArguments>(args)) {
          return misTypedArgsRoute<EditProfileArguments>(args);
        }
        final typedArgs =
            args as EditProfileArguments ?? EditProfileArguments();
        return CupertinoPageRoute(
          builder: (_) => EditProfile(
              key: typedArgs.key, userProfile: typedArgs.userProfile),
          settings: settings,
        );
      case MyRouter.chatScreen:
        if (hasInvalidArgs<SelectedChatScreenArguments>(args)) {
          return misTypedArgsRoute<SelectedChatScreenArguments>(args);
        }
        final typedArgs = args as SelectedChatScreenArguments ??
            SelectedChatScreenArguments();
        return CupertinoPageRoute(
          builder: (_) => SelectedChatScreen(
              key: typedArgs.key,
              receiverId: typedArgs.receiverId,
              connectionId: typedArgs.connectionId,
              otherPersonName: typedArgs.otherPersonName),
          settings: settings,
        );
      case MyRouter.instaWebView:
        if (hasInvalidArgs<InstaWebViewArguments>(args)) {
          return misTypedArgsRoute<InstaWebViewArguments>(args);
        }
        final typedArgs =
            args as InstaWebViewArguments ?? InstaWebViewArguments();
        return CupertinoPageRoute(
          builder: (_) => InstaWebView(
              key: typedArgs.key,
              otherUrl: typedArgs.otherUrl,
              title: typedArgs.title),
          settings: settings,
        );
      case MyRouter.myApp:
        return CupertinoPageRoute(
          builder: (_) => MyApp(),
          settings: settings,
        );
      case MyRouter.updatePasswordScreen:
        return CupertinoPageRoute(
          builder: (_) => UpdatePasswordScreen(),
          settings: settings,
        );
      case MyRouter.termsOfUse:
        return CupertinoPageRoute(
          builder: (_) => TermsOfUse(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//ConnectSocialPlatforms arguments holder class
class ConnectSocialPlatformsArguments {
  final Key key;
  final bool isFacebook;
  final bool isInsta;
  final bool fromCompleteDialog;
  final bool addRequest;
  ConnectSocialPlatformsArguments(
      {this.key,
      this.isFacebook = false,
      this.isInsta = false,
      this.fromCompleteDialog = false,
      this.addRequest = false});
}

//Home arguments holder class
class HomeArguments {
  final Key key;
  final bool fromPush;
  HomeArguments({this.key, this.fromPush = false});
}

//EditPotentialMeet arguments holder class
class EditPotentialMeetArguments {
  final Key key;
  final PotentialMeetEntity userData;
  EditPotentialMeetArguments({this.key, this.userData});
}

//EditProfile arguments holder class
class EditProfileArguments {
  final Key key;
  final Userprofile userProfile;
  EditProfileArguments({this.key, this.userProfile});
}

//SelectedChatScreen arguments holder class
class SelectedChatScreenArguments {
  final Key key;
  final String receiverId;
  final String connectionId;
  final String otherPersonName;
  SelectedChatScreenArguments(
      {this.key, this.receiverId, this.connectionId, this.otherPersonName});
}

//InstaWebView arguments holder class
class InstaWebViewArguments {
  final Key key;
  final String otherUrl;
  final String title;
  InstaWebViewArguments({this.key, this.otherUrl, this.title});
}
