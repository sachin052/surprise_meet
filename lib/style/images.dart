class Images {
  static String appIcon = "asset/images/applogo.svg";
  static String emailIcon = "asset/images/emailicon.svg";
  static String lockIcon = "asset/images/lockicon.svg";
  static String fbIcon = "asset/images/fbicon.svg";
  static String instaIcon = "asset/images/instaicon.svg";
  static String emailSentIcon = "asset/images/email_sent.svg";
  static String heartEmailIcon = "asset/images/heart_email.svg";
  static String instaSvgIcon = "asset/images/insta_svg.svg";
  static String facebookSvglIcon = "asset/images/facebook_svg.svg";
  static String phoneSvgIcon = "asset/images/phone_icon.svg";
  static String deleteSvgIcon = "asset/images/delete_icon.svg";
  static String userSvgIcon = "asset/images/user.svg";
  static String noInternet = "asset/images/no_internet.png";
  static String wenWrong = "asset/images/went_wrong.png";
  static String editIcon = "asset/images/editIcon.svg";
  static String pencilIcon = "asset/images/pencilIcon.svg";
  static String aboutIcon = "asset/images/about.svg";
  static String shareIcon = "asset/images/share.svg";
  static String starIcon = "asset/images/star.svg";
  static String signOutSvg = "asset/images/signOut.svg";

  static String heartOn = "asset/images/heart_on.svg";
  static String heartOff = "asset/images/heart_off.svg";

  static String chatIcon = "asset/images/chat_icon.svg";
  static String userIcon = "asset/images/user_icon.svg";
}
