import 'package:flutter/material.dart';
class AppColors{
  static Color colorPrimaryDark=Color(0xFFFF3333);
  static Color colorPrimary=Color(0xFFFF703A);
  static Color colorAccent=Color(0xFF0D2E27);
  static Color redTextColor=Color(0xFFFF335D);
  static const scaffoldBGColor=Color(0XFFEFF3F6);
}