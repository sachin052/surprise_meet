import 'package:flutter/material.dart';
import 'package:surprise_meet/style/colors.dart';

class AppTheme {
  static String fontFamily = "Lato";

  static ThemeData getAppTheme(BuildContext context) {
    return ThemeData(
      primaryColorDark: AppColors.colorPrimaryDark,
      primaryColor: AppColors.colorPrimary,
      cursorColor: AppColors.colorPrimary,
      textSelectionHandleColor: AppColors.colorPrimary,
      toggleableActiveColor: AppColors.colorPrimary,
      accentColor: AppColors.colorAccent,
      bottomAppBarTheme: BottomAppBarTheme(color: Colors.white,elevation: 10.0),
      appBarTheme: AppBarTheme(
          color: Colors.transparent,
          textTheme: Theme.of(context).textTheme,
          brightness: Brightness.light,
          iconTheme: IconThemeData(color: Colors.black),
//          actionsIconTheme: IconThemeData(color: AppColors.colorPrimary)
      ),
      textTheme: TextTheme(
          headline: TextStyle(fontSize: 34, fontWeight: FontWeight.w800),
          title: TextStyle(fontSize: 24, fontWeight: FontWeight.w400),
          subhead: TextStyle(fontSize: 22, fontWeight: FontWeight.w400),
          body1: TextStyle(fontSize: 14, fontWeight: FontWeight.w400,),
          body2: TextStyle(fontSize: 16, fontWeight: FontWeight.w400,height: 1.3)).apply(bodyColor: AppColors.colorAccent,displayColor: AppColors.colorAccent,),
      fontFamily: fontFamily,
      inputDecorationTheme: InputDecorationTheme(
//        contentPadding: EdgeInsets.all(0.0),
      hasFloatingPlaceholder: false,
//        isDense: true,
        errorStyle: Theme.of(context)
            .textTheme
            .body2
            .copyWith(color: Colors.red.withOpacity(.8)),
        hintStyle: Theme.of(context)
            .textTheme
            .body1
            .copyWith(color: Colors.black.withOpacity(.5)
        ),
        labelStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w400,color: Colors.black.withOpacity(.5))
      ),
    );
  }

  static LinearGradient gradient() {
    return LinearGradient(
      stops: [0.4,0.8],
        colors: [AppColors.redTextColor, AppColors.colorPrimary],
        end: Alignment.centerRight,
        begin: Alignment.centerLeft);
  }

  static LinearGradient reverseGradient() {
    return LinearGradient(
        colors: [AppColors.colorPrimary, AppColors.colorPrimaryDark],
        end: Alignment.centerRight,
        begin: Alignment.centerLeft);
  }
}
