import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_cupertino.dart';
import 'package:country_pickers/country_picker_dialog.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:surprise_meet/core/routes/router.gr.dart';
import 'package:surprise_meet/core/utils/animteddialog.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/bloc/signup/sign_up_state.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/commoncarview.dart';
import 'package:surprise_meet/features/surprise_meet/presentation/widgets/raisedgradientbutton.dart';
import 'package:surprise_meet/style/sizingconfig.dart';

import 'apptheme.dart';
import 'colors.dart';
import 'images.dart';

void showSuccessDialog(BuildContext context, {SignUpSuccessState state}) {
  showAnimatedDialog(
      context,
      CommonCard(
        topContainerChild: SvgPicture.asset(Images.emailSentIcon),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: SizeConfig.blockSizeWidth * 4,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                "Thank you for registering with us!",
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headline
                    .copyWith(fontSize: 18.0),
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeWidth * 4,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: Text(
                "We sent you an account verification email, kindly make sure you check the spam/junk folder of the mailbox.",
                style: Theme.of(context).textTheme.body2,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeWidth * 4,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4),
              child: RaisedGradientButton(
                gradient: AppTheme.gradient(),
                isDialog: true,
                text: "Done",
                onPressed: () {
                  MyRouter.navigatorKey.currentState.pop();
                  MyRouter.navigatorKey.currentState
                      .pop(state.data.message ?? "");
                },
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeWidth * 6,
            ),
          ],
        ),
      ),barrierDismissible: false);
}

void showCountryPicker(BuildContext context,Function(Country) onCountryChange){
  showDialog(
    context: context,
    builder: (context) => Theme(
      data: Theme.of(context).copyWith(primaryColor: Colors.pink),
      child: CountryPickerDialog(
        titlePadding: EdgeInsets.all(8.0),
        searchCursorColor: AppColors.colorAccent,
        searchInputDecoration: InputDecoration(
          prefixIcon: Icon(Icons.search,color: AppColors.colorPrimary,),
            focusColor: AppColors.colorPrimary,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.colorPrimary),
            ),
            hintText: 'Search...',border: UnderlineInputBorder(

            borderSide: BorderSide(color: AppColors.colorPrimary,width: 1.0),

        ),
        ),
        isSearchable: true,
        title: Text('Select your country code',style: TextStyle(fontSize: 20.0),),
        onValuePicked: onCountryChange,
        itemBuilder: _buildDialogItem,
      ),
    ),
  );
}

Widget _buildDialogItem(Country country) => Row(
  children: <Widget>[
    CountryPickerUtils.getDefaultFlagImage(country),
    SizedBox(width: 8.0),
    Text("+${country.phoneCode}",style: TextStyle(fontSize: 16.0),),
    SizedBox(width: 8.0),
    Flexible(child: Text(country.name,style: TextStyle(fontSize: 16.0),))
  ],
);

