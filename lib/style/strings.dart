class Strings{
  static String loginTitle="Welcome Back";
  static String signUpTitle="Surprise Meet";
  static String signInToContinue="Please Sign in to continue.";
  static String signUpToContinue="Please Sign up to continue.";
  static String visSocialMedial="Or via Social Media";
}