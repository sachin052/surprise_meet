import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeWidth;
  static double blockSizeHeight;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  void init(BuildContext context) {

    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    print(screenHeight);
    if(screenHeight<400){
      blockSizeWidth = screenWidth / 90;
      blockSizeHeight = screenHeight / 90;
    }
    else if (Device.get().isTablet) {
      blockSizeWidth = screenWidth / 130;
      blockSizeHeight = screenHeight / 130;
    }

//Check if device is iphone x
    else if (Device.get().isIphoneX) {
      blockSizeWidth = screenWidth /110;
      blockSizeHeight = screenHeight / 110;
    } else {
      blockSizeWidth = screenWidth / 100;
      blockSizeHeight = screenHeight / 100;
    }

    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  }
}
