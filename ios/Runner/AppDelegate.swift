import UIKit
import Flutter
import Firebase
@UIApplicationMain


@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    if #available(iOS 10.0, *) {
         UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
       }
    
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
//    override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//       // return SimpleAuthFlutterPlugin.check(url)
//    }
//    
//  
//    override func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//      //  return SimpleAuthFlutterPlugin.check(url)
//    }
//    
   override func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      NSLog("PUSH registration failed: \(error)")
    // findDependency(\.userData).newPushToken = .waitingForUpload(nil)
   }

   


}
//let kOverlayStyleUpdateNotificationName = "io.flutter.plugin.platform.SystemChromeOverlayNotificationName"
//let kOverlayStyleUpdateNotificationKey = "io.flutter.plugin.platform.SystemChromeOverlayNotificationKey"
//extension FlutterViewController {
//       private struct StatusBarStyleHolder {
//           static var style: UIStatusBarStyle = .default
//       }
//
//       open override func viewDidLoad() {
//           super.viewDidLoad()
//
//           NotificationCenter.default.addObserver(
//               self,
//               selector: #selector(appStatusBar(notification:)),
//               name: NSNotification.Name(kOverlayStyleUpdateNotificationName),
//               object: nil
//           )
//       }
//
//       open override var preferredStatusBarStyle: UIStatusBarStyle {
//           return StatusBarStyleHolder.style
//       }
//
//       @objc private func appStatusBar(notification: NSNotification) {
//           guard
//               let info = notification.userInfo as? Dictionary<String, Any>,
//               let statusBarStyleKey = info[kOverlayStyleUpdateNotificationKey] as? Int
//           else {
//               return
//           }
//
//           if #available(iOS 13.0, *) {
//               StatusBarStyleHolder.style = statusBarStyleKey == 0 ? .darkContent : .lightContent
//           } else {
//               StatusBarStyleHolder.style = statusBarStyleKey == 0 ? .default : .lightContent
//           }
//
//           setNeedsStatusBarAppearanceUpdate()
//       }
//   }
